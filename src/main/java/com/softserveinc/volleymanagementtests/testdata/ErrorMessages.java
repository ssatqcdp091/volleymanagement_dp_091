package com.softserveinc.volleymanagementtests.testdata;

/**
 * Class with constant error messages String's.
 *
 * @author Danil Zhyliaiev
 */
public final class ErrorMessages {
    // Player errors
    public static final String PLAYER_FIRST_NAME_CAN_NOT_BE_EMPTY_ERROR =
            "The field can`t be empty";
    public static final String PLAYER_LAST_NAME_CAN_NOT_BE_EMPTY_ERROR =
            "The field can`t be empty";
    public static final String PLAYER_FIRST_NAME_MUST_CONTAIN_ONLY_LETTERS_ERROR =
            "The field must contain only letters";
    public static final String PLAYER_LAST_NAME_MUST_CONTAIN_ONLY_LETTERS_ERROR =
            "The field must contain only letters";
    public static final String PLAYER_FIRST_NAME_LENGTH_ERROR =
            "The field can`t contain more than 60 symbols";
    public static final String PLAYER_LAST_NAME_LENGTH_ERROR =
            "The field can`t contain more than 60 symbols";
    public static final String PLAYER_BIRTH_YEAR_INVALID_INPUT_ERROR =
            "The field must contain only numbers";
    public static final String PLAYER_HEIGHT_INVALID_INPUT_ERROR =
            "The field must contain only numbers";
    public static final String PLAYER_WEIGHT_INVALID_INPUT_ERROR =
            "The field must contain only numbers";
    public static final String PLAYER_BIRTH_YEAR_RANGE_ERROR =
            "An year of birth must be between 1900 and 2100";
    public static final String PLAYER_HEIGHT_RANGE_ERROR = "A height must be between 10 and 250";
    public static final String PLAYER_WEIGHT_RANGE_ERROR = "A weight must be between 10 and 500";

    // Team errors
    public static final String TEAM_NAME_CAN_NOT_BE_EMPTY_ERROR = "The field can`t be empty";
    public static final String TEAM_CAPTAIN_IN_OTHER_TEAM_ERROR =
            "The player is a member of the other team";
    public static final String TEAM_CAPTAIN_IS_NOT_CHOSEN_ERROR =
            "The captain is neede to be chosen";
    public static final String TEAM_NAME_LENGTH_ERROR =
            "The field can not contain more than 30 symbols";
    public static final String TEAM_COACH_NAME_LENGTH_ERROR =
            "The name can not contain more than 60 symbols";
    public static final String TEAM_ACHIEVEMENTS_LENGTH_ERROR =
            "The field can not contain more than 4000 symbols";
    public static final String TEAM_COACH_NAME_INVALID_INPUT_ERROR =
            "The field must contain only letters";

    /**
     * Suppresses default constructor ensuring non-instantability.
     */
    private ErrorMessages() {
    }
}
