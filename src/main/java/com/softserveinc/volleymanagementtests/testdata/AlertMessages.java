package com.softserveinc.volleymanagementtests.testdata;

public final class AlertMessages {
    public static final String DO_YOU_REALLY_WANT_TO_DELETE_THE_PLAYER =
            "Вы действительно хотите удалить игрока %s ?";

    public static final String THE_PLAYER_WAS_DELETED =
            "Игрок был удален успешно";

    private AlertMessages() {
    }
}
