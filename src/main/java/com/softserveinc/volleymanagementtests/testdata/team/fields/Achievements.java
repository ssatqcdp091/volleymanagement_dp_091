package com.softserveinc.volleymanagementtests.testdata.team.fields;

import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * @author J.Bodnar
 */
public class Achievements {

    public static final int MAX_FIELD_LENGTH = 4000;

    public static String getInvalidBy(Violations violation) {
        switch (violation) {
            case MAX_LENGTH_VIOLATED:
                return RandomStringUtils.random(MAX_FIELD_LENGTH + 1, true, false);
            default:
                throw new RuntimeException(String.format(
                        "%s violation is not available for the %s field",
                        violation.toString(), "achievements"));
        }
    }
}
