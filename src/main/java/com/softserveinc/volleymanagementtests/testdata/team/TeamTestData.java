package com.softserveinc.volleymanagementtests.testdata.team;

import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.testdata.team.fields.Achievements;
import com.softserveinc.volleymanagementtests.testdata.team.fields.Coach;
import com.softserveinc.volleymanagementtests.testdata.team.fields.Name;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author J.Bodnar 22.02.2016.
 */
public class TeamTestData {

    private static List<Team> validTeamsList;

    private TeamTestData() {
    }

    static {
       validTeamsList = new ArrayList<>();
        validTeamsList.add(Team.newBuilder()
                .setName("1").setCoach("W").setAchievements("2")
                .setCaptain(PlayerTestData.getValidPlayer()).build());
        validTeamsList.add(Team.newBuilder()
                .setName("AAAЇІЄҐAAASSSSSSSSSSDDDDDDDDDD")
                .setCoach("AAAЇІЄҐAAASSSSSSSSSSDDDDDDDDDDFFFFFFFFFFGGGGGGGGGGHHHHHHHHHH")
                .setAchievements(RandomStringUtils.random(100, true, true))
                .setCaptain(PlayerTestData.getValidPlayer())
                .addPlayer(PlayerTestData.getValidPlayer()).build());
        validTeamsList.add(Team.newBuilder()
                .setName("Ъ+!)№;%:'?*ff_+=/-)1234").setCoach("Co a'c-h").setAchievements("3547u78i9o0:?*'ff_+=/-)12322")
                .setCaptain(PlayerTestData.getValidPlayer())
                .addPlayer(PlayerTestData.getValidPlayer())
                .addPlayer(PlayerTestData.getValidPlayer())
                .addPlayer(PlayerTestData.getValidPlayer()).build());
        validTeamsList.add(Team.newBuilder()
                .setName("teamname").setCoach("").setAchievements("")
                .setCaptain(PlayerTestData.getValidPlayer()).build());
    }

    public static List<Team> getValidTeamsList(){
        return validTeamsList;
    }

    public static Team getValidTeam(){
        return Team.newBuilder()
                .setName(RandomStringUtils.random(10, true, true))
                .setCoach(RandomStringUtils.random(20, false, true))
                .setAchievements(RandomStringUtils.random(100, true, true))
                .setCaptain(PlayerTestData.getValidPlayer())
                .addPlayer(PlayerTestData.getValidPlayer())
                .build();
    }

    public static Team getInvalidTeamByName(Violations violation){
        return Team.newBuilder(getValidTeam())
                .setName(Name.getInvalidBy(violation)).build();
    }

    public static Team getInvalidTeamByCoach(Violations violation){
        return Team.newBuilder(getValidTeam())
                .setCoach(Coach.getInvalidBy(violation)).build();
    }

    public static Team getInvalidTeamByAchievements(Violations violation){
        return Team.newBuilder(getValidTeam())
                .setAchievements(Achievements.getInvalidBy(violation)).build();
    }

    public static Team getInvalidTeamWithoutCaptain(){
        return   Team.newBuilder()
                .setName(RandomStringUtils.random(10, true, true))
                .setCoach(RandomStringUtils.random(20, false, true))
                .setAchievements(RandomStringUtils.random(100, true, true))
                .build();
    }

}
