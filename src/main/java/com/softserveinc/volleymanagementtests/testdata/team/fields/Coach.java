package com.softserveinc.volleymanagementtests.testdata.team.fields;

import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

/**
 * @author  J.Bodnar
 */
public class Coach {

    public static final int MAX_FIELD_LENGTH = 60;
    public static final char[] NOT_ALLOWED_CHARACTERS
            = {'!', '?', '@', '#', '%', '^', '&', '*', '(', ')', ',', '/', '.'};

    public static String getInvalidBy(Violations violation) {
        switch (violation) {
            case MAX_LENGTH_VIOLATED:
                return RandomStringUtils.random(MAX_FIELD_LENGTH + 1, true, false);
            case CONTAINS_NUMBERS:
                return RandomStringUtils.random(new Random().nextInt(MAX_FIELD_LENGTH), true, true);
            case CONTAIN_NOT_ALLOWED_CHARACTERS:
                return RandomStringUtils
                        .random(new Random().nextInt(MAX_FIELD_LENGTH), NOT_ALLOWED_CHARACTERS);
            default:
                throw new RuntimeException(String.format(
                        "%s violation is not available for the %s field",
                        violation.toString(), "coach"));
        }
    }
}
