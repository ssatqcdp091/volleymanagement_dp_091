package com.softserveinc.volleymanagementtests.testdata.team;

import com.softserveinc.volleymanagementtests.testdata.player.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Class represents Team entity as a test data set.
 * Based on Builder pattern.
 *
 * @author Danil Zhyliaiev
 */
public class Team {
    private String id;
    private String name;
    private String coach;
    private String achievements;
    private Player captain;
    private List<Player> roster = new ArrayList<>();

    private Team() { }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Player getCaptain() {
        return captain;
    }

    public String getCoach() {
        return coach;
    }

    public String getAchievements() {
        return achievements;
    }

    public List<Player> getRoster() {
        return roster;
    }

    public static TeamBuilder newBuilder() {
        return new Team().new TeamBuilder();
    }

    public static TeamBuilder newBuilder(Team team){
        return new Team().new TeamBuilder()
                .setId(team.getId())
                .setName(team.getName())
                .setCoach(team.getCoach())
                .setAchievements(team.getAchievements())
                .setCaptain(team.getCaptain())
                .setRoster(team.getRoster());
    }

    @Override
    public String toString() {
        StringBuilder team = new StringBuilder("Team{");

        if ((id != null) && (id.length() > 0)) {
            team.append("id='").append(id).append("'");
        }

        if ((name != null) && (name.length() > 0)) {
            team.append("name='").append(name).append("'");
        }

        if ((coach != null) && (coach.length() > 0)) {
            team.append("coach='").append(coach).append("'");
        }

        if ((achievements != null) && (achievements.length() > 0)) {
            team.append("achievements='").append(achievements).append("...'");
        }

        if (captain != null) {
            team.append("captain='")
                    .append(captain.getFirstName())
                    .append(" ")
                    .append(captain.getLastName()).append("'");
        }

        if ((roster != null) && (roster.size() > 0)) {
            team.append("roster='");

            for (Player player : roster) {
                team.append(player.getLastName()).append(";");
            }

            team.append("'");
        }

        return team.append("}").toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Team team = (Team) o;

        if (getId() != null) {
            if (!getId().equals(team.getId())) return false;
        } else {
            if (team.getId() != null) return false;
        }
        if (getName() != null) {
            if (!getName().equals(team.getName()))
                return false;
        } else {
            if (team.getName() != null)
                return false;
        }
        if (getCoach() != null) {
            if (!getCoach().equals(team.getCoach()))
                return false;
        } else {
            if (team.getCoach() != null)
                return false;
        }
        if (getAchievements() != null) {
            if (!getAchievements().equals(team.getAchievements()))
                return false;
        } else {
            if (team.getAchievements() != null)
                return false;
        }
        if (getCaptain() != null ? !getCaptain().equals(team.getCaptain()) : team.getCaptain() != null) {
            return false;
        }
        return getRoster() != null ? getRoster().equals(team.getRoster()) : team.getRoster() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getCoach() != null ? getCoach().hashCode() : 0);
        result = 31 * result + (getAchievements() != null ? getAchievements().hashCode() : 0);
        result = 31 * result + (getCaptain() != null ? getCaptain().hashCode() : 0);
        result = 31 * result + (getRoster() != null ? getRoster().hashCode() : 0);
        return result;
    }

    public class TeamBuilder {
        private TeamBuilder() { }

        public TeamBuilder setId(String idArg) {
            id = idArg;
            return this;
        }

        public TeamBuilder setName(String nameArg) {
            name = nameArg;
            return this;
        }

        public TeamBuilder setCaptain(Player captainArg) {
            captain = captainArg;
            return this;
        }

        public TeamBuilder setCoach(String coachArg) {
            coach = coachArg;
            return this;
        }

        public TeamBuilder setAchievements(String achievementsArg) {
            achievements = achievementsArg;
            return this;
        }

        public TeamBuilder setRoster(List<Player> rosterArg) {
            roster = rosterArg;
            return this;
        }

        public TeamBuilder addPlayer(Player playerArg) {
            roster.add(playerArg);
            return this;
        }

        public Team build() {
            return Team.this;
        }
    }
}
