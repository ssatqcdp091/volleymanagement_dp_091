package com.softserveinc.volleymanagementtests.testdata.contributor;

import com.softserveinc.volleymanagementtests.dal.models.ContributorTeamDal;
import com.softserveinc.volleymanagementtests.dal.repositories.ContributorTeamRepository;
import com.softserveinc.volleymanagementtests.testdata.contributor.mappers.DalToContributorTeam;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Danil Zhyliaiev
 */
public class ContributorTestData {
    private ContributorTestData() {
    }

    public static List<ContributorTeam> getAllExistingTeams() throws SQLException, ClassNotFoundException {
        List<ContributorTeam> teams = new ArrayList<>();

        for (ContributorTeamDal contributorTeamDal : new ContributorTeamRepository().getAll()) {
            teams.add(DalToContributorTeam.getInstance().map(contributorTeamDal));
        }

        return teams;
    }
}
