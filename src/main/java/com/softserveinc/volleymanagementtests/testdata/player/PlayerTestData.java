package com.softserveinc.volleymanagementtests.testdata.player;

import com.softserveinc.volleymanagementtests.testdata.player.fields.BirthYear;
import com.softserveinc.volleymanagementtests.testdata.player.fields.FirstName;
import com.softserveinc.volleymanagementtests.testdata.player.fields.Height;
import com.softserveinc.volleymanagementtests.testdata.player.fields.LastName;
import com.softserveinc.volleymanagementtests.testdata.player.fields.Weight;

import java.util.ArrayList;
import java.util.List;

/**
 * @author J.Bodnar 22.02.2016.
 */
public final class PlayerTestData {
    private static final int AVERAGE_NAME_LENGTH = 8;

    private static List<Player> validPlayersList;
    private static List<Player> playersListForSearching;

    private PlayerTestData() {
    }

    static  {
        validPlayersList = new ArrayList<>();
        validPlayersList.add(Player.newBuilder()

                .setFirstName("A").setLastName("A")
                .setBirthYear("1900").setHeight("10").setWeight("10").build());
        validPlayersList.add(Player.newBuilder()
               .setFirstName("ырнолAAAAASSSSSSSSSSDDDDDDDDDDFFFFFFFFFFGGGGGGGGGGHHHHHHHHHH")
               .setLastName("AAAЇІЄҐAAASSSSSSSSSSDDDDDDDDDDFFFFFFFFFFGGGGGGGGGGHHHHHHHHHH")
               .setBirthYear("2100").setHeight("250").setWeight("500").build());
        validPlayersList.add(Player.newBuilder()
               .setFirstName("FirstName").setLastName("LastName")
               .setBirthYear("1990").setHeight("180").setWeight("80").build());
        validPlayersList.add(Player.newBuilder()
               .setFirstName("F irs't-Name").setLastName("Las't Na-me")
               .setBirthYear("").setHeight("").setWeight("").build());

        playersListForSearching =  new ArrayList<>();
        playersListForSearching.add(Player.newBuilder()
                .setFirstName("search").setLastName("TEXT")
                .setBirthYear("1900").setHeight("10").setWeight("10").build());
        playersListForSearching.add(Player.newBuilder()
                .setFirstName("ASearchA").setLastName("ATextA")
                .setBirthYear("1900").setHeight("10").setWeight("10").build());
        playersListForSearching.add(Player.newBuilder()
                .setFirstName("earchS").setLastName("extT")
                .setBirthYear("1900").setHeight("10").setWeight("10").build());
        playersListForSearching.add(Player.newBuilder()
                .setFirstName("searcH").setLastName("teXt")
                .setBirthYear("1900").setHeight("10").setWeight("10").build());
   }

    public static Player getValidPlayer() {
        return Player.newBuilder()
                .setFirstName(FirstName.getRandomFirstName(AVERAGE_NAME_LENGTH))
                .setLastName(LastName.getRandomLastName(AVERAGE_NAME_LENGTH))
                .setBirthYear(BirthYear.getRandomBirthYear())
                .setHeight(Height.getRandomHeight())
                .setWeight(Weight.getRandomWeight()).build();
    }

    public static List<Player> getValidPlayersList() {
        return validPlayersList;
    }

    public static List<Player> getPlayersListForSearching() {
        return playersListForSearching;
    }


    public static Player getInvalidPlayerByFirstName(Violations violation) {
        return Player.newBuilder(getValidPlayer())
                .setFirstName(FirstName.getInvalidBy(violation)).build();
    }

    public static Player getInvalidPlayerByLastName(Violations violation) {
        return Player.newBuilder(getValidPlayer())
                .setLastName(LastName.getInvalidBy(violation)).build();
    }

    public static Player getInvalidPlayerByBirthYear(Violations violation) {
        return Player.newBuilder(getValidPlayer())
                .setBirthYear(BirthYear.getInvalidBy(violation)).build();
    }

    public static Player getInvalidPlayerByHeight(Violations violation) {
        return Player.newBuilder(getValidPlayer())
                .setHeight(Height.getInvalidBy(violation)).build();
    }

    public static Player getInvalidPlayerByWeight(Violations violation) {
        return Player.newBuilder(getValidPlayer())
                .setWeight(Weight.getInvalidBy(violation)).build();
    }
}
