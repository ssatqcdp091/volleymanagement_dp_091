package com.softserveinc.volleymanagementtests.testdata.player.fields;

import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

/**
 * @author J.Bodnar 22.02.2016.
 */
public class BirthYear {

    public static final int MAX_VALUE = 2100;
    public static final int MIN_VALUE = 1900;
    public static final int FIELD_LENGTH = 4;
    private static final char[] NOT_ALLOWED_CHARACTERS =
            {'!', '?', '@', '#', '%', '^', '&', '*', '(', ')', ',', '/', '.'};

    public static String getInvalidBy(Violations violation) {
        switch (violation) {
            case MIN_VALUE_VIOLATED:
                return "" + (MIN_VALUE - 1);
            case MAX_VALUE_VIOLATED:
                return "" + (MAX_VALUE + 1);
            case CONTAINS_LETTERS:
                return RandomStringUtils.random(FIELD_LENGTH, true, true);
            case CONTAIN_NOT_ALLOWED_CHARACTERS:
                return RandomStringUtils
                        .random(new Random().nextInt(FIELD_LENGTH) + 1, NOT_ALLOWED_CHARACTERS);
            case NEGATIVE_NUMBER:
                return "-" + RandomStringUtils.random(FIELD_LENGTH, false, true);
            case NOT_INTEGER_NUMBER:                                                                // To be deleted
                return getRandomNotIntegerBirthYear();
            case NOT_INTEGER_NUMBER_WITH_DOT:
                return getRandomNotIntegerBirthYear().replace(',', '.');
            case NOT_INTEGER_NUMBER_WITH_COMMA:
                return getRandomNotIntegerBirthYear().replace('.', ',');
            default:
                throw new RuntimeException(String.format(
                        "%s violation is not available for the %s field",
                        violation.toString(), "birth year"));
        }
    }

    private static String getRandomNotIntegerBirthYear() {
        return String.format("%s%s%s",
                new Random().nextInt(MAX_VALUE - MIN_VALUE) + (MIN_VALUE),
                RandomStringUtils.random(1, ',', '.'),
                RandomStringUtils
                        .random(2, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'));

    }

    public static String getRandomBirthYear() {
        return String.valueOf(new Random().nextInt(MAX_VALUE - MIN_VALUE + 1) + (MIN_VALUE));
    }
}
