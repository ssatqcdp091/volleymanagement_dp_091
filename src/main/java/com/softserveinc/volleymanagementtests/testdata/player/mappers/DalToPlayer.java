package com.softserveinc.volleymanagementtests.testdata.player.mappers;

import com.softserveinc.volleymanagementtests.testdata.Mapper;
import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.testdata.player.Player;

/**
 * Class to convert test data {@code PlayerDal} object into {@code Player} object.
 * Based on Singleton pattern(static initialization).
 *
 * @author Danil Zhyliaiev
 */
public class DalToPlayer implements Mapper<Player, PlayerDal> {
    private static DalToPlayer instance = new DalToPlayer();

    private DalToPlayer() {
    }

    public static DalToPlayer getInstance() {
        return instance;
    }

    /**
     * Maps given {@code PlayerDal} into {@code Player} field-by-field.
     * All {@code null} fields are converted into empty {@code String} objects("").
     *
     * @param source    given {@code PlayerDal} to convert.
     *
     * @return          new instance of {@code Player} based on given parameter.
     */
    @Override
    public Player map(final PlayerDal source) {
        Player.PlayerBuilder builder = Player.newBuilder();

        if (source.getFirstName() == null) {
            builder.setFirstName("");
        } else {
            builder.setFirstName(source.getFirstName());
        }

        if (source.getLastName() == null) {
            builder.setLastName("");
        } else {
            builder.setLastName(source.getLastName());
        }

        if (source.getBirthYear() == null) {
            builder.setBirthYear("");
        } else {
            builder.setBirthYear(String.valueOf(source.getBirthYear()));
        }

        if (source.getHeight() == null) {
            builder.setHeight("");
        } else {
            builder.setHeight(String.valueOf(source.getHeight()));
        }

        if (source.getWeight() == null) {
            builder.setWeight("");
        } else {
            builder.setWeight(String.valueOf(source.getWeight()));
        }

        if (source.getId() == null) {
            builder.setId("");
        } else {
            builder.setId(String.valueOf(source.getId()));
        }

        return builder.build();
    }
}
