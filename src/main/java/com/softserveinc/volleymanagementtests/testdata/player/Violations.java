package com.softserveinc.volleymanagementtests.testdata.player;

public enum Violations {
    /**
     * Represents the violation: Field is empty.
     * For field, which is required.
     */
    EMPTY_FIELD,
    /**
     * Represents the violation: Maximal field length is violated.
     * For field, which length is limited.
     */
    MAX_LENGTH_VIOLATED,
    /**
     * Represents the violation: Field contains numbers.
     * For field, which defines some name.
     */
    CONTAINS_NUMBERS,
    /**
     * Represents the violation: Field contains letters.
     * For numerical field.
     */
    CONTAINS_LETTERS,
    /**
     * Represents the violation: Field contains not allowed characters.
     * For all fields.
     */
    CONTAIN_NOT_ALLOWED_CHARACTERS,
    /**
     * Represents the violation: Maximal field value is violated.
     * For numerical field.
     */
    MAX_VALUE_VIOLATED,
    /**
     * Represents the violation: Minimal field value is violated.
     * For numerical field.
     */
    MIN_VALUE_VIOLATED,
    /**
     * Represents the violation: Number is negative.
     * For numerical field.
     */
    NEGATIVE_NUMBER,
    /**
     * Represents the violation: Number is not integer.
     * For numerical field.
     */
    NOT_INTEGER_NUMBER,
    /**
     * Represents the violation: Number is not integer with dot separator.
     * For numerical field.
     */
    NOT_INTEGER_NUMBER_WITH_DOT,
    /**
     * Represents the violation: Number is not integer with comma separator.
     * For numerical field.
     */
    NOT_INTEGER_NUMBER_WITH_COMMA
}
