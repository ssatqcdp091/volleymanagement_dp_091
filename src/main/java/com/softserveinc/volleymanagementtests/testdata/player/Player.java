package com.softserveinc.volleymanagementtests.testdata.player;

/**
 * Object that represents Player entity, as a test data set.
 *
 * @author J.Bodnar 22.02.2016.
 */
public class Player {
    private String id;
    private String firstName;
    private String lastName;
    private String birthYear;
    private String height;
    private String weight;

    private Player() {
    }
    public String getId() {
        return id;
    }

    public  String getFirstName() {
        return firstName;
    }

    public  String getLastName() {
        return lastName;
    }

    public  String getBirthYear() {
        return birthYear;
    }

    public  String getHeight() {
        return height;
    }

    public  String getWeight() {
        return weight;
    }

    public class PlayerBuilder {
        private PlayerBuilder() {
        }

        public PlayerBuilder setId(final String idArg) {
            id = idArg;
            return this;
        }

        public PlayerBuilder setFirstName(final String firstNameArg) {
            firstName = firstNameArg;
            return this;
        }

        public PlayerBuilder setLastName(final String lastNameArg) {
            lastName = lastNameArg;
            return this;
        }

        public PlayerBuilder setBirthYear(final String birthYearArg) {
            birthYear = birthYearArg;
            return this;
        }

        public PlayerBuilder setHeight(final String heightArg) {
            height = heightArg;
            return this;
        }

        public PlayerBuilder setWeight(final String weightArg) {
            weight = weightArg;
            return this;
        }

        public Player build() {
            return Player.this;
        }

    }

    public static PlayerBuilder newBuilder() {
        return new Player().new PlayerBuilder();
    }

    public static PlayerBuilder newBuilder(Player player) {
        return new Player().new PlayerBuilder()
                .setFirstName(player.getFirstName())
                .setLastName(player.getLastName())
                .setBirthYear(player.getBirthYear())
                .setHeight(player.getHeight())
                .setWeight(player.getWeight());
    }

    @Override
    public final String toString() {
        StringBuilder player = new StringBuilder("Player{");

        if ((id != null) && (id.length() > 0)) {
            player.append("id='").append(id).append("'");
        }

        if ((firstName != null) && (firstName.length() > 0)) {
            player.append("firstName='").append(firstName).append("'");
        }

        if ((lastName != null) && (lastName.length() > 0)) {
            player.append("lastName='").append(lastName).append("'");
        }

        if ((birthYear != null) && (birthYear.length() > 0)) {
            player.append("birthYear='").append(birthYear).append("'");
        }

        if ((height != null) && (height.length() > 0)) {
            player.append("height='").append(height).append("'");
        }

        if ((weight != null) && (weight.length() > 0)) {
            player.append("weight='").append(weight).append("'");
        }

        return player.append("}").toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        if (!firstName.equals(player.firstName)) return false;
        if (!lastName.equals(player.lastName)) return false;
        if (!birthYear.equals(player.birthYear)) return false;
        if (!height.equals(player.height)) return false;
        return weight.equals(player.weight);
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + birthYear.hashCode();
        result = 31 * result + height.hashCode();
        result = 31 * result + weight.hashCode();
        return result;
    }
}
