package com.softserveinc.volleymanagementtests.testdata.player.fields;

import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

/**
 * @author J.Bodnar 22.02.2016.
 */
public class Height {

    public static final int MAX_VALUE = 250;
    public static final int MIN_VALUE = 10;
    public static final int MAX_FIELD_LENGTH = 3;
    private static final char[] NOT_ALLOWED_CHARACTERS =
            {'!', '?', '@', '#', '%', '^', '&', '*', '(', ')', ',', '/', '.'};

    public static String getInvalidBy(Violations violation) {
        switch (violation) {
            case MIN_VALUE_VIOLATED:
                return "" + (MIN_VALUE - 1);
            case MAX_VALUE_VIOLATED:
                return "" + (MAX_VALUE + 1);
            case CONTAINS_LETTERS:
                return RandomStringUtils.random(MAX_FIELD_LENGTH, true, true);
            case CONTAIN_NOT_ALLOWED_CHARACTERS:
                return RandomStringUtils
                        .random(new Random().nextInt(MAX_FIELD_LENGTH) + 1, NOT_ALLOWED_CHARACTERS);
            case NEGATIVE_NUMBER:
                return "-" + RandomStringUtils.random(MAX_FIELD_LENGTH, false, true);
            case NOT_INTEGER_NUMBER:
                return getRandomNotIntegerHeight();
            case NOT_INTEGER_NUMBER_WITH_DOT:
                return getRandomNotIntegerHeight().replace(',', '.');
            case NOT_INTEGER_NUMBER_WITH_COMMA:
                return getRandomNotIntegerHeight().replace('.', ',');
            default:
                throw new RuntimeException(String.format(
                        "%s violation is not available for the %s field",
                        violation.toString(), "height"));
        }
    }

    private static String getRandomNotIntegerHeight() {
        return String.format("%s%s%s",
                new Random().nextInt(MAX_VALUE - MIN_VALUE) + (MIN_VALUE),
                RandomStringUtils.random(1, ',', '.'),
                RandomStringUtils
                        .random(2, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'));
    }

    public static String getRandomHeight() {
        return String.valueOf(new Random().nextInt(MAX_VALUE - MIN_VALUE + 1) + (MIN_VALUE));
    }
}
