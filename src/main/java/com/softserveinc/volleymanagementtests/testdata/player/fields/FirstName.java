package com.softserveinc.volleymanagementtests.testdata.player.fields;

import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

/**
 * @author J.Bodnar  22.02.2016.
 */
public class FirstName {
    private static final int MAX_FIELD_LENGTH = 60;
    private static final char[] NOT_ALLOWED_CHARACTERS =
            {'!', '?', '@', '#', '%', '^', '&', '*', '(', ')', ',', '/', '.'};


    public static String getInvalidBy(Violations violation) {
        switch (violation) {
            case EMPTY_FIELD:
                return "";
            case MAX_LENGTH_VIOLATED:
                return RandomStringUtils.random(MAX_FIELD_LENGTH + 1, true, false);
            case CONTAINS_NUMBERS:
                return RandomStringUtils.random(new Random().nextInt(MAX_FIELD_LENGTH), true, true)
                        + (RandomStringUtils.random(1, false, true));
            case CONTAIN_NOT_ALLOWED_CHARACTERS:
                return RandomStringUtils
                        .random(new Random().nextInt(MAX_FIELD_LENGTH), NOT_ALLOWED_CHARACTERS);
            default:
                throw new RuntimeException(String.format(
                        "%s violation is not available for the %s field",
                        violation.toString(), "first name"));
        }
    }

    public static String getRandomFirstName(int firstNameLength) {
        String firstName = RandomStringUtils.random(firstNameLength, true, false).toLowerCase();
        return firstName.substring(0, 1).toUpperCase() + firstName.substring(1).toLowerCase();
    }
}
