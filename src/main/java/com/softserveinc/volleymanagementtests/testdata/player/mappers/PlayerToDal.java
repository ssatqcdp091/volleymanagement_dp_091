package com.softserveinc.volleymanagementtests.testdata.player.mappers;

import com.softserveinc.volleymanagementtests.testdata.Mapper;
import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.testdata.player.Player;

/**
 * Class to convert test data {@code Player} object into {@code PlayerDal} object.
 * Based on Singleton pattern(static initialization).
 *
 * @author Danil Zhyliaiev
 */
public class PlayerToDal implements Mapper<PlayerDal, Player> {
    private static PlayerToDal instance = new PlayerToDal();

    private PlayerToDal() {
    }

    public static PlayerToDal getInstance() {
        return instance;
    }

    /**
     * Maps given {@code Player} into {@code PlayerDal} field-by-field.
     * Both {@code null} and empty {@code String}("") in all fields are converted into {@code null}.
     *
     * @param source    given {@code Player} to convert.
     *
     * @return          new instance of {@code PlayerDal} based on given parameter.
     */
    @Override
    public PlayerDal map(final Player source) {
        PlayerDal.PlayerDalBuilder playerDalBuilder = new PlayerDal.PlayerDalBuilder();

        if ((source.getFirstName() != null) && (source.getFirstName().length() > 0)) {
            playerDalBuilder.firstName(source.getFirstName());
        } else {
            playerDalBuilder.firstName(null);
        }

        if ((source.getLastName() != null) && (source.getLastName().length() > 0)) {
            playerDalBuilder.lastName(source.getLastName());
        } else {
            playerDalBuilder.lastName(null);
        }

        if ((source.getBirthYear() != null) && (source.getBirthYear().length() > 0)) {
            playerDalBuilder.birthYear(Integer.parseInt(source.getBirthYear()));
        } else {
            playerDalBuilder.birthYear(null);
        }

        if ((source.getHeight() != null) && (source.getHeight().length() > 0)) {
            playerDalBuilder.height(Integer.parseInt(source.getHeight()));
        } else {
            playerDalBuilder.height(null);
        }

        if ((source.getWeight() != null) && (source.getWeight().length() > 0)) {
            playerDalBuilder.weight(Integer.parseInt(source.getWeight()));
        } else {
            playerDalBuilder.weight(null);
        }

        if ((source.getId() != null) && (source.getId().length() > 0)) {
            playerDalBuilder.id(Integer.parseInt(source.getId()));
        } else {
            playerDalBuilder.id(null);
        }

        return playerDalBuilder.build();
    }
}
