package com.softserveinc.volleymanagementtests.pages.team;

import com.softserveinc.volleymanagementtests.pages.player.PlayerDetailsPage;
import com.softserveinc.volleymanagementtests.pages.team.uimaps.TeamDetailsUIMap;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import org.openqa.selenium.TimeoutException;

import java.util.ArrayList;
import java.util.List;

/**
 * Object that represents  TeamDetails page and provide methods to interact
 * with it.
 *
 * @author S.Tsyganovskiy
 */
public class TeamDetailsPage {

    private TeamDetailsUIMap controls;
    public TeamDetailsPage() {
        controls = new TeamDetailsUIMap();
    }

    public Link getCaptainDetailsLink() {
        return controls.getCaptainDetails();
    }
    public PlayerDetailsPage clickCaptainDetails() {
        controls.getCaptainDetails().click();
        return new PlayerDetailsPage();
    }


    public List<Link> getPlayerRoaster() {
        return controls.getPlayerRoaster();
    }

    public PlayerDetailsPage getPlayersDetails(String linkText) {
        List<Link> playerList = controls.getPlayerRoaster();
        for (Link i:playerList) {
            if (i.getText().equals(linkText)) {
                i.click();
                break;
            }
        }
        return new PlayerDetailsPage();
    }

    public Link getEditTeamLink() {
        return controls.getEditTeamLink();
    }

    public TeamEditPage clickEditTeamLink() {
        controls.getEditTeamLink().click();
        return new TeamEditPage();
    }

    public Link getBack() {
        return controls.getBack();
    }

    public TeamListPage clickBackLink() {
        controls.getBack().click();
        return new TeamListPage();
    }

    public Label getTeamDetailsLabel() {
        return controls.getTeamDetailsLabel();
    }

    public Label getTeamNameLabel() {
        return controls.getTeamNameLabel();
    }

    public Label getCoachLabel() {
        return controls.getCoachLabel();
    }

    public Label getCoachNameLabel() {
        return controls.getCoachNameLabel();
    }

    public Label getAchievementsLabel() {
        return controls.getAchievementsLabel();
    }

    public Label getAchievementsDescriptionLabel() {
        return controls.getAchievementsDescriptionLabel();
    }

    public Label getCaptainLabel() {
        return controls.getCaptainLabel();
    }

    public Label getRosterLabel() {
        return controls.getRosterLabel();
    }

    private String getTeamId() {
        String currentUrl = WebDriverUtils.getCurrentUrl();
        return currentUrl.substring(currentUrl.lastIndexOf('/') + 1);
    }

    private String getPlayerId(Link playerLink) {
        String playerHref = playerLink.getHref().toLowerCase();
        int idBeginIndex = playerHref.lastIndexOf("details/") + "details/".length();
        int idEndIndex = playerHref.indexOf("?return");
        return playerHref.substring(idBeginIndex, idEndIndex);
    }

    private Player readPlayer(Link playerLink) {
        String[] playerFullName = playerLink.getText().split(" ");                                  // temporal solution. doesn't work when full name has more than 3 words.
        return Player.newBuilder()
                .setId(getPlayerId(playerLink))
                .setLastName(playerFullName[0])
                .setFirstName(playerFullName[1])
                .build();
    }

    private List<Player> readRoster() {
        List<Player> roster = new ArrayList<>();
        for (Link player : getPlayerRoaster()) {
            roster.add(readPlayer(player));
        }

        return roster;
    }

    public Team readTeam() {
        Team.TeamBuilder teamBuilder = Team.newBuilder();
        String name = getTeamNameLabel().getText();
        String achievements;
        try {
            achievements = getAchievementsDescriptionLabel().getText();
        } catch (TimeoutException e) {
            achievements = null;
        }

        String coachName;
        try {
            coachName = getCoachNameLabel().getText();
        } catch (TimeoutException e) {
            coachName = null;
        }

        return teamBuilder
                .setId(getTeamId())
                .setName(name.substring(name.indexOf(" ")).trim())
                .setAchievements(achievements)
                .setCoach(coachName)
                .setCaptain(readPlayer(getCaptainDetailsLink()))
                .setRoster(readRoster())
                .build();
    }
}
