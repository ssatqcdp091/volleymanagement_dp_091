package com.softserveinc.volleymanagementtests.pages.team.uimaps;

import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

import java.util.List;

/**
 * UI Map contain all Team Details page locators and initializes controls using
 * lazy initialization approach.
 *
 * @author S.Tsyganovskiy
 */
public class TeamDetailsUIMap {

    private Link captainDetails;
    private List<Link> playerRoaster;
    private Link editTeamLink;
    private Link back;
    private Label teamDetailsLabel;
    private Label teamNameLabel;
    private Label coachLabel;
    private Label coachNameLabel;
    private Label achievementsLabel;
    private Label achievementsDescriptionLabel;
    private Label captainLabel;
    private Label rosterLabel;


    public TeamDetailsUIMap() {
        if (!getTeamDetailsLabel().getText().equals("Подробно о команде")) {
            throw new RuntimeException("This is not \"Team details\" page.");
        }
    }


    public Link getCaptainDetails() {
        if (captainDetails == null) {
            captainDetails = LinkImpl.getByCss("fieldset > a");
        }
        return captainDetails;
    }

    public List<Link> getPlayerRoaster() {
        if (playerRoaster == null) {
            playerRoaster = WebElementsList.getByCss("li>a").asLinks();
        }
        return playerRoaster;
    }

    public Link getEditTeamLink() {
        if (editTeamLink == null) {
            editTeamLink = LinkImpl.getByCss("[href^=\"/Teams/Edit/\"]");
        }
        return editTeamLink;
    }

    public Link getBack() {
        if (back == null) {
            back = LinkImpl.getByCss("#content > p > a:nth-child(2)");
        }
        return back;
    }

    public Label getTeamDetailsLabel() {
        if (teamDetailsLabel == null) {
            teamDetailsLabel = LabelImpl.getByCss("#content>h2");
        }
        return teamDetailsLabel;
    }

    public Label getTeamNameLabel() {
        if (teamNameLabel == null) {
            teamNameLabel = LabelImpl.getByCss("#content>fieldset>legend");
        }
        return teamNameLabel;
    }

    public Label getCoachLabel() {
        if (coachLabel == null) {
            coachLabel = LabelImpl.getByCss(".display-label>label");
        }
        return coachLabel;
    }

    public Label getCoachNameLabel() {
        if (coachNameLabel == null) {
            coachNameLabel = LabelImpl.getByCss("legend + div.display-label + div.display-field");
        }
        return coachNameLabel;
    }

    public Label getAchievementsLabel() {
        if (achievementsLabel == null) {
            achievementsLabel = LabelImpl.getByCss("[for=\"Achievements\"]");
        }
        return achievementsLabel;
    }

    public Label getAchievementsDescriptionLabel() {
        if (achievementsDescriptionLabel == null) {
            achievementsDescriptionLabel = LabelImpl.getByCss(
                    "legend + div.display-label + div.display-field"
                    + "+ div.display-label + div.display-field");
        }
        return achievementsDescriptionLabel;
    }

    public Label getCaptainLabel() {
        if (captainLabel == null) {
            captainLabel = LabelImpl.getByCss("[for=\"Captain\"]");
        }
        return captainLabel;
    }

    public Label getRosterLabel() {
        if (rosterLabel == null) {
            rosterLabel = LabelImpl.getByCss("[for=\"Roster\"]");
        }
        return rosterLabel;
    }
}
