package com.softserveinc.volleymanagementtests.pages.team.uimaps;

import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.TextInputImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

import java.util.List;

/**
 * !!!This page is based on sources and may be inaccurate!!!
 * List of TeamEditPage controls and accessors.
 */
public class TeamEditUIMap {
    private Label editTeamLabel;
    private Label teamLabel;
    private Label nameLabel;
    private TextInput nameInput;
    private Label errorMessageNameInput;
    private Label coachLabel;
    private TextInput coachInput;
    private Label captainLabel;
    private TextInput captainInput;
    private Button chooseCaptainButton;
    private Label achievementsLabel;
    private TextInput achievementsInput;
    private Label teamPlayersLabel;
    private Button addPlayersButton;
    private Label firstAndLastNameLabel;
    private List<TextInput> teamRoster;
    private Button saveTeamButton;
    private Link backToListLink;
    private Label errorMessage;

    public TeamEditUIMap() {
        if (getEditTeamLabel().getText().equals("Редактирование команды")) {
            throw new RuntimeException("This is not \"Edit team\" page.");
        }
    }

    public final Label getEditTeamLabel() {
        if (editTeamLabel == null) {
            editTeamLabel = LabelImpl.getByCss("#content > h2");
        }
        return editTeamLabel;
    }

    public final Label getTeamLabel() {
        if (teamLabel == null) {
            teamLabel = LabelImpl.getByCss("fieldset > legend");
        }
        return teamLabel;
    }

    public final  Label getNameLabel() {
        if (nameLabel == null) {
            nameLabel = LabelImpl.getByCss("[for=\"Name\"]");
        }
        return nameLabel;
    }

    public final  TextInput getNameInput() {
        if (nameInput == null) {
            nameInput = TextInputImpl.getById("Name");
        }
        return nameInput;
    }

    public final Label getErrorMessageNameInput() {
        if (errorMessageNameInput == null) {
            errorMessageNameInput = LabelImpl.getById("Name-error");
        }
        return errorMessageNameInput;
    }

    public final  Label getCoachLabel() {
        if (coachLabel == null) {
            coachLabel = LabelImpl.getByCss("[for=\"Coach\"]");
        }
        return coachLabel;
    }

    public final  TextInput getCoachInput() {
        if (coachInput == null) {
            coachInput = TextInputImpl.getById("Coach");
        }
        return coachInput;
    }

    public final  Label getCaptainLabel() {
        if (captainLabel == null) {
            captainLabel = LabelImpl.getByCss("[for=\"Captain_FullNameText\"]");
        }
        return captainLabel;
    }

    public final  TextInput getCaptainInput() {
        if (captainInput == null) {
            captainInput = TextInputImpl.getById("captainFullName");
        }
        return captainInput;
    }

    public final  Button getChooseCaptainButton() {
        if (chooseCaptainButton == null) {
            chooseCaptainButton = ButtonImpl.getByName("chooseCaptainButton");
        }
        return chooseCaptainButton;
    }

    public final  Label getAchievementsLabel() {
        if (achievementsLabel == null) {
            achievementsLabel = LabelImpl.getByCss("[for=\"Achievements\"]");
        }
        return achievementsLabel;
    }

    public final  TextInput getAchievementsInput() {
        if (achievementsInput == null) {
            achievementsInput = TextInputImpl.getById("Achievements");
        }
        return achievementsInput;
    }

    public final  Label getTeamPlayersLabel() {
        if (teamPlayersLabel == null) {
            teamPlayersLabel = LabelImpl.getByCss("#content>form>fieldset>div>h4");
        }
        return teamPlayersLabel;
    }

    public final  Button getAddPlayersButton() {
        if (addPlayersButton == null) {
            addPlayersButton
                    = ButtonImpl.getByName("[onclick=\"openChoosingPlayersWindow('ChoosingRosterWindow')\"]");
        }
        return addPlayersButton;
    }

    public final  Label getFirstAndLastNameLabel() {
        if (firstAndLastNameLabel == null) {
            firstAndLastNameLabel = LabelImpl.getByCss("#teamRoster>tbody>tr>th");
        }
        return firstAndLastNameLabel;
    }

    public List<TextInput> getTeamRoster() {
        return WebElementsList.getByCss("tr.rosterPlayer > td:first-child > input").asTextInputs();
    }

    public final  Button getSaveTeamButton() {
        if (saveTeamButton == null) {
            saveTeamButton = ButtonImpl.getByCss("[type=\"submit\"]");
        }
        return saveTeamButton;
    }

    public final  Link getBackToListLink() {
        if (backToListLink == null) {
            backToListLink = LinkImpl.getByCss("[href$=\"/Teams\"]");
        }
        return backToListLink;
    }

    public Label getErrorMessage() {
        if (errorMessage == null) {
            errorMessage = LabelImpl.getByCss(".validation-summary-errors>ul>li");
        }
        return errorMessage;
    }
}
