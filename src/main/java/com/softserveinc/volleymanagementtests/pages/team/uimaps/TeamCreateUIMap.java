package com.softserveinc.volleymanagementtests.pages.team.uimaps;

import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.*;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

import java.util.List;

/**
 * UI Map contain all CreateTeam page locators and initializes controls using
 * lazy initialization approach.
 *
 * @author S.Tsyganovskiy
 */
public class TeamCreateUIMap {

    private Label createNewTeam;
    private Label teamLabel;
    private Label nameLabel;
    private TextInput nameInput;
    private Label errorMessageNameInput;
    private Label coachLabel;
    private TextInput coachInput;
    private Label coachErrorInput;
    private Label captainLabel;
    private TextInput captainInput;
    private Button openChoosingPlayersButton;
    private Label achievementsLabel;
    private TextInput achievementsInput;
    private Label teamPlayersLabel;
    private Button addPlayersButton;
    private Label firstAndLasttNameLabel;
    private List<Label> playersList;
    private Button createTeamButton;
    private Link backToListLink;
    private Label errorMessage;

    public TeamCreateUIMap() {

        if (!(WebDriverUtils.getCurrentUrl().contains("/Teams/Create"))) {
            throw new RuntimeException("This is not \"Create new team\" page.");
        }
    }


    public final Label getCreateNewTeam() {
        if (createNewTeam == null) {
            createNewTeam = LabelImpl.getByCss("#content > h2");
        }
        return createNewTeam;
    }

    public final Label getTeamLabel() {
        if (teamLabel == null) {
            teamLabel = LabelImpl.getByCss("fieldset > legend");
        }
        return teamLabel;
    }

    public final  Label getNameLabel() {
        if (nameLabel == null) {
            nameLabel = LabelImpl.getByCss("[for=\"Name\"]");
        }
        return nameLabel;
    }

    public final  TextInput getNameInput() {
        if (nameInput == null) {
            nameInput = TextInputImpl.getById("Name");
        }
        return nameInput;
    }

    public final Label getErrorMessageNameInput() {
        if (errorMessageNameInput == null) {
            errorMessageNameInput = LabelImpl.getById("Name-error");
        }
        return errorMessageNameInput;
    }

    public final  Label getCoachLabel() {
        if (coachLabel == null) {
            coachLabel = LabelImpl.getByCss("[for=\"Coach\"]");
        }
        return coachLabel;
    }

    public final  TextInput getCoachInput() {
        if (coachInput == null) {
            coachInput = TextInputImpl.getById("Coach");
        }
        return coachInput;
    }

    public Label getCoachErrorInput() {
        if (coachErrorInput == null) {
            coachErrorInput = LabelImpl.getById("Coach-error");
        }
        return coachErrorInput;
    }

    public final  Label getCaptainLabel() {
        if (captainLabel == null) {
            captainLabel = LabelImpl.getByCss("[for=\"Captain_FullNameText\"]");
        }
        return captainLabel;
    }

    public final  TextInput getCaptainInput() {
        if (captainInput == null) {
            captainInput = TextInputImpl.getById("captainFullName");
        }
        return captainInput;
    }

    public final  Button getOpenChoosingPlayersButton() {
        if (openChoosingPlayersButton == null) {
            openChoosingPlayersButton = ButtonImpl.getByCss("input+input");
        }
        return openChoosingPlayersButton;
    }

    public final  Label getAchievementsLabel() {
        if (achievementsLabel == null) {
            achievementsLabel = LabelImpl.getByCss("[for=\"Achievements\"]");
        }
        return achievementsLabel;
    }

    public final  TextInput getAchievementsInput() {
        if (achievementsInput == null) {
            achievementsInput = TextInputImpl.getById("Achievements");
        }
        return achievementsInput;
    }

    public final  Label getTeamPlayersLabel() {
        if (teamPlayersLabel == null) {
            teamPlayersLabel = LabelImpl.getByCss("#content>form>fieldset>div>h4");
        }
        return teamPlayersLabel;
    }

    public final  Button getAddPlayersButton() {
        if (addPlayersButton == null) {
            addPlayersButton
                    = ButtonImpl.getByCss("[onclick=\"openChoosingPlayersWindow('ChoosingRosterWindow')\"]");
        }
        return addPlayersButton;
    }

    public final  Label getFirstAndLasttNameLabel() {
        if (firstAndLasttNameLabel == null) {
            firstAndLasttNameLabel = LabelImpl.getByCss("#teamRoster>tbody>tr>th");
        }
        return firstAndLasttNameLabel;
    }

    public List<Label> getPlayersList() {
        if (playersList == null) {
            playersList = WebElementsList.getByCss("td>input").asLabels();
        }
        return playersList;
    }

    public final  Button getCreateTeamButton() {
        if (createTeamButton == null) {
            createTeamButton = ButtonImpl.getByCss("#content>form>fieldset>p>input");
        }
        return createTeamButton;
    }


    public final  Link getBackToListLink() {
        if (backToListLink == null) {
            backToListLink = LinkImpl.getByCss("[href$=\"/Teams\"]");
        }
        return backToListLink;
    }


    public Label getErrorMessage() {
        if (errorMessage == null) {
            errorMessage = LabelImpl.getByCss(".validation-summary-errors>ul>li");
        }
        return errorMessage;
    }
}