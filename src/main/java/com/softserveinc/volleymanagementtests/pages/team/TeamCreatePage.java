package com.softserveinc.volleymanagementtests.pages.team;

import com.softserveinc.volleymanagementtests.pages.player.ChoosePlayerPage;
import com.softserveinc.volleymanagementtests.pages.team.uimaps.TeamCreateUIMap;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

import java.util.List;

/**
 * Object that represents Create Team  page and provide methods to interact
 * with it.
 *
 * @author S.Tsyganovskiy
 */
public class TeamCreatePage {
    private TeamCreateUIMap controls;

    public TeamCreatePage() {
        controls = new TeamCreateUIMap();
    }

    public final Label getCreateNewTeamLabel() {
        return controls.getCreateNewTeam();
    }

    public final Label getTeamLabel() {
        return controls.getTeamLabel();
    }


    public final Label getNameLabel() {
        return controls.getNameLabel();
    }


    public final TextInput getNameInput() {
        return controls.getNameInput();
    }

    public final TeamCreatePage nameInput(final String name) {
        controls.getNameInput().type(name);
        return this;
    }

    public final Label getErrorMessageNameInput() {
        return controls.getErrorMessageNameInput();
    }


    public final Label getCoachLabel() {
        return controls.getCoachLabel();
    }


    public final TextInput getCoachInput() {
        return controls.getCoachInput();
    }


    public final TeamCreatePage coachInput(final String name) {
        controls.getCoachInput().type(name);
        return this;
    }

    public final Label getCoachErrorInputLabel() {
        return controls.getCoachErrorInput();
    }

    public final Label getCaptainLabel() {
        return controls.getCaptainLabel();
    }


    public final TextInput getCaptainInput() {
        return controls.getCaptainInput();
    }

    public final TeamCreatePage captainInput(final String name) {
        controls.getCaptainInput().type(name);
        return this;
    }

    public final Button getOpenChoosingPlayersButton() {
        return controls.getOpenChoosingPlayersButton();
    }

    public final ChoosePlayerPage submitOpenChoosingPlayersButton() {
        controls.getOpenChoosingPlayersButton().submit();
        WebDriverUtils.switchToNewWindow();
        return new ChoosePlayerPage();
    }

    public final Label getAchievementsLabel() {
        return controls.getAchievementsLabel();
    }


    public final TextInput getAchievementsInput() {
        return controls.getAchievementsInput();
    }

    public final TeamCreatePage achievementsInput(final String name) {
        controls.getAchievementsInput().type(name);
        return this;
    }

    public final Label getTeamPlayersLabel() {
        return controls.getTeamPlayersLabel();
    }


    public final Button getAddPlayersButton() {
        return controls.getAddPlayersButton();
    }

    public final ChoosePlayerPage submitAddPlayersButton() {
        controls.getAddPlayersButton().submit();
        WebDriverUtils.switchToNewWindow();
        return new ChoosePlayerPage();
    }

    public final Label getFirstAndLasttNameLabel() {
        return controls.getFirstAndLasttNameLabel();
    }

    public final List<Label> getPlayersList(){
        return controls.getPlayersList();
    }

    public final Button getCreateTeamButton() {
        return controls.getCreateTeamButton();
    }

    public final TeamListPage submitCreateTeamButton() {
        controls.getCreateTeamButton().submit();
        return new TeamListPage();
    }

    public Link getBackToListLink() {
        return controls.getBackToListLink();
    }

    public final TeamListPage clickBackToList() {
        controls.getBackToListLink().click();
        return new TeamListPage();
    }

    public final Label getErroeMessageLabel() {
        return controls.getErrorMessage();
    }

    public TeamCreatePage logInAsCaptain() {
        return this;
    }

    public void setAllTeamFields(Team team){

        if (team.getName()!=null) {
            setTeamName(team.getName());
        }

        if (team.getCoach()!= null) {
            setCoachName(team.getCoach());
        }

        if (team.getCaptain()!=null) {
            ChoosePlayerPage choosePlayerPageToAddCaptain = submitOpenChoosingPlayersButton();
            choosePlayerPageToAddCaptain.chooseCaptain(team.getCaptain());
        }

        if (team.getAchievements()!=null) {
            setAchievements(team.getAchievements());
        }

        if (team.getRoster().size()>0) {
            ChoosePlayerPage choosePlayerPageToAddPlayers = submitAddPlayersButton();
            choosePlayerPageToAddPlayers.choosePlayers(team.getRoster());
        }
    }



    private void setCoachName(String name) {
        controls.getCoachInput().type(name);
    }

    private void setTeamName(String name) {
        controls.getNameInput().type(name);
    }

    private void setAchievements(String text) {
        controls.getAchievementsInput().type(text);
    }
}
