package com.softserveinc.volleymanagementtests.pages.team.uimaps;

import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import java.util.ArrayList;
import java.util.List;

/**
 * UI Map contain all  TeamList page locators and initializes controls using
 * lazy initialization approach.
 *
 * @author S.Tsyganovskiy
 */
public class TeamListUIMap {

    private Label listOfTeamsLabel;
    private Label nameLabel;
    private Link createTeamLink;
    private Button deleteButton;
    private List<TeamTableRow> teamList;

    public TeamListUIMap() {
        if (!(WebDriverUtils.getCurrentUrl().contains("/Teams"))) {
            throw new RuntimeException("This is not \"Team list\" page.");
        }
    }

    public Button getDeleteButton() {
        if (deleteButton == null) {
            deleteButton = ButtonImpl.getByCss("td>input");
        }
        return deleteButton;
    }

    public Label getListOfTeamsLabel() {
        if (listOfTeamsLabel == null) {
            listOfTeamsLabel = LabelImpl.getByCss("#content>h2");
        }
        return listOfTeamsLabel;
    }

    public Label getNameLabel() {
        if (nameLabel == null) {
            nameLabel = LabelImpl.getByCss(".table>tbody>tr>th");
        }
        return nameLabel;
    }

    public Link getCreateTeamLink() {
        if (createTeamLink == null) {
            createTeamLink = LinkImpl.getByCss("#content>p>a");
        }
        return createTeamLink;
    }

    public List<TeamTableRow> getTeamList() {
        if (teamList == null) {
            teamList = new ArrayList<>();

            List<Label> teamNames = WebElementsList.getByCss("tbody > tr >td:nth-child(1)")
                    .asLabels();
            List<Link> teamDetailsLinks = WebElementsList.getByCss("[href^=\"/Teams/Details/\"]")
                    .asLinks();
            List<Button> deleteButtons = WebElementsList.getByCss("a+input")
                    .asButtons();

            for (int i = 0; i < teamNames.size(); i++) {
                teamList.add(new TeamTableRow(teamNames.get(i), teamDetailsLinks.get(i),
                        deleteButtons.get(i)));
            }
        }
        return teamList;
    }

    public class TeamTableRow {

        private Label nameTeam;
        private Link teamDetails;
        private Button deleteTeam;

        public TeamTableRow(Label nameTeam, Link teamDetails, Button deleteTeam) {
            this.nameTeam = nameTeam;
            this.teamDetails = teamDetails;
            this.deleteTeam = deleteTeam;
        }

        public Label getNameTeam() {
            return nameTeam;
        }

        public Link getTeamDetails() {
            return teamDetails;
        }

        public Button getDeleteTeamButton() {
            return deleteTeam;
        }
    }
}
