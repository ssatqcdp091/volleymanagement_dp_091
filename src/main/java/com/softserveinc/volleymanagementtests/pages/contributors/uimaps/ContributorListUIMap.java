package com.softserveinc.volleymanagementtests.pages.contributors.uimaps;

import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides access to web elements on Contributor List page.
 *
 * @author Danil Zhyliaiev
 */
public class ContributorListUIMap {
    private Label pageHeader;
    private List<TeamPanel> teams;

    public ContributorListUIMap() {
        if (!(WebDriverUtils.getCurrentUrl().endsWith("ContributorsTeam"))
                || !(getPageHeader().getText().equals("Contributors"))) {
            throw new RuntimeException("This is not Contributors list page.");
        }
    }

    public Label getPageHeader() {
        if (pageHeader == null) {
            pageHeader = LabelImpl.getByCss("#content > h2");
        }

        return pageHeader;
    }

    public List<TeamPanel> getTeams() {
        if (teams == null) {

            int panelCount = WebElementsList.getByCss("div.panel").asLabels().size();
            teams = new ArrayList<>();
            for (int i = 1; i <= panelCount; i++) {
                teams.add(new TeamPanel(String.format("div.panel:nth-child(%s)", i + 1)));
            }
        }
        return teams;
    }

    public class TeamPanel {
        private Label teamName;
        private List<Label> teamMembers;
        private final String parentCss;

        private TeamPanel(String parentCss) {
            this.parentCss = parentCss;
        }

        public Label getTeamName() {
            if (teamName == null) {
                teamName = LabelImpl.getByCss(parentCss + " > div.panel-heading");
            }

            return teamName;
        }

        public List<Label> getTeamMembers() {
            if (teamMembers == null) {
                teamMembers = WebElementsList
                        .getByCss(parentCss + " > div.panel-body > div.col-md-4")
                        .asLabels();
            }

            return teamMembers;
        }
    }

}
