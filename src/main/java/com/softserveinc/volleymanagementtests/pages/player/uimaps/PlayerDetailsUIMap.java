package com.softserveinc.volleymanagementtests.pages.player.uimaps;

import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

/**
 * UI Map contains all controls of Player Details Page as fields.
 * And methods initialising controls with their locators
 *
 * @author J.Bodnar
 */
public class PlayerDetailsUIMap {

    private Label firstNameValue;
    private Label lastNameValue;
    private Label birthYearValue;
    private Label weightValue;
    private Label heightValue;
    private Link editLink;
    private Link backLink;

    /**
     * Default constructor.
     * Verify if the page-url matches Player Details  page
     * @throws RuntimeException thrown if url does not match Player Details page.
     */
    public PlayerDetailsUIMap() {
        if (!(WebDriverUtils.getCurrentUrl().endsWith("Players")
                || WebDriverUtils.getCurrentUrl().contains("Details"))) {
            throw new RuntimeException("It is not Players Details List page.");
        }
    }

    public final Label getFirstNameValue() {
        if (firstNameValue == null) {
            firstNameValue = LabelImpl.getByCss(
                    "#content > fieldset > div:nth-child(3)");
        }
        return firstNameValue;
    }

    public final Label getLastNameValue() {
        if (lastNameValue == null) {
            lastNameValue = LabelImpl.getByCss(
                    "#content > fieldset > div:nth-child(5)");
        }
        return lastNameValue;
    }

    public final Label getBirthYearValue() {
        if (birthYearValue == null) {
            birthYearValue = LabelImpl.getByCss(
                    "#content > fieldset > div:nth-child(7)");
        }
        return birthYearValue;
    }

    public final Label getHeightValue() {
        if (heightValue == null) {
            heightValue = LabelImpl.getByCss(
                    "#content > fieldset > div:nth-child(9)");
        }
        return heightValue;
    }

    public final Label getWeightValue() {
        if (weightValue == null) {
            weightValue = LabelImpl.getByCss(
                    "#content > fieldset > div:nth-child(11)");
        }
        return weightValue;
    }

    public final Link getEditLink() {
        if (editLink == null) {
            editLink = LinkImpl.getByCss("#content > p > a:nth-child(1)");
        }
        return editLink;
    }

    public final Link getBackLink() {
        if (backLink == null) {
            backLink = LinkImpl.getByCss("#content > p > a:nth-child(2)");
        }
        return backLink;
    }

}
