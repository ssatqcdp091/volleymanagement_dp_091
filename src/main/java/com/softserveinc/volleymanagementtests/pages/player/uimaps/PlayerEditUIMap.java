package com.softserveinc.volleymanagementtests.pages.player.uimaps;

import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.TextInputImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

import java.util.List;

/**
 * UI Map contain all Edit Player locators and initializes controls using
 * Lazy initialization approach.
 *
 * @author Danil Zhyliaiev
 */
public class PlayerEditUIMap {
    private Label editPlayerLabel;
    private Label playerLabel;
    private Label firstNameLabel;
    private TextInput firstNameInput;
    private Label lastNameLabel;
    private TextInput lastNameInput;
    private Label birthYearLabel;
    private TextInput birthYearInput;
    private Label heightLabel;
    private TextInput heightInput;
    private Label weightLabel;
    private TextInput weightInput;
    private Button saveButton;
    private Link backToListLink;
    private Label nameErrorLabel;
    private Label lastNameErrorLabel;
    private Label birthYearErrorLabel;
    private Label birthYearValidationErrorLabel;
    private Label heightErrorLabel;
    private Label heightValidationErrorLabel;
    private Label weightErrorLabel;
    private Label weightValidationErrorLabel;
    private List<Label> errorLabels;

    public PlayerEditUIMap() {
        if (!WebDriverUtils.getCurrentUrl().contains("/Players/Edit/")) {
            throw new RuntimeException("This is not the 'Edit Player' page");
        }
    }

    public final Label getEditPlayerLabel() {
        if (editPlayerLabel == null) {
            editPlayerLabel = LabelImpl.getByCss("#content > h2");
        }

        return editPlayerLabel;
    }

    public final Label getPlayerLabel() {
        if (playerLabel == null) {
            playerLabel = LabelImpl
                    .getByCss("#content > form > fieldset > legend");
        }

        return playerLabel;
    }

    public final Label getFirstNameLabel() {
        if (firstNameLabel == null) {
            firstNameLabel = LabelImpl.getByCss("label[for=\"FirstName\"]");
        }
        return firstNameLabel;
    }

    public final TextInput getFirstNameInput() {
        if (firstNameInput == null) {
            firstNameInput = TextInputImpl.getById("FirstName");
        }

        return firstNameInput;
    }

    public final Label getLastNameLabel() {
        if (lastNameLabel == null) {
            lastNameLabel = LabelImpl.getByCss("label[for=\"LastName\"]");
        }

        return lastNameLabel;
    }

    public final TextInput getLastNameInput() {
        if (lastNameInput == null) {
            lastNameInput = TextInputImpl.getById("LastName");
        }

        return lastNameInput;
    }

    public final Label getBirthYearLabel() {
        if (birthYearLabel == null) {
            birthYearLabel = LabelImpl.getByCss("label[for=\"BirthYear\"]");
        }

        return birthYearLabel;
    }

    public final TextInput getBirthYearInput() {
        if (birthYearInput == null) {
            birthYearInput = TextInputImpl.getById("BirthYear");
        }

        return birthYearInput;
    }

    public final Label getHeightLabel() {
        if (heightLabel == null) {
            heightLabel = LabelImpl.getByCss("label[for=\"Height\"]");
        }

        return heightLabel;
    }

    public final TextInput getHeightInput() {
        if (heightInput == null) {
            heightInput = TextInputImpl.getById("Height");
        }

        return heightInput;
    }

    public final Label getWeightLabel() {
        if (weightLabel == null) {
            weightLabel = LabelImpl.getByCss("label[for=\"Weight\"]");
        }

        return weightLabel;
    }

    public final TextInput getWeightInput() {
        if (weightInput == null) {
            weightInput = TextInputImpl.getById("Weight");
        }

        return weightInput;
    }


    public final Button getSaveButton() {
        if (saveButton == null) {
            saveButton = ButtonImpl.getByCss("input[type=\"submit\"]");
        }

        return saveButton;
    }

    public final Link getBackToListLink() {
        if (backToListLink == null) {
            backToListLink = LinkImpl.getByCss("form + div > a");
        }

        return backToListLink;
    }

    public final Label getNameErrorLabel() {
        if (nameErrorLabel == null) {
            nameErrorLabel = LabelImpl.getById("FirstName-error");
        }
        return nameErrorLabel;
    }

    public final Label getLastNameErrorLabel() {
        if (lastNameErrorLabel == null) {
            lastNameErrorLabel = LabelImpl.getById("LastName-error");
        }

        return lastNameErrorLabel;
    }

    public final Label getBirthYearErrorLabel() {
        if (birthYearErrorLabel == null) {
            birthYearErrorLabel = LabelImpl.getById("BirthYear-error");
        }

        return birthYearErrorLabel;
    }

    public Label getBirthYearValidationErrorLabel() {
        if (birthYearValidationErrorLabel == null) {
            birthYearValidationErrorLabel = LabelImpl.getByCss("span[data-valmsg-for=BirthYear]");
        }

        return birthYearValidationErrorLabel;
    }

    public final Label getHeightErrorLabel() {
        if (heightErrorLabel == null) {
            heightErrorLabel = LabelImpl.getById("Height-error");
        }

        return heightErrorLabel;
    }

    public Label getHeightValidationErrorLabel() {
        if (heightValidationErrorLabel == null) {
            heightValidationErrorLabel = LabelImpl.getByCss("span[data-valmsg-for=Height]");
        }

        return heightValidationErrorLabel;
    }

    public final Label getWeightErrorLabel() {
        if (weightErrorLabel == null) {
            weightErrorLabel = LabelImpl.getById("Weight-error");
        }

        return weightErrorLabel;
    }

    public Label getWeightValidationErrorLabel() {
        if (weightValidationErrorLabel == null) {
            weightValidationErrorLabel = LabelImpl.getByCss("span[data-valmsg-for=Weight]");
        }

        return weightValidationErrorLabel;
    }

    public List<Label> getErrorLabels() {
        if (errorLabels == null) {
            errorLabels = WebElementsList.getByCss("[id$=error]").asLabels();
        }

        return errorLabels;
    }
}
