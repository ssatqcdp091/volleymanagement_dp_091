package com.softserveinc.volleymanagementtests.pages.player.uimaps;

import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.ButtonImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.TextInputImpl;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

/**
 * UI Map contain all Create Player locators and initializes controls using
 * lazy initialization approach.
 *
 * @author S.Tsyganovskiy
 */
public class PlayerCreateUIMap {

    private Label createNewPlayer;

    private Label player;

    private Label nameLabel;

    private TextInput nameInput;

    private Label lastNameLabel;

    private TextInput lastNameInput;

    private Label birthYearLabel;

    private TextInput birthYearInput;

    private Label heigthLabel;

    private TextInput heigthInput;

    private Label weigthLabel;

    private TextInput weigthInput;

    private Button createButton;

    private Link backToListLink;

    private Label errorMessageFirstNameInput;

    private Label errorMessageLastNameInput;

    private Label errorMessageBirthYearInput;

    private Label errorMessageHeigthInput;

    private Label errorMessageWeigthInput;


    public PlayerCreateUIMap() {

        if (!(WebDriverUtils.getCurrentUrl().contains("/Players/Create"))) {
            throw new RuntimeException("This is not \"Create player\" page.");
        }
    }

    public final Label getCreateNewPlayer() {
        if (createNewPlayer == null) {
            createNewPlayer = LabelImpl.getByCss("#content > h2");
        }
        return createNewPlayer;
    }


    public final Label getPlayer() {
        if (player == null) {
            player = LabelImpl.getByCss("fieldset > legend");
        }
        return player;
    }


    public final Label getNameLabel() {
        if (nameLabel == null) {
            nameLabel = LabelImpl.getByCss("[for=\"FirstName\"]");
        }
        return nameLabel;
    }


    public final TextInput getNameInput() {
        if (nameInput == null) {
            nameInput = TextInputImpl.getById("FirstName");
        }
        return nameInput;
    }


    public final Label getLastNameLabel() {
        if (lastNameLabel == null) {
            lastNameLabel = LabelImpl.getByCss("[for=\"LastName\"]");
        }
        return lastNameLabel;
    }


    public final TextInput getLastNameInput() {
        if (lastNameInput == null) {
            lastNameInput = TextInputImpl.getById("LastName");
        }
        return lastNameInput;
    }


    public final Label getBirthYearLabel() {
        if (birthYearLabel == null) {
            birthYearLabel = LabelImpl.getByCss("[for=\"BirthYear\"]");
        }
        return birthYearLabel;
    }


    public final TextInput getBirthYearInput() {
        if (birthYearInput == null) {
            birthYearInput = TextInputImpl.getById("BirthYear");
        }
        return birthYearInput;
    }


    public final Label getHeigthLabel() {
        if (heigthLabel == null) {
            heigthLabel = LabelImpl.getByCss("[for=\"Height\"]");
        }
        return heigthLabel;
    }


    public final TextInput getHeigthInput() {
        if (heigthInput == null) {
            heigthInput = TextInputImpl.getById("Height");
        }
        return heigthInput;
    }


    public final Label getWeigthLabel() {
        if (weigthLabel == null) {
            weigthLabel = LabelImpl.getByCss("[for=\"Weight\"]");
        }
        return weigthLabel;
    }


    public final TextInput getWeigthInput() {
        if (weigthInput == null) {
            weigthInput = TextInputImpl.getById("Weight");
        }
        return weigthInput;
    }


    public final Button getButtonCreate() {
        if (createButton == null) {
            createButton = ButtonImpl.getByCss("[type=\"submit\"]");
        }
        return createButton;
    }


    public final Link getBackToListLink() {
        if (backToListLink == null) {
            backToListLink = LinkImpl.getByCss("[href$=\"Players\"]");
        }
        return backToListLink;
    }


    // Getters for error messages


    public final Label getErrorMessageFirstNameInput() {
        if (errorMessageFirstNameInput == null) {
            errorMessageFirstNameInput = LabelImpl.getById("FirstName-error");
        }
        return errorMessageFirstNameInput;
    }


    public final Label getErrorMessageLastNameInput() {
        if (errorMessageLastNameInput == null) {
            errorMessageLastNameInput = LabelImpl.getById("LastName-error");
        }
        return errorMessageLastNameInput;
    }


    public final Label getErrorMessageBirthYearInput() {
        if (errorMessageBirthYearInput == null) {
            errorMessageBirthYearInput = LabelImpl.getById("BirthYear-error");
        }
        return errorMessageBirthYearInput;
    }


    public final Label getErrorMessageHeigthInput() {
        if (errorMessageHeigthInput == null) {
            errorMessageHeigthInput = LabelImpl.getById("Height-error");
        }
        return errorMessageHeigthInput;
    }


    public final Label getErrorMessageWeigthInput() {
        if (errorMessageWeigthInput == null) {
            errorMessageWeigthInput = LabelImpl.getById("Weight-error");
        }
        return errorMessageWeigthInput;
    }

}
