package com.softserveinc.volleymanagementtests.pages.player;

import com.softserveinc.volleymanagementtests.pages.player.uimaps.PlayersListUIMap;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

import java.util.*;

/**
 * Karabaza Anton on 23.02.2016.
 */
public class PlayersListPage {
    /**
     * Base PlayersListPage Url.
     */
    private static final String BASE_PLAYERS_LIST_URL
            = "https://109.206.43.99:44300/Players";
    /**
     * URL of first page when searching player.
     */
    private static final String START_PLAYERS_LIST_SEARCH_URL
            = "https://109.206.43.99:44300/Players?Length=5";


    /**
     * Object containing controls on page and their locators.
     */
    private PlayersListUIMap controls;

    /**
     * Default constructor, initializes page object with related UI map.
     */
    public PlayersListPage() {
        controls = new PlayersListUIMap();
    }

    /**
     * Types given string in Search field, submits it and refreshes
     * page elements.
     *
     * @param playerName player's first name or last name to search.
     * @return this {@code PlayersListPage} object.
     */
    public final PlayersListPage searchForPlayer(final String playerName) {
        controls.getSearchByTextInput().clear();
        controls.getSearchByTextInput().type(playerName);
        controls.getSearchByTextInput().submit();
        controls.remap();

        return this;
    }

    /**
     * Clicks CreateNewPlayerLink.
     *
     * @return new {@code PlayerCreatePage} object.
     */
    public final PlayerCreatePage createNewPlayer() {
        controls.getCreateNewPlayerLink().click();

        return new PlayerCreatePage();
    }

    /**
     * Verifies that the player with such full name exist
     * on current Players list page.
     *
     * @param player to check.
     * @return true - if payer exist, false - if not exist.
     */
    public final boolean isPlayerExistOnCurrentPage(final Player player) {
        List<PlayersListUIMap.PlayersTableRow> playersList =
                controls.getPlayersList();

        if (playersList.size() != 0) {
            for (PlayersListUIMap.PlayersTableRow playerTableRow : playersList) {
                if (playerTableRow.getFullNameLink().getText()
                        .equals(player.getLastName() + " " + player.getFirstName())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Finds player by full name in list on the page and clicks full
     * name link.
     *
     * @param fullName player's full name whose details are needed.
     * @return new {@code PlayerDetailsPage} object.
     */
    public final PlayerDetailsPage showPlayerDetails(final String fullName) {
        List<PlayersListUIMap.PlayersTableRow> playersList =
                controls.getPlayersList();

        for (PlayersListUIMap.PlayersTableRow playerTableRow : playersList) {
            if (playerTableRow.getFullNameLink().getText().equals(fullName)) {
                playerTableRow.getFullNameLink().click();
                return new PlayerDetailsPage();
            }
        }

        throw new NoSuchElementException(String.format(
                "There is no player with the name %s on the page", fullName));
    }

    /**
     * Finds player by full name in list on the page and clicks edit link.
     *
     * @param fullName player's full name to edit.
     * @return new {@code PlayerEditPage} object.
     */
    public final PlayerEditPage editPlayer(final String fullName) {
        List<PlayersListUIMap.PlayersTableRow> playersList =
                controls.getPlayersList();

        for (PlayersListUIMap.PlayersTableRow playerTableRow : playersList) {
            if (playerTableRow.getFullNameLink().getText().equals(fullName)) {
                playerTableRow.getEditLink().click();
                return new PlayerEditPage();
            }
        }

        throw new NoSuchElementException(String.format(
                "There is no player with the name %s on the page", fullName));
    }

    /**
     * Choose random player in list on page and clicks edit link.
     *
     * @return new {@code PlayerEditPage} object.
     */
    public final PlayerEditPage editRandomPlayer() {
        List<PlayersListUIMap.PlayersTableRow> playersList =
                controls.getPlayersList();

        playersList.get(new Random().nextInt(playersList.size()))
                .getEditLink()
                .click();

        return new PlayerEditPage();
    }

    /**
     * Choose player in list by given position and clicks edit link.
     *
     * @param position desired player position in list.
     * @return new {@code PlayerEditPage} object.
     */
    public final PlayerEditPage editPlayerByPosition(final int position) {
        List<PlayersListUIMap.PlayersTableRow> playersList =
                controls.getPlayersList();

        playersList.get(position - 1)
                .getEditLink()
                .click();

        return new PlayerEditPage();
    }

    /**
     * Choose random player in list on page and go to details page.
     *
     * @return new {@code PlayerEditPage} object.
     */
    public final PlayerDetailsPage showRandomPlayerDetails() {
        List<PlayersListUIMap.PlayersTableRow> playersList =
                controls.getPlayersList();

        playersList.get(new Random().nextInt(playersList.size()))
                .getFullNameLink()
                .click();

        return new PlayerDetailsPage();
    }

    public final PlayerDetailsPage showPlayerDetailsByPosition(final int position) {
        List<PlayersListUIMap.PlayersTableRow> playersList =
                controls.getPlayersList();

        playersList.get(position)
                .getFullNameLink()
                .click();

        return new PlayerDetailsPage();
    }

    /**
     * Click the delete link in the row with the player the id of is given
     * as params,
     * turns the driver to an Alert if present.
     * walks through the all paginatop-pages to find the player.
     * <p>
     * the NoAlertPresentException will be thrown if there is no Alert.
     *
     * @param idToFindAndClick the name of the player the row to click of
     * @return an alert instance of this page
     */
    public final Alert clickDeleteLink(final Integer idToFindAndClick) {

        boolean thereIsNextPage = true;
        while (thereIsNextPage) {
            try {
                return findAndClickPlayerDeleteLinkOnCurrentPageView(idToFindAndClick);
            } catch (NoSuchElementException e) {
                thereIsNextPage = goNextPage();
            }
        }

        throw new NoSuchElementException(String.format(
                "There is no player with the id of %s on the page", idToFindAndClick));
    }

    /**
     * Current paginator-page only.
     */
    public final Alert findAndClickPlayerDeleteLinkOnCurrentPageView(
            final Integer idToFindAndClick) {

        List<PlayersListUIMap.PlayersTableRow> playersList = controls.getPlayersList();

        for (PlayersListUIMap.PlayersTableRow playerTableRow : playersList) {
            Integer playerInRowId = Integer.parseInt(playerTableRow.getFullNameLink()
                    .getID().replace("playerName", ""));
            if (playerInRowId.equals(idToFindAndClick)) {

                playerTableRow.getDeleteLink().click();

                return getAlert();
            }
        }

        throw new NoSuchElementException(String.format(
                "There is no player with the id of %s on the page", idToFindAndClick));
    }


    /**
     * Counts number of players on current page.
     *
     * @return {@code int} players number.
     */
    public final int getCountOfPlayersOnCurrentPage() {
        return controls.getPlayersList().size();
    }

    /**
     * Gets string representation of current page index.
     *
     * @return new {@code String} object.
     */
    public final String getCurrentPageIndex() {
        String currentURL = WebDriverUtils.getCurrentUrl();

        if (currentURL.endsWith("Players")
                || currentURL.endsWith("Players?Length=5")) {
            return "1";
        } else {
            int startIndexOfPageValue =
                    currentURL.indexOf("page=") + "page=".length();
            int endIndexOfPageValue = currentURL.indexOf("&text");

            if (endIndexOfPageValue == -1) {
                return currentURL.substring(startIndexOfPageValue);
            }

            return currentURL.substring(startIndexOfPageValue, endIndexOfPageValue);
        }
    }

    /**
     * Turns the PlayersListPage to next page.
     *
     * @return true - if page changed, false - if still the same.
     */
    public final boolean goNextPage() {
        String currentPage = getCurrentPageIndex();
        String pageToGo = String.valueOf(Integer.parseInt(currentPage) + 1);
        List<Link> pageLinks = getPaginator();

        for (Link pageLink : pageLinks) {
            if (pageLink.getText().equals(pageToGo)) {
                pageLink.click();
                controls.remap();
                return true;
            }
        }

        return false;
    }

    /**
     * Turns the PlayersListPage to previous page.
     *
     * @return true - if page changed, false - if still the same.
     */
    public final boolean goPreviousPage() {
        String currentPage = getCurrentPageIndex();
        String pageToGo = String.valueOf(Integer.parseInt(currentPage) - 1);
        List<Link> pageLinks = getPaginator();

        for (Link pageLink : pageLinks) {
            if (pageLink.getText().equals(pageToGo)) {
                pageLink.click();
                controls.remap();
                return true;
            }
        }

        return false;
    }

    /**
     * Turns the PlayersListPage to specific page.
     *
     * @param pageToGo page is needed to move on.
     * @return @return true - if page changed, false - if still the same.
     */
    public final boolean goToPage(final int pageToGo) {
        List<Link> pageLinks = getPaginator();
        int lastPageIndex;
        if (pageLinks.size() != 0) {
            lastPageIndex = Integer.parseInt(pageLinks
                    .get(pageLinks.size() - 1).getText());
        } else {
            lastPageIndex = 1;
        }

        if (pageToGo > lastPageIndex || pageToGo <= 0) {
            throw new NoSuchElementException();
        }

        String stringToGo = String.format("page=%d", pageToGo);
        String currentPageString = String.format("page=%s",
                getCurrentPageIndex());
        String currentURL = WebDriverUtils.getCurrentUrl();

        switch (currentURL) {
            case BASE_PLAYERS_LIST_URL:
                WebDriverUtils.load(String.format("%s?%s",
                        BASE_PLAYERS_LIST_URL, stringToGo));
                break;
            case START_PLAYERS_LIST_SEARCH_URL:
                String searchString = controls.getSearchByTextInput().getText();
                WebDriverUtils.load(String.format("%s?%s&textToSearch=%s",
                        BASE_PLAYERS_LIST_URL, stringToGo, searchString));
                break;
            default:
                WebDriverUtils.load(currentURL.replaceFirst(currentPageString,
                        stringToGo));
        }

        controls.remap();
        return true;
    }

    /**
     * Gets players full names on current page.
     *
     * @return players - List of full names.
     */
    public List<String> getPlayersOnCurrentPage() {
        List<String> players = new ArrayList<>();
        List<PlayersListUIMap.PlayersTableRow> playersList =
                controls.getPlayersList();

        for (PlayersListUIMap.PlayersTableRow playerTableRow : playersList) {
            players.add(playerTableRow.getFullNameLink().getText());
        }

        return players;
    }

    /**
     * @return players - Map[id=>fullName]
     */
    public Map<Integer, String> getPlayersMapWithIdAndFullNameOnCurrentPage() {
        Map<Integer, String> playersIdNameMap = new HashMap<>();

        List<PlayersListUIMap.PlayersTableRow> playersRowsList = controls.getPlayersList();

        for (PlayersListUIMap.PlayersTableRow row : playersRowsList) {
            playersIdNameMap.put(
                    Integer.parseInt(row.getFullNameLink().getID().replace("playerName", "")),
                    row.getFullNameLink().getText()
            );
        }
        return playersIdNameMap;
    }

    /**
     * Gets players list page main header as label.
     *
     * @return main header as {@code LabelImpl} object.
     */
    public final Label getPlayersLabel() {
        return controls.getPlayersLabel();
    }

    /**
     * Gets SearchByText input.
     *
     * @return SearchByText input as {@code TextInputImpl} object.
     */
    public final TextInput getSearchByTextInput() {
        return controls.getSearchByTextInput();
    }

    /**
     * Gets CreateNewPlayer link.
     *
     * @return CreateNewPlayer as {@code LinkImpl} object.
     */
    public final Link getCreateNewPlayerLink() {
        return controls.getCreateNewPlayerLink();
    }

    /**
     * Gets page links on table footer.
     *
     * @return List of {@code LinkImpl} objects.
     */
    public final List<Link> getPaginator() {
        return controls.getPaginator();
    }

    /**
     * Turns the driver to an Alert if present.
     * <p>
     * The NoAlertPresentException will be thrown if there is no Alert.
     *
     * @return an alert instance if this page
     */
    public final Alert getAlert() {
        return controls.getAlert();
    }

    public int getPlayerPageIndexAt(int playerIdToSearch) {
        goToPage(1);
        List<Link> paginator = controls.getPaginator();
        if (paginator.size() == 0) {
            return 1;
        }
        int notVisitedPages = Integer.parseInt(paginator.get(paginator.size() - 1).getText());

        while (notVisitedPages > 0) {
            try {
                List<PlayersListUIMap.PlayersTableRow> playersList = controls.getPlayersList();

                for (PlayersListUIMap.PlayersTableRow playerTableRow : playersList) {
                    Integer playerInRowId = Integer.parseInt(playerTableRow.getFullNameLink()
                            .getID().replace("playerName", ""));
                    if (playerInRowId.equals(playerIdToSearch)) {
                        return Integer.parseInt(getCurrentPageIndex());
                    }
                }

                throw new NoSuchElementException(String.format(
                        "There is no player with the id of %s on the page", playerIdToSearch));

            } catch (NoSuchElementException e) {
                goNextPage();
            }
            notVisitedPages -= 1;
        }
        throw new NoSuchElementException(String.format(
                "There is no player with the id of %s on the PlayersListPage", playerIdToSearch));
    }
}
