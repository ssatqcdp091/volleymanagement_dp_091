package com.softserveinc.volleymanagementtests.pages.player.uimaps;

import com.softserveinc.volleymanagementtests.pages.contracts.Remapable;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.AlertImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.LinkImpl;
import com.softserveinc.volleymanagementtests.tools.controls.TextInputImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

import java.util.ArrayList;
import java.util.List;

/**
 * Karabaza Anton on 23.02.2016.
 */
public class PlayersListUIMap implements Remapable {

    /**
     * Represents main header on Players list page.
     */
    private Label playersLabel;
    /**
     * Represents label near SearchByText text-box on Edit Players list page.
     */
    private Label searchByTextLabel;
    /**
     * Represents SearchByText text-box on Players list page.
     */
    private TextInput searchByTextInput;
    /**
     * Represents Create new player link on Players list page.
     */
    private Link createNewPlayerLink;
    /**
     * Represents list of players on current Players list page.
     */
    private List<PlayersTableRow> playersList;
    /**
     * Represents list of page links on Players list page.
     */
    private List<Link> paginator;
    /**
     * an alert popup on the page.
     */
    private Alert alert;

    /**
     * Constructor. Verifies that user is on Players list page.
     *
     * @throws RuntimeException thrown if user is not on Players list page.
     */
    public PlayersListUIMap() {
        if (!(WebDriverUtils.getCurrentUrl().endsWith("Players")
                || WebDriverUtils.getCurrentUrl().contains("Players?page")
                || WebDriverUtils.getCurrentUrl().contains("Players?Length"))) {
            throw new RuntimeException("This is not Players List page.");
        }
    }

    /**
     * Gets players list page main header as label.
     *
     * @return main header as {@code LabelImpl} object.
     */
    public final Label getPlayersLabel() {
        if (playersLabel == null) {
            playersLabel = LabelImpl.getByCss("#content > h2");
        }
        return playersLabel;
    }

    /**
     * Gets label near SearchByText input.
     *
     * @return main header as {@code LabelImpl} object.
     */
    public final Label getSearchByTextLabel() {
        if (searchByTextLabel == null) {
            searchByTextLabel = LabelImpl.getByCss(
                    "#content > div > form > label");
        }
        return searchByTextLabel;
    }

    /**
     * Gets SearchByText input.
     *
     * @return SearchByText input as {@code TextInputImpl} object.
     */
    public final TextInput getSearchByTextInput() {

        if (searchByTextInput == null) {
            searchByTextInput = TextInputImpl.getById("textToSearch");
        }
        return searchByTextInput;
    }

    /**
     * Gets CreateNewPlayer link.
     *
     * @return CreateNewPlayer as {@code LinkImpl} object.
     */
    public final Link getCreateNewPlayerLink() {
        if (createNewPlayerLink == null) {
            createNewPlayerLink = LinkImpl.getByCss("#content > a");
        }
        return createNewPlayerLink;
    }

    /**
     * Gets list of PlayersTableRow on current Players list page.
     *
     * @return list of {@code PlayersTableRow} objects.
     */
    public final List<PlayersTableRow> getPlayersList() {

        if (playersList == null) {
            playersList = new ArrayList<>();
            List<Link> fullNameLinks = WebElementsList.getByCss(".playerName")
                    .asLinks();
            List<Link> editLinks = WebElementsList.getByCss(
                    "tr > td + td > a:not(.delete)")
                    .asLinks();
            List<Link> deleteLinks = WebElementsList.getByCss("td .delete")
                    .asLinks();

            for (int i = 0; i < fullNameLinks.size(); i++) {
                playersList.add(new PlayersTableRow(fullNameLinks.get(i),
                        editLinks.get(i), deleteLinks.get(i)));
            }
        }

        return playersList;
    }

    /**
     * Gets page links on table footer.
     *
     * @return List of {@code LinkImpl} objects.
     */
    public final List<Link> getPaginator() {
        if (paginator == null) {
            paginator = new ArrayList<>();
            paginator = WebElementsList.getByCss(
                    "#content > a:not(:nth-child(3))")
                    .asLinks();
        }
        return paginator;
    }

    /**
     * turns the driver to an Alert if present.
     * <p>
     * the NoAlertPresentException will be thrown if there is no Alert.
     *
     * @return an alert instance if this page
     */
    public final Alert getAlert() {
        alert = AlertImpl.get();
        return alert;
    }

    @Override
    public void remap() {
        playersLabel = null;
        searchByTextLabel = null;
        searchByTextInput = null;
        createNewPlayerLink = null;
        playersList = null;
        paginator = null;
        alert = null;
    }

    /**
     * Class that represents one row in players table, which contains player's
     * fullNameLink, editLink and deleteLink.
     */
    public class PlayersTableRow {

        /**
         * Represets full name link of the player.
         */
        private Link fullNameLink;
        /**
         * Represents edit link for the player.
         */
        private Link editLink;
        /**
         * Represents delete link for the player.
         */
        private Link deleteLink;

        /**
         * Constructor.
         *
         * @param fullName fullNameLink of the player.
         * @param edit     editLink for the player.
         * @param delete   deleteLink for the player.
         */
        PlayersTableRow(final Link fullName, final Link edit, final Link delete) {
            fullNameLink = fullName;
            editLink = edit;
            deleteLink = delete;
        }

        /**
         * Gets Full name link of the player's row.
         *
         * @return full name as {@code LinkImpl} object.
         */
        public final Link getFullNameLink() {
            return fullNameLink;
        }

        /**
         * Gets edit link of the player's row.
         *
         * @return editLink as {@code LinkImpl} object.
         */
        public final Link getEditLink() {
            return editLink;
        }

        /**
         * Gets delete link of the player's row.
         *
         * @return deleteLink as {@code LinkImpl} object.
         */
        public final Link getDeleteLink() {
            return deleteLink;
        }
    }
}
