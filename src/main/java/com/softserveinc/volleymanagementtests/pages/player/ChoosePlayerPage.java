package com.softserveinc.volleymanagementtests.pages.player;

import com.softserveinc.volleymanagementtests.pages.player.uimaps.ChoosePlayersUIMap;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

import java.util.List;

/**
 * UI Map contain all Create Player locators and initializes controls using
 * lazy initialization approach.
 *
 * @author S.Tsyganovskiy
 */

public class ChoosePlayerPage {
    private ChoosePlayersUIMap controls;

    public ChoosePlayerPage() {
        controls = new ChoosePlayersUIMap();
    }


    public Label getFirstAndLastName() {
        return controls.getFirstAndLastName();
    }


    public Label getChoosePlayer() {
        return controls.getChoosePlayer();
    }

    public List<ChoosePlayersUIMap.PlayersTableRow> getPlayersTableRows() {
        return controls.getPlayersTableRows();
    }

    public ChoosePlayerPage addPlayerToTeam(String lastNameAndFirstName) {
        List<ChoosePlayersUIMap.PlayersTableRow> playersTableRows = controls.getPlayersTableRows();
        for (ChoosePlayersUIMap.PlayersTableRow i : playersTableRows) {
            if (i.getLastAndFirstNameDetailsLabel().getText().equals(lastNameAndFirstName)) {
                i.getAddPlayerToTeamButton().submit();
            }
        }
        return this;
    }

    public List<Link> getPaginatior() {
        return controls.getPaginator();
    }

    private boolean goNextPage() {
        List<Link> paginationLinks = controls.getPaginator();
        boolean isNextPageExists = false;
        for (Link i : paginationLinks) {
            if (i.getText().equals("»")) {
                isNextPageExists = true;
                i.click();
                controls.remap();
            }
        }
        return isNextPageExists;
    }


    public void chooseCaptain(Player player) {
        List<ChoosePlayersUIMap.PlayersTableRow> playersTableRowList;
        do {
            playersTableRowList = controls.getPlayersTableRows();
            for (ChoosePlayersUIMap.PlayersTableRow i : playersTableRowList) {
                if (i.getLastAndFirstNameDetailsLabel().getText()
                        .equals(player.getLastName() + " " + player.getFirstName())) {
                    i.submitAddPlayerToTeamButton();
                    WebDriverUtils.switchToNewWindow();
                    return;
                }
            }
        } while (goNextPage());
    }

    public void choosePlayers(List<Player> list) {
        List<ChoosePlayersUIMap.PlayersTableRow> playersTableRowList;
        do {
            playersTableRowList = controls.getPlayersTableRows();
            for (ChoosePlayersUIMap.PlayersTableRow i : playersTableRowList) {
                for (Player j : list) {
                    if (i.getLastAndFirstNameDetailsLabel().getText()
                            .equals(j.getLastName() + " " + j.getFirstName())) {
                        i.submitAddPlayerToTeamButton();
                    }
                }
            }

        } while (goNextPage());
        WebDriverUtils.getDriver().close();
        WebDriverUtils.switchToNewWindow();
    }

}
