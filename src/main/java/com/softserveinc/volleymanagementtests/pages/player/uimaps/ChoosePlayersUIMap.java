package com.softserveinc.volleymanagementtests.pages.player.uimaps;

import com.softserveinc.volleymanagementtests.pages.contracts.Remapable;
import com.softserveinc.volleymanagementtests.tools.controls.LabelImpl;
import com.softserveinc.volleymanagementtests.tools.controls.WebElementsList;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

import java.util.ArrayList;
import java.util.List;

/**
 * UI Map contain all Create Player locators and initializes controls using
 * lazy initialization approach.
 *
 * @author S.Tsyganovskiy
 */
public class ChoosePlayersUIMap implements Remapable {
    private Label choosePlayer;
    private Label firstAndLastName;
    private List<PlayersTableRow> playersTableRows;
    private List<Link> paginator;


    public ChoosePlayersUIMap() {
    }

    public Label getChoosePlayer() {
        if (choosePlayer == null) {
            choosePlayer = LabelImpl.getByCss("#content>h2");
        }
        return choosePlayer;
    }

    public Label getFirstAndLastName() {
        if (firstAndLastName == null) {
            firstAndLastName = LabelImpl.getByCss(".table>tbody>tr>th:first-child");
        }
        return firstAndLastName;
    }

    public List<PlayersTableRow> getPlayersTableRows() {
        if (playersTableRows == null) {
            getPaginator();
            playersTableRows = new ArrayList<>();
            List<Label> listOfPlayers = WebElementsList.getByCss("tr>td:first-child").asLabels();
            List<Button> addPlayerButtons = WebElementsList.getByCss("td>input").asButtons();

            for (int i = 0; i < listOfPlayers.size(); i++) {
                playersTableRows.add(new PlayersTableRow(listOfPlayers.get(i), addPlayerButtons.get(i)));
            }
        }

        return playersTableRows;
    }


    public List<Link> getPaginator() {
        if (paginator == null) {
            paginator = WebElementsList.getByCss("a[onclick^=\"change\"]").asLinks();
        }
        return paginator;
    }

    public class PlayersTableRow {

        private Label firstAndLastNameDetails;
        private Button addPlayerToTeam;

        public PlayersTableRow(Label firstAndLastNameDetails, Button addPlayerToTeam) {
            this.firstAndLastNameDetails = firstAndLastNameDetails;
            this.addPlayerToTeam = addPlayerToTeam;
        }

        public Button getAddPlayerToTeamButton() {
            return addPlayerToTeam;
        }

        public void submitAddPlayerToTeamButton(){
            getAddPlayerToTeamButton().submit();
        }

        public Label getLastAndFirstNameDetailsLabel() {
            return firstAndLastNameDetails;
        }
    }

    @Override
    public void remap() {
        choosePlayer = null;
        firstAndLastName = null;
        playersTableRows = null;
        paginator = null;
    }
}
