package com.softserveinc.volleymanagementtests.pages.player;

import com.softserveinc.volleymanagementtests.pages.player.uimaps.PlayerDetailsUIMap;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.TimeoutException;

/**
 * Object that represents Player Details page and provide methods to interact
 * with it.
 *
 * @author J.Bodnar
 */
public class PlayerDetailsPage {
    /**
     * Object containing controls of the page.
     */
    private PlayerDetailsUIMap controls;

    /**
     * Default constructor, initializes page object with related UI map.
     */
    public PlayerDetailsPage() {
        controls = new PlayerDetailsUIMap();
    }

    public final Label getFirstNameValue() {
        return controls.getFirstNameValue();
    }

    public final Label getLastNameValue() {
        return controls.getLastNameValue();
    }

    public final Label getBirthYearValue() {
        return controls.getBirthYearValue();
    }

    public final Label getHeightValue() {
        return controls.getHeightValue();
    }

    public final Label getWeightValue() {
        return controls.getWeightValue();
    }

    public final PlayerEditPage clickEditLink() {
        controls.getEditLink().click();
        return new PlayerEditPage();
    }

    public final PlayersListPage clickBackLink() {
        controls.getBackLink().click();
        return new PlayersListPage();
    }

    public final Player readPlayer() {
        String birthYear = "";
        String height = "";
        String weight = "";

        try {
            birthYear = getBirthYearValue().getText();
        } catch (TimeoutException ignored) {
        }

        try {
            height = getHeightValue().getText();
        } catch (TimeoutException ignored) {
        }

        try {
            weight = getWeightValue().getText();
        } catch (TimeoutException ignored) {
        }

        return Player.newBuilder()
                .setFirstName(getFirstNameValue().getText())
                .setLastName(getLastNameValue().getText())
                .setBirthYear(birthYear)
                .setHeight(height)
                .setWeight(weight).build();

    }

}
