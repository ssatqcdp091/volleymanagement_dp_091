package com.softserveinc.volleymanagementtests.pages;

import com.softserveinc.volleymanagementtests.specification.TestConfig;

/**
 * System under test page locators are stored here.
 * in order the possibility the web driver to go directly to the
 * specified page location (not "getting the base url->clicking links" to reach the needed page)
 * is present.
 */
public final class PageUrls {

    public static final String PLAYER_CREATE_URL =
            String.format("%s/Players/Create", TestConfig.getSystemUnderTestBaseUrl());

    public static final String TEAM_CREATE_URL =
            String.format("%s/Teams/Create", TestConfig.getSystemUnderTestBaseUrl());

    public static final String PLAYERS_LIST_URL =
            String.format("%s/Players", TestConfig.getSystemUnderTestBaseUrl());

    public static final String TEAMS_LIST_URL =
            String.format("%s/Teams", TestConfig.getSystemUnderTestBaseUrl());

    public static final String CONTRIBUTORS_LIST_URL =
            String.format("%s/ContributorsTeam", TestConfig.getSystemUnderTestBaseUrl());

    private PageUrls() {
    }
}
