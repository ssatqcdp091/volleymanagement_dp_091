package com.softserveinc.volleymanagementtests.pages.contracts;

/**
 * @author  Karabaza Anton.
 */
public interface Remapable {
    /**
     * Remap page elements to use correct elements if page was changed.
     */
    void remap();
}
