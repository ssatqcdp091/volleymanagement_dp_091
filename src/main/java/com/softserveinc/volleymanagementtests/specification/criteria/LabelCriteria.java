package com.softserveinc.volleymanagementtests.specification.criteria;
/**
 * Author S.Tsiganovskiy
 */

import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.NotFoundException;

/**
 * contains the methods which are the way the control can be validated.
 * and the mechanism for the writing down the validation errors to the
 * specification.
 */
public final class LabelCriteria implements Specifiable {
    /**
     * control to be validated with the <code>Criteria</code>.
     */
    private Label label;
    /**
     * the Specification the validation result will be added to.
     */
    private Specification specification;

    /**
     * Constructor.
     *
     * @param aLabel         control to be validated with the
     *                       <code>Criteria</code>.
     * @param aSpecification the validation result will be added to it
     */
    private LabelCriteria(final Label aLabel,
                          final Specification aSpecification) {
        if (aLabel == null) {
            throw new NotFoundException("the income Label == null");
        }

        label = aLabel;
        specification = aSpecification;
    }

    /**
     * in order to provide the "fluent api".
     *
     * @param aLabel         control to be validated with the
     *                       <code>Criteria</code>.
     * @param aSpecification the validation result will be added to it
     * @return LabelCriteria
     */
    public static LabelCriteria get(final Label aLabel,
                                    final Specification aSpecification) {
        return new LabelCriteria(aLabel, aSpecification);
    }

    /**
     * verify if the text of the label is as expected.
     *
     * @param expectedResult String to compare with
     * @return LabelCriteria
     */
    public LabelCriteria textMatch(final String expectedResult) {
        String actualResult = label.getText();
        specification.add(actualResult.equals(expectedResult),
                String.format("%s:%n  expected [%s]%n but found [%s]; ",
                        "The Label text does not match",
                        expectedResult, actualResult));
        return this;
    }

    /**
     * verify if the label is visible.
     *
     * @return LabelCriteria
     */
    public LabelCriteria isVisible() {
        specification.add(label.isDisplayed(),
                "The Label is not visible.");
        return this;
    }

    @Override
    public Specification next() {
        return specification;
    }
}
