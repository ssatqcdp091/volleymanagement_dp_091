package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import org.openqa.selenium.NotFoundException;

import java.util.List;

/**
 * @author Danil Zhyliaiev
 */
public final class ListCriteria implements Specifiable {
    private List list;
    private Specification specification;

    private ListCriteria(final List list,
                         final Specification aSpecification) {
        if (list == null) {
            throw new NotFoundException("the income List == null");
        }

        this.list = list;
        specification = aSpecification;
    }

    public static ListCriteria get(final List aList,
                                   final Specification aSpecification) {
        return new ListCriteria(aList, aSpecification);
    }

    public ListCriteria containsAll(final List expectedList) {
        specification.add(list.containsAll(expectedList),
                String.format("%s:%n  expected [%s]%n but found [%s]; ",
                        "The List does not contain all the elements",
                        expectedList, list));
        return this;
    }

    public ListCriteria listSizeEquals(final int expectedSize) {
        int actualSize = list.size();
        specification.add(actualSize == expectedSize,
                String.format("%s:%n  expected [%d]%n but found [%d]; ",
                        "The List has wrong size",
                        expectedSize, actualSize));
        return this;
    }

    public ListCriteria labelTextsMatch(String expectedText) {
        for (Object label : list) {
            specification.add(((Label) label).getText().equals(expectedText),
                    String.format("%s:%n  expected [%s]%n but found [%s]; ",
                            "Labels text doesn't match ",
                            expectedText, ((Label) label).getText()));
        }
        return this;
    }

    @Override
    public Specification next() {
        return this.specification;
    }
}
