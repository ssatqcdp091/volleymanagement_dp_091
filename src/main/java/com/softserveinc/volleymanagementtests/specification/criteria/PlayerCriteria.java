package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;

/**
 * Criteria to compare and verify player test data.
 *
 * @author Danil Zhyliaiev
 */
public class PlayerCriteria implements Specifiable {
    private Player player;
    private Specification specification;

    private PlayerCriteria(final Player player, final Specification specification) {
        if (player == null) {
            throw new IllegalArgumentException("Incoming player can't be null");
        }

        this.player = player;
        this.specification = specification;
    }

    public static PlayerCriteria get(final Player player, final Specification specification) {
        return new PlayerCriteria(player, specification);
    }

    public PlayerCriteria equalsTo(Player anotherPlayer) {
        specification.add(player.equals(anotherPlayer),
                String.format("%s:%n expected [%s]%n but found [%s];",
                        "Players are not equal", player.toString(), anotherPlayer.toString()));
        return this;
    }

    public Specification next() {
        return specification;
    }
}
