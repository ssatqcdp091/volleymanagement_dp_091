package com.softserveinc.volleymanagementtests.specification.criteria;

/**
 * Author S.Tsiganovskiy
 */
import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import org.openqa.selenium.NotFoundException;


public class CreatePlayerCriteria implements Specifiable {
    private PlayerCreatePage playerCreatePage;
    private Specification specification;

    public CreatePlayerCriteria(PlayerCreatePage playerCreatePage, Specification specification) {
        if (playerCreatePage == null) {
            throw new NotFoundException("the income PlayerCreatePage == null");
        }
        this.playerCreatePage = playerCreatePage;
        this.specification = specification;
    }

    public static CreatePlayerCriteria get(PlayerCreatePage playerCreatePage, Specification specification) {
        return new CreatePlayerCriteria(playerCreatePage, specification);
    }


    public CreatePlayerCriteria isAllFieldsFilledInCorrectly(Player player) {
        boolean allFieldsFilledInCorrectly = true;
        if (!playerCreatePage.getInputFirstName().getText().equals(player.getFirstName())
                && playerCreatePage.getInputLastName().getText().equals(player.getLastName())
                && playerCreatePage.getInputBirthYear().getText().equals(player.getBirthYear())
                && playerCreatePage.getInputHeight().getText().equals(player.getHeight())
                && playerCreatePage.getInputWeight().getText().equals(player.getWeight())) {
            allFieldsFilledInCorrectly = false;
        }

        specification.add(allFieldsFilledInCorrectly, String.format("The player's fields  %s, %s, %s, %s, %s"
                        + " aren't filled in correctly.",player.getFirstName(), player.getLastName()
                , player.getBirthYear(),player.getHeight(),player.getWeight()));

        return this;
    }

    @Override
    public Specification next() {
        return this.specification;
    }
}



