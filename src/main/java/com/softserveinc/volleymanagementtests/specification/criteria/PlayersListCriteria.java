package com.softserveinc.volleymanagementtests.specification.criteria;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.pages.player.PlayerDetailsPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specifiable;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.DalToPlayer;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NotFoundException;

import java.util.List;

/**
 * Karabaza Anton on 09.03.2016.
 */
public class PlayersListCriteria implements Specifiable {
    private PlayersListPage playersListPage;
    private Specification specification;

    private PlayersListCriteria(PlayersListPage playersListPage, Specification specification) {
        if (playersListPage == null) {
            throw new NotFoundException("the income PlayersListPage == null");
        }
        this.playersListPage = playersListPage;
        this.specification = specification;
    }

    public static PlayersListCriteria get(PlayersListPage playersListPage,
                                          Specification specification) {
        return new PlayersListCriteria(playersListPage, specification);
    }

    public PlayersListCriteria isPlayerDisplayedOnList(PlayerDal player) {
        boolean isPlayerDisplayed;

        do {
            isPlayerDisplayed = playersListPage.isPlayerExistOnCurrentPage(
                    DalToPlayer.getInstance().map(player));
            if (isPlayerDisplayed) {
                playersListPage.goToPage(1);
                break;
            }
        }
        while (playersListPage.goNextPage());

        specification.add(isPlayerDisplayed, String.format("The player %s %s isn't displayed on"
                + "PlayersListPage:%n expected [%s]%n found [%s]", player.getLastName(),
                player.getFirstName(), "true", "false"));
        return this;
    }

    public PlayersListCriteria isPlayerDisplayedOnListWithCorrectDetails(PlayerDal player) {
        boolean isPlayerDisplayed = false;
        List<String> players;
        PlayerDetailsPage detailsPage;

        do {
            players = playersListPage.getPlayersOnCurrentPage();

            for (int i = 0; i < playersListPage.getCountOfPlayersOnCurrentPage(); i++) {
                if (players.get(i).equals(player.getLastName() + " " + player.getFirstName())) {
                    detailsPage = playersListPage.showPlayerDetailsByPosition(i);
                    Player readPlayer = detailsPage.readPlayer();

                    playersListPage = detailsPage.clickBackLink();

                    if (readPlayer.equals(DalToPlayer.getInstance().map(player))) {
                        isPlayerDisplayed = true;
                        playersListPage.goToPage(1);
                        break;
                    }
                }
            }
        }
        while (!isPlayerDisplayed && playersListPage.goNextPage());

        specification.add(isPlayerDisplayed, String.format("The player %s %s isn't displayed "
                        + "with correct details:%n expected [%s]%n found [%s]", player.getLastName(),
                player.getFirstName(), "true", "false"));
        return this;
    }

    public PlayersListCriteria matchPageIndex(String pageIndex) {
        specification.add(pageIndex.equals(playersListPage.getCurrentPageIndex()),
                String.format("The PlayersListPage isn't on expected page"
                                + ":%n expected [%s]%n found [%s]", pageIndex,
                        playersListPage.getCurrentPageIndex()));
        return this;
    }

    public PlayersListCriteria matchURL(String pageURL) {
        specification.add(WebDriverUtils.getCurrentUrl().equals(pageURL),
                String.format("The PlayersListPage isn't on expected page"
                                + ":%n expected [%s]%n found [%s]", pageURL,
                        WebDriverUtils.getCurrentUrl()));
        return this;
    }

    public final PlayersListCriteria ifUnexpectedAlertPresent() {
        try {
            playersListPage.getAlert();
            specification.add(false, "There is an unexpected alert present on the page.");
        } catch (NoAlertPresentException e) {
            return this;
        }
        return this;
    }

    @Override
    public Specification next() {
        return this.specification;
    }
}
