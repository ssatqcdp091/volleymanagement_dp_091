package com.softserveinc.volleymanagementtests.specification;

/**
 * @author Danil Zhyliaiev
 */

import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.criteria.*;
import com.softserveinc.volleymanagementtests.testdata.contributor.ContributorTeam;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.*;

import java.util.List;

/**
 * Contains the <code>StringBuilder summaryDescription</code> which is to store
 * all the error messages which are generated during the test case running
 * and the mechanism to build the <code>summaryDescription</code>.
 */
public final class Specification {

    /**
     * if there NO error occurred.
     */
    private boolean summaryResult;
    /**
     * all the error messages which are generated during the test case running.
     */
    private StringBuilder summaryDescription;

    /**
     * Constructor.
     * primary initialization of the fields.
     */
    private Specification() {
        summaryResult = true;
        summaryDescription = new StringBuilder();
    }

    /**
     * @return the new Specification instance
     * in order to provide the "fluent api".
     */
    public static Specification get() {
        return new Specification();
    }

    /**
     * Accessor.
     *
     * @return <code>summaryResult</code>.
     * true  if there NO  error occurred,
     * false if there any error occurred
     */
    public boolean getPassed() {
        return summaryResult;
    }

    /**
     * Accessor.
     *
     * @return <code>summaryDescription</code> as a String
     */
    public String getDescription() {
        return summaryDescription.toString();
    }

    /**
     * Changes the Specification object state.
     * Should be called after every test step.
     * <p>
     * adds the potentially occurred error
     * to the <code>summaryDescription</code>
     *
     * @param pass      true if it is not an error (test step result is valid).
     * @param errorText error message.
     */
    public void add(final boolean pass, final String errorText) {
        summaryResult = summaryResult && pass;

        if (!pass) {
            summaryDescription.append(errorText).append(System.getProperty("line.separator"));
        }
    }

    /**
     * should be called at the end of the test case in order to validate
     * if there any error occurred during the test case running.
     * <p>
     * if there any error occurred during the running the test case, the
     * "?AssertationError?" will be thrown with the
     * <code>summaryDescription</code> as an errorMessage.
     */
    public void check() {
        if (!summaryResult) {
            throw new AssertionError(summaryDescription.toString());
        }
    }

    /**
     * @param checkbox control to get Criteria for.
     * @return CheckboxCriteria.
     */
    public CheckboxCriteria forThe(final Checkbox checkbox) {
        return CheckboxCriteria.get(checkbox, this);
    }

    /**
     * @param dropdown control to get Criteria for.
     * @return DropdownCriteria.
     */
    public DropdownCriteria forThe(final Dropdown dropdown) {
        return DropdownCriteria.get(dropdown, this);
    }

    /**
     * @param label control to get Criteria for.
     * @return LabelCriteria.
     */
    public LabelCriteria forThe(final Label label) {
        return LabelCriteria.get(label, this);
    }

    /**
     * @param link control to get Criteria for.
     * @return LinkCriteria.
     */
    public LinkCriteria forThe(final Link link) {
        return LinkCriteria.get(link, this);
    }

    /**
     * @param textInput control to get Criteria for.
     * @return TextInputCriteria.
     */
    public TextInputCriteria forThe(final TextInput textInput) {
        return TextInputCriteria.get(textInput, this);
    }

    /**
     * @param button control to get Criteria for.
     * @return ButtonCriteria.
     */
    public ButtonCriteria forThe(final Button button) {
        return ButtonCriteria.get(button, this);
    }

    /**
     * @param list controls list to get Criteria for.
     * @return ListCriteria.
     */
    public ListCriteria forThe(final List list) {
        return ListCriteria.get(list, this);
    }

    /**
     * @param digit Double to get Criteria for.
     * @return DigitCriteria.
     */
    public DigitCriteria forThe(final Double digit) {
        return DigitCriteria.get(digit, this);
    }

    /**
     * @param aBoolean to get Criteria for.
     * @return BooleanCriteria.
     */
    public BooleanCriteria forThe(final boolean aBoolean) {
        return BooleanCriteria.get(aBoolean, this);
    }

    /**
     * @param anAlert to get Criteria for.
     * @return AlertCriteria
     */
    public AlertCriteria forThe(final Alert anAlert) {
        return AlertCriteria.get(anAlert, this);
    }

    public PlayersListCriteria forThe(final PlayersListPage aPlayersListPage) {
        return PlayersListCriteria.get(aPlayersListPage, this);
    }

    public CreatePlayerCriteria forThe(final PlayerCreatePage aPlayerCreatePage) {
        return CreatePlayerCriteria.get(aPlayerCreatePage, this);
    }

    public CreateTeamCriteria forThe(final TeamCreatePage aTeamCreatePage) {
        return CreateTeamCriteria.get(aTeamCreatePage, this);
    }

    public TeamCriteria forThe(final Team team) {
        return TeamCriteria.get(team, this);
    }

    public PlayerCriteria forThe(final Player player) {
        return PlayerCriteria.get(player, this);
    }

    public ContributorTeamCriteria forThe(final ContributorTeam contributorTeam) {
        return ContributorTeamCriteria.get(contributorTeam, this);
    }

    public TeamListCriteria forThe(final TeamListPage aTeamListPage) {
        return TeamListCriteria.get(aTeamListPage, this);
    }
}
