package com.softserveinc.volleymanagementtests.specification;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * the class contains getters of the parameters out of the .properties file
 * which are used to specify the potentially various tools and the settings
 * to run the framework and system under test.
 */
public final class TestConfig {

    private TestConfig() {
    }

    public static String getSystemUnderTestBaseUrl() {
        String relativeFileLocation = "config.properties";
        Properties property = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(relativeFileLocation)) {
            property.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String systemUnderTestBaseUrl =
                property.getProperty("systemUnderTestBaseUrl");

        return systemUnderTestBaseUrl;

    }

    public static String getJDBCDriver() {
        String relativeFileLocation = "config.properties";
        Properties property = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(relativeFileLocation)) {
            property.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String jdbcDriver =
                property.getProperty("jdbcDriver");

        return jdbcDriver;
    }

    public static String getDBConnectionString() {

        String relativeFileLocation = "config.properties";
        Properties property = new Properties();

        try (FileInputStream fileInputStream = new FileInputStream(relativeFileLocation)) {
            property.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String dbConnectionString = String.format(
                "jdbc:jtds:sqlserver://%s/%s;instance=%s;user=%s;password=%s",
                property.getProperty("db.host"),
                property.getProperty("db.name"),
                property.getProperty("db.instance"),
                property.getProperty("db.user"),
                property.getProperty("db.password")
        );

        return dbConnectionString;
    }
}
