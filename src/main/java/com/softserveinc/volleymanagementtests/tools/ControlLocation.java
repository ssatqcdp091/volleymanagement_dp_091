package com.softserveinc.volleymanagementtests.tools;

import org.openqa.selenium.By;


/**
 * Wrapper for the location the elements within a document.
 * the constructor is private in order to ControlLocation class work as a tool
 * (set of methods)
 * contains methods to search elements within a document.
 *
 * @author J.Bodnar on 11.02.2016.
 */
public final class ControlLocation {

    /**
     * Mechanism used to locate elements within a document.
     */
    private By by;
    /**
     * specified locator (css/xpath/id/name ...).
     */
    private String value;

    /**
     * constructor with initialization the ControlLocation fields.
     * the constructor is private in order to ControlLocation class work as
     * a tool (set of methods)
     * <p>
     * avoid creating the ControlLocation object with using the
     * "new" word from the outside (instance can me created inside the
     * ControlLocation only) and reject the extension of the
     * ControlLocation object.
     *
     * @param specifiedBy    specifically located WebElement
     * @param specifiedValue specific locator (css/xpath/id/name ...).
     */
    private ControlLocation(final By specifiedBy, final String specifiedValue) {
        by = specifiedBy;
        value = specifiedValue;
    }

    /**
     * creates the ControlLocation instance using the <code>By.id()</code> as a
     * specifiedBy param.
     *
     * @param id -string value of an id attribute of the element(control) to get
     * @return ControlLocation element got by id
     */
    public static ControlLocation getById(final String id) {
        return new ControlLocation(By.id(id), id);
    }

    /**
     * creates the ControlLocation instance using the <code>By.name()</code>
     * as a specifiedBy param.
     *
     * @param searchName -string value of a name attribute of the
     *                   element(control) to get
     * @return ControlLocation element got by name
     */
    public static ControlLocation getByName(final String searchName) {
        return new ControlLocation(By.name(searchName), searchName);
    }

    /**
     * creates the ControlLocation instance using the <code>By.xpath()</code>
     * as a specifiedBy param.
     *
     * @param xpathExpression -string value of a xpathExpression of the
     *                        element(control) to get
     * @return ControlLocation element got by xpathExpression
     */
    public static ControlLocation getByXPath(final String xpathExpression) {
        return new ControlLocation(By.xpath(xpathExpression), xpathExpression);
    }

    /**
     * creates the ControlLocation instance using the
     * <code>By.cssSelector()</code> as a specifiedBy param.
     *
     * @param cssSelector -string value of a cssSelector of the element(control)
     *                    to get
     * @return ControlLocation element got by cssSelector
     */
    public static ControlLocation getByCss(final String cssSelector) {
        return new ControlLocation(By.cssSelector(cssSelector), cssSelector);
    }

    /**
     * creates the ControlLocation instance using the <code>By.linkText()</code>
     * as a specifiedBy param.
     *
     * @param linkText -string value of a linkText attribute of
     *                 the element(control) to get
     * @return ControlLocation element got by linkText
     */
    public static ControlLocation getByLinkText(final String linkText) {
        return new ControlLocation(By.linkText(linkText), linkText);
    }

    /**
     * accessor.
     *
     * @return value field
     */
    public String getValue() {
        return value;
    }

    /**
     * accessor.
     *
     * @return by field
     */
    public By getBy() {
        return by;
    }
}
