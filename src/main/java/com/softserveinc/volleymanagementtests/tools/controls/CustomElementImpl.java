package com.softserveinc.volleymanagementtests.tools.controls;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.ControlLocation;
import com.softserveinc.volleymanagementtests.tools.ControlWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.CustomElement;

/**
 * Class to represent custom div and elements.
 * <p>
 * implementation of the CustomElement control for the web API.
 *
 * @author Danil Zhyliaiev
 */
public class CustomElementImpl implements CustomElement {

    /**
     * control wrapped as CustomElement.
     */
    private ControlWrapper control;

    /**
     * Constructor.
     * initialization of the <code>control</code> field.
     * <p>
     * could be private but is protected because of the usage in the
     * WebElementsList class.
     *
     * @param controlToWrap control to wrap as CustomElement.
     */
    protected CustomElementImpl(final ControlWrapper controlToWrap) {
        control = controlToWrap;
    }

    /**
     * gets the visible CustomElement using the By mechanism
     * with getById().
     *
     * @param id - specific xpathExpression String
     * @return CustomElement got by id
     */
    public static CustomElement getById(final String id) {
        return get(ControlLocation.getById(id));
    }

    /**
     * gets the visible CustomElement using the By mechanism
     * with getByName().
     *
     * @param searchedName - specific searchedName String
     * @return CustomElement got by searchedName
     */
    public static CustomElement getByName(final String searchedName) {
        return get(ControlLocation.getByName(searchedName));
    }

    /**
     * gets the visible CustomElement using the By mechanism
     * with getByLinkText().
     *
     * @param linkText - specific linkText String
     * @return CustomElement got by linkText
     */
    public static CustomElement getByLinkText(final String linkText) {
        return get(ControlLocation.getByLinkText(linkText));
    }

    /**
     * gets the visible CustomElement using the By mechanism
     * with getByXPath().
     *
     * @param xpathExpression - specific xpathExpression String
     * @return CustomElement got by xpathExpression
     */
    public static CustomElement getByXpath(final String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    /**
     * gets the visible CustomElement using the By mechanism
     * with getByCss().
     *
     * @param cssSelector - specific cssSelector String
     * @return CustomElement got by cssSelector
     */
    public static CustomElement getByCss(final String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    /**
     * gets the CustomElement which is found by controlLocation
     * via ContextVisible and wrapped with ControlWrapper.
     *
     * @param controlLocation - element to get from the page.
     * @return the CustomElement which is found by controlLocation
     * via ContextVisible and wrapped with ControlWrapper.
     */
    private static CustomElement get(final ControlLocation controlLocation) {
        return new CustomElementImpl(new ControlWrapper(ContextVisible.get()
                .get(controlLocation)));
    }

    @Override
    public final void click() {
        control.click();
    }

    @Override
    public final void clear() {
        control.clear();
    }

    @Override
    public final void submit() {
        control.submit();
    }

    @Override
    public final boolean isSelected() {
        return control.isSelected();
    }

    @Override
    public final boolean isEnabled() {
        return control.isEnabled();
    }

    @Override
    public final boolean isDisplayed() {
        return control.isDisplayed();
    }

    @Override
    public final String getText() {
        return control.getText();
    }

    @Override
    public final void sendKeys(final String text) {
        control.sendKeys(text);
    }
}
