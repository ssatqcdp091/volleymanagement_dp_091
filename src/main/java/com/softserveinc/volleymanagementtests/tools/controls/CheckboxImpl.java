package com.softserveinc.volleymanagementtests.tools.controls;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.ControlLocation;
import com.softserveinc.volleymanagementtests.tools.ControlWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Checkbox;

/**
 * implementation of the Checkbox control for the web API.
 */
public class CheckboxImpl implements Checkbox {

    /**
     * control wrapped as Checkbox.
     */
    private ControlWrapper control;

    /**
     * Constructor.
     * initialization of the <code>control</code> field.
     * <p>
     * could be private but is protected because of the usage in the
     * WebElementsList class.
     *
     * @param controlToWrapAsCheckbox control to wrap as Checkbox.
     */
    protected CheckboxImpl(final ControlWrapper controlToWrapAsCheckbox) {
        this.control = controlToWrapAsCheckbox;
    }

    /**
     * gets the Checkbox which is found by controlLocation via ContextVisible
     * and wrapped with ControlWrapper.
     *
     * @param controlLocation - element to get from the page.
     * @return the Checkbox which is found by controlLocation via ContextVisible
     * and wrapped with ControlWrapper.
     */
    public static Checkbox get(final ControlLocation controlLocation) {
        return new CheckboxImpl(new ControlWrapper(ContextVisible.get()
                .get(controlLocation)));
    }

    /**
     * gets the visible Checkbox using the By mechanism
     * with getByCss().
     *
     * @param cssSelector - specific cssSelector String
     * @return Checkbox got by cssSelector
     */
    public static Checkbox getByCss(final String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    /**
     * gets the visible Checkbox using the By mechanism
     * with getByXPath().
     *
     * @param xpathExpression - specific xpathExpression String
     * @return Checkbox got by xpathExpression
     */
    public static Checkbox getByXpath(final String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    /**
     * gets the visible Checkbox using the By mechanism
     * with getById().
     *
     * @param id - specific id String
     * @return Checkbox got by id
     */
    public static Checkbox getById(final String id) {
        return get(ControlLocation.getById(id));
    }

    @Override
    public final void checkOff() {
        if (!control.get().isSelected()) {
            control.click();
        }
    }

    @Override
    public final void unCheck() {
        if (control.get().isSelected()) {
            control.click();
        }
    }

    @Override
    public final boolean isCheckedOff() {
        return control.isSelected();
    }

    @Override
    public final boolean isEnabled() {
        return control.isEnabled();
    }
}
