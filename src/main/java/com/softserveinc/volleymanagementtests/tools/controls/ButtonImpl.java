package com.softserveinc.volleymanagementtests.tools.controls;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.ControlLocation;
import com.softserveinc.volleymanagementtests.tools.ControlWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;

/**
 * implementation of the Button control for the web API.
 */
public class ButtonImpl implements Button {
    /**
     * control wrapped as Button.
     */
    private ControlWrapper control;

    /**
     * Constructor.
     * initialization of the <code>control</code> field.
     * <p>
     * could be private but is protected because of the usage in the
     * WebElementsList class.
     *
     * @param controlToWrapAsButton control to wrap as Button.
     */
    protected ButtonImpl(final ControlWrapper controlToWrapAsButton) {
        control = controlToWrapAsButton;
    }


    /**
     * gets the visible Button using the By mechanism
     * with getByXPath().
     *
     * @param xpathExpression - specific xpathExpression String
     * @return Button got by xpathExpression
     */
    public static Button getByXpath(final String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    /**
     * gets the visible Button using the By mechanism
     * with getById().
     *
     * @param id - specific id String
     * @return Button got by id
     */
    public static Button getById(final String id) {
        return get(ControlLocation.getById(id));
    }

    /**
     * gets the visible Button using the By mechanism
     * with getByName().
     *
     * @param searchedName - specific searchedName String
     * @return Button got by searchedName
     */
    public static Button getByName(final String searchedName) {
        return get(ControlLocation.getByName(searchedName));
    }

    /**
     * gets the visible Button using the By mechanism
     * with getByCss().
     *
     * @param cssSelector - specific cssSelector String
     * @return Button got by cssSelector
     */
    public static Button getByCss(final String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    /**
     * gets the visible Button using the By mechanism
     * with getByLinkText().
     *
     * @param linkText - specific linkText String
     * @return Button got by linkText
     */
    public static Button getByLinkText(final String linkText) {
        return get(ControlLocation.getByLinkText(linkText));
    }

    /**
     * gets the Button which is found by controlLocation via ContextVisible and
     * wrapped with ControlWrapper.
     *
     * @param controlLocation - element to get from the page.
     * @return the Button which is found by controlLocation via ContextVisible
     * and wrapped with ControlWrapper.
     */
    private static Button get(final ControlLocation controlLocation) {
        return new ButtonImpl(new ControlWrapper(ContextVisible.get()
                .get(controlLocation)));
    }

    @Override
    public final boolean isDisplayed() {
        return control.isDisplayed();
    }

    @Override
    public final boolean isEnabled() {
        return control.isEnabled();
    }

    @Override
    public final String getText() {
        return control.getValue();
    }

    @Override
    public final void submit() {
        control.submit();
    }
}
