package com.softserveinc.volleymanagementtests.tools.controls;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.ControlLocation;
import com.softserveinc.volleymanagementtests.tools.ControlWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

/**
 * implementation of the TextInput control for the web API.
 *
 * @author J.Bodnar on 11.02.2016.
 */
public class TextInputImpl implements TextInput {

    /**
     * control wrapped as TextInput.
     */
    private ControlWrapper control;

    /**
     * Constructor.
     * initialization of the <code>control</code> field.
     * <p>
     * could be private but is protected because of the usage in the
     * WebElementsList class.
     *
     * @param controlToWrap - control to wrap as link.
     */
    protected TextInputImpl(final ControlWrapper controlToWrap) {
        this.control = controlToWrap;
    }

    /**
     * gets the visible TextInput using the By mechanism
     * with getByXPath().
     *
     * @param xpathExpression - specific xpathExpression String
     * @return TextInput got by xpathExpression
     */
    public static TextInput getByXpath(final String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    /**
     * gets the visible TextInput using the By mechanism
     * with getById().
     *
     * @param id - specific id String
     * @return TextInput got by id
     */
    public static TextInput getById(final String id) {
        return get(ControlLocation.getById(id));
    }

    /**
     * gets the visible TextInput using the By mechanism
     * with getByName().
     *
     * @param name - specific name String
     * @return TextInput got by name
     */
    public static TextInput getByName(final String name) {
        return get(ControlLocation.getByName(name));
    }

    /**
     * gets the visible TextInput using the By mechanism
     * with getByCss().
     *
     * @param cssSelector - specific cssSelector String
     * @return TextInput got by cssSelector
     */
    public static TextInput getByCss(final String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    /**
     * gets the visible TextInput using the By mechanism
     * with getByLinkText().
     *
     * @param linkText - specific linkText String
     * @return TextInput got by linkText
     */
    public static TextInput getByLinkText(final String linkText) {
        return get(ControlLocation.getByLinkText(linkText));
    }

    /**
     * gets the TextInput which is found by controlLocation via ContextVisible
     * and wrapped with ControlWrapper.
     *
     * @param controlLocation - element to get from the page.
     * @return the TextInput which is found by controlLocation via
     * ContextVisible and wrapped with ControlWrapper.
     */
    private static TextInput get(final ControlLocation controlLocation) {
        return new TextInputImpl(new ControlWrapper(ContextVisible.get()
                .get(controlLocation)));
    }

    @Override
    public final boolean isDisplayed() {
        return control.isDisplayed();
    }

    @Override
    public final void clear() {
        control.clear();
    }

    @Override
    public final void type(final String text) {
        control.sendKeys(text);
    }

    @Override
    public final void submit() {
        control.submit();
    }

    @Override
    public final void click() {
        control.click();
    }

    @Override
    public final String getText() {
        return control.getValue();
    }
}

