package com.softserveinc.volleymanagementtests.tools.controls;


import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.ControlLocation;
import com.softserveinc.volleymanagementtests.tools.ControlWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;

/**
 * implementation of the Dropdown control for the web API.
 *
 * @author J.Bodnar on 11.02.2016.
 */
public class LabelImpl implements Label {

    /**
     * control to wrap as a label.
     */
    private ControlWrapper control;

    /**
     * Constructor.
     * initialization of the <code>control</code> field.
     * could be private but is protected because of the usage in the
     * WebElementsList class.
     *
     * @param controlToWrap - control to wrap as a Label.
     */
    protected LabelImpl(final ControlWrapper controlToWrap) {
        control = controlToWrap;
    }

    /**
     * gets the visible Label using the By mechanism
     * with getByXPath().
     *
     * @param xpathExpression - specific xpathExpression String
     * @return Label got by xpathExpression
     */
    public static Label getByXpath(final String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    /**
     * gets the visible Label using the By mechanism
     * with getById().
     *
     * @param id - specific id String
     * @return Label got by id
     */
    public static Label getById(final String id) {
        return get(ControlLocation.getById(id));
    }

    /**
     * gets the visible Label using the By mechanism
     * with getByName().
     *
     * @param searchedName - specific searchedName String
     * @return Label got by searchedName
     */
    public static Label getByName(final String searchedName) {
        return get(ControlLocation.getByName(searchedName));
    }

    /**
     * gets the visible Label using the By mechanism
     * with getByCss().
     *
     * @param cssSelector - specific cssSelector String
     * @return Label got by cssSelector
     */
    public static Label getByCss(final String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    /**
     * gets the visible Label using the By mechanism
     * with getByLinkText().
     *
     * @param linkText - specific linkText String
     * @return Label got by linkText
     */
    public static Label getByLinkText(final String linkText) {
        return get(ControlLocation.getByLinkText(linkText));
    }

    /**
     * gets the Label which is found by controlLocation via ContextVisible and
     * wrapped with ControlWrapper.
     *
     * @param controlLocation - element to get from the page.
     * @return the Label which is found by controlLocation via ContextVisible
     * and wrapped with ControlWrapper.
     */
    private static Label get(final ControlLocation controlLocation) {
        return new LabelImpl(new ControlWrapper(ContextVisible.get()
                .get(controlLocation))) {
        };

    }

    @Override
    public final String getText() {
        return control.getText();
    }

    @Override
    public final boolean isDisplayed() {
        return control.isDisplayed();
    }
}
