package com.softserveinc.volleymanagementtests.tools.controls;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.ControlLocation;
import com.softserveinc.volleymanagementtests.tools.SelectWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Dropdown;

import java.util.List;

/**
 * implementation of the Dropdown control for the web API.
 *
 * @author Danil Zhyliaiev
 */
public class DropdownImpl implements Dropdown {

    /**
     * select wrapped as Dropdown.
     */
    private SelectWrapper select;

    /**
     * Constructor.
     * initialization of the <code>select</code> field.
     * <p>
     * could be private but is protected because of the usage in the
     * WebElementsList class.
     *
     * @param selectToWrap select to wrap as Dropdown.
     */
    protected DropdownImpl(final SelectWrapper selectToWrap) {
        select = selectToWrap;
    }

    /**
     * gets the visible Dropdown using the By mechanism
     * with getById().
     *
     * @param id - specific id String
     * @return Dropdown got by id
     */
    public static Dropdown getById(final String id) {
        return get(ControlLocation.getById(id));
    }

    /**
     * gets the visible Dropdown using the By mechanism
     * with getByName().
     *
     * @param searchName - specific searchName String
     * @return Dropdown got by searchName
     */
    public static Dropdown getByName(final String searchName) {
        return get(ControlLocation.getByName(searchName));
    }

    /**
     * gets the visible Dropdown using the By mechanism
     * with getByCss().
     *
     * @param cssSelector - specific cssSelector String
     * @return Dropdown got by cssSelector
     */
    public static Dropdown getByCss(final String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    /**
     * gets the visible Dropdown using the By mechanism
     * with getByXPath().
     *
     * @param xpathExpression - specific xpathExpression String
     * @return Dropdown got by xpathExpression
     */

    public static Dropdown getByXPath(final String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    /**
     * gets the Dropdown which is found by controlLocation via ContextVisible
     * and wrapped with ControlWrapper.
     *
     * @param controlLocation - element to get from the page.
     * @return the Dropdown which is found by controlLocation via ContextVisible
     * and wrapped with ControlWrapper.
     */
    private static Dropdown get(final ControlLocation controlLocation) {
        return new DropdownImpl(new SelectWrapper(
                ContextVisible.get().get(controlLocation)));
    }

    @Override
    public final String getSelectedOptionIndex() {
        return select.getSelectedOptionIndex();
    }

    @Override
    public final String getSelectedOptionValue() {
        return select.getSelectedOptionValue();
    }

    @Override
    public final String getSelectedOptionText() {
        return select.getSelectedOptionText();
    }

    @Override
    public final List<String> getOptions() {
        return select.getOptions();
    }

    @Override
    public final boolean isEnabled() {
        return select.isEnabled();
    }

    @Override
    public final void selectByIndex(final int index) {
        select.selectByIndex(index);
    }

    @Override
    public final void selectByValue(final String value) {
        select.selectByValue(value);
    }

    @Override
    public final void selectByVisibleText(final String text) {
        select.selectByVisibleText(text);
    }
}
