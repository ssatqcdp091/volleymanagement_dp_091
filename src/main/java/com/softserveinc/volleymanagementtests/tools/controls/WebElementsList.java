package com.softserveinc.volleymanagementtests.tools.controls;


import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.ControlListWrapper;
import com.softserveinc.volleymanagementtests.tools.ControlLocation;
import com.softserveinc.volleymanagementtests.tools.ControlWrapper;
import com.softserveinc.volleymanagementtests.tools.SelectWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Button;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Checkbox;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.CustomElement;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Dropdown;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Label;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.TextInput;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to handle and represent list of ControlWrapper's as list of
 * needed Controls.
 *
 * @author Danil Zhyliaiev
 */
public final class WebElementsList {

    /**
     * the List of wrapped Controls.
     */
    private List<ControlWrapper> controls;

    /**
     * The list of Controls.
     */
    private ControlListWrapper controlListWrapper;

    /**
     * Constructor.
     * <code>fields</code> initialization.
     *
     * @param aControlListWrapper list to be manipulated with.
     */
    private WebElementsList(final ControlListWrapper aControlListWrapper) {
        controlListWrapper = aControlListWrapper;
        controls = controlListWrapper.get();
    }

    /**
     * gets the visible WebElementsList using the By mechanism
     * with getById().
     *
     * @param id - specific id String
     * @return WebElementsList got by id
     */
    public static WebElementsList getById(final String id) {
        return get(ControlLocation.getById(id));
    }

    /**
     * gets the visible WebElementsList using the By mechanism
     * with getByName().
     *
     * @param searchName - specific searchName String
     * @return WebElementsList got by searchName
     */
    public static WebElementsList getByName(final String searchName) {
        return get(ControlLocation.getByName(searchName));
    }

    /**
     * gets the visible WebElementsList using the By mechanism
     * with getByCss().
     *
     * @param cssSelector - specific cssSelector String
     * @return WebElementsList got by cssSelector
     */
    public static WebElementsList getByCss(final String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    /**
     * gets the visible WebElementsList using the By mechanism
     * with getByXPath().
     *
     * @param xpathExpression - specific xpathExpression String
     * @return WebElementsList got by xpathExpression
     */
    public static WebElementsList getByXPath(final String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    /**
     * gets the WebElementsList which is found by controlLocation via
     * ContextVisible and wrapped with ControlListWrapper.
     *
     * @param controlLocation - element to get from the page.
     * @return the WebElementsList which is found by controlLocation via
     * ContextVisible and wrapped with ControlListWrapper.
     */
    private static WebElementsList get(final ControlLocation controlLocation) {
        return new WebElementsList(new ControlListWrapper(
                ContextVisible.get().getElements(controlLocation)));
    }

    /**
     * @return List*#60Button> got from the <code>controls</code>
     */
    public List<Button> asButtons() {
        List<Button> buttons = new ArrayList<>();

        for (ControlWrapper control : controls) {
            buttons.add(new ButtonImpl(control));
        }

        return buttons;
    }

    /**
     * @return List*#60Checkbox> got from the <code>controls</code>
     */
    public List<Checkbox> asCheckboxes() {
        List<Checkbox> checkboxes = new ArrayList<>();

        for (ControlWrapper control : controls) {
            checkboxes.add(new CheckboxImpl(control));
        }

        return checkboxes;
    }

    /**
     * @return List*#60Dropdown> got from the <code>controls</code>
     */
    public List<Dropdown> asDropdowns() {
        List<Dropdown> dropdowns = new ArrayList<>();

        for (SelectWrapper control : controlListWrapper.getSelects()) {
            dropdowns.add(new DropdownImpl(control));
        }

        return dropdowns;
    }

    /**
     * @return List*#60Label> got from the <code>controls</code>
     */
    public List<Label> asLabels() {
        List<Label> labels = new ArrayList<>();

        for (ControlWrapper control : controls) {
            labels.add(new LabelImpl(control));
        }

        return labels;
    }

    /**
     * @return List*#60Link> got from the <code>controls</code>
     */
    public List<Link> asLinks() {
        List<Link> links = new ArrayList<>();

        for (ControlWrapper control : controls) {
            links.add(new LinkImpl(control));
        }

        return links;
    }

    /**
     * @return List*#60TextInput> got from the <code>controls</code>
     */
    public List<TextInput> asTextInputs() {
        List<TextInput> textInputs = new ArrayList<>();

        for (ControlWrapper control : controls) {
            textInputs.add(new TextInputImpl(control));
        }

        return textInputs;
    }

    /**
     * @return List*#60CustomElement> got from the <code>controls</code>
     */
    public List<CustomElement> asCustomElements() {
        List<CustomElement> textInputs = new ArrayList<>();

        for (ControlWrapper control : controls) {
            textInputs.add(new CustomElementImpl(control));
        }

        return textInputs;
    }
}
