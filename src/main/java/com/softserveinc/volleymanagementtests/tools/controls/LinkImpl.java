/**
 * Author S.Tsiganovskiy
 */
package com.softserveinc.volleymanagementtests.tools.controls;

import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.ControlLocation;
import com.softserveinc.volleymanagementtests.tools.ControlWrapper;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Link;

/**
 * implementation of the Checkbox control for the web API.
 */
public class LinkImpl implements Link {
    /**
     * control wrapped as link.
     */
    private ControlWrapper control;

    /**
     * Constructor.
     * initialization of the <code>control</code> field.
     * <p>
     * could be private but is protected because of the usage in the
     * WebElementsList class.
     *
     * @param controlToWrapAsLink - control to wrap as link.
     */
    protected LinkImpl(final ControlWrapper controlToWrapAsLink) {
        control = controlToWrapAsLink;
    }

    /**
     * gets the visible Link using the By mechanism
     * with getById().
     *
     * @param id - specific id String
     * @return Link got by id
     */
    public static Link getById(final String id) {
        return get(ControlLocation.getById(id));
    }

    /**
     * gets the visible Link using the By mechanism
     * with getByName().
     *
     * @param searchedName - specific searchedName String
     * @return Link got by searchedName
     */
    public static Link getByName(final String searchedName) {
        return get(ControlLocation.getByName(searchedName));
    }

    /**
     * gets the visible Link using the By mechanism
     * with getByLinkText().
     *
     * @param linkText - specific linkText String
     * @return Link got by linkText
     */
    public static Link getByLinkText(final String linkText) {
        return get(ControlLocation.getByLinkText(linkText));
    }

    /**
     * gets the visible Link using the By mechanism
     * with getByXPath().
     *
     * @param xpathExpression - specific xpathExpression String
     * @return Link got by xpathExpression
     */
    public static Link getByXpath(final String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    /**
     * gets the visible Link using the By mechanism
     * with getByCss().
     *
     * @param cssSelector - specific cssSelector String
     * @return Link got by cssSelector
     */
    public static Link getByCss(final String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    /**
     * gets the Link which is found by controlLocation via ContextVisible and
     * wrapped with ControlWrapper.
     *
     * @param controlLocation - element to get from the page.
     * @return the Link which is found by controlLocation via ContextVisible
     * and wrapped with ControlWrapper.
     */
    public static Link get(final ControlLocation controlLocation) {
        return new LinkImpl(new ControlWrapper(ContextVisible.get()
                .get(controlLocation)));
    }

    @Override
    public final String getHref() {
        return control.getAttribute("href");
    }

    @Override
    public String getID() {
        return control.getAttribute("id");
    }

    @Override
    public final void click() {
        control.click();
    }

    @Override
    public final String getText() {
        return control.getText();
    }
}
