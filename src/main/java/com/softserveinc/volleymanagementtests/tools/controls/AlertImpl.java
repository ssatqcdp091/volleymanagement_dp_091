package com.softserveinc.volleymanagementtests.tools.controls;

import com.softserveinc.volleymanagementtests.tools.AlertWrapper;
import com.softserveinc.volleymanagementtests.tools.ContextVisible;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert;

/**
 * implementation of the Alert popup.
 */
public final class AlertImpl implements Alert {

    /**
     * Alert to handle.
     */
    private AlertWrapper alert;

    /**
     * Constructor.
     * initialization of the <code>alert</code> field.
     *
     * @param anAlert popup to wrap as Alert.
     */
    private AlertImpl(final AlertWrapper anAlert) {
        alert = anAlert;
    }

    /**
     * turns the driver to an Alert if present,
     * wraps it for the further usage of it.
     * <p>
     * the NoAlertPresentException will be thrown if there is no Alert.
     *
     * @return an alert instance if this page
     */
    public static Alert get() {
        return new AlertImpl(new AlertWrapper(ContextVisible.get().getAlert()));
    }

    @Override
    public void accept() {
        alert.accept();
    }

    @Override
    public void dismiss() {
        alert.dismiss();
    }

    @Override
    public String getText() {
        return alert.getText();
    }
}
