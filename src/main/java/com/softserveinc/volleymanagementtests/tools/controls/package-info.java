/**
 * Controls are stored in this package.
 * Controls classes represents the base web elements on the page
 * or elements of the desktop API. in order the PageObject could be used
 * on any implementation of the API(desktop/web/android...)
 * <p>
 * Controls implementations can be readAllPlayers as a wrapper over the
 * ControlWrapper (which is the wrapper over the any WebElement
 * (abstract control))
 * which gives the control the actual and specific functionality for it.
 */
package com.softserveinc.volleymanagementtests.tools.controls;
