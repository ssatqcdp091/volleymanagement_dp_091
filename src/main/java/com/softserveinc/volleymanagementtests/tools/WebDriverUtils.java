package com.softserveinc.volleymanagementtests.tools;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * The main class for the WebDriver initialization and WebDriver configuration.
 * contains the methods to access the WebDriver instance
 */
public final class WebDriverUtils {
    /**
     * timeout in seconds.
     * time to start to search for the element after.
     */
    public static final int ABSENCE_TIMEOUT = 5;
    /**
     * implicit timeout in seconds.
     * time to wait for the element is loaded on the page.
     */
    private static final long IMPLICITLY_WAIT_TIMEOUT = 4;
    /**
     * contains a WebDriver object.
     */
    private static WebDriver driver;

    /**
     * suppressed constructor.
     * in order to avoid creating the WebDriverUtils object with using the
     * "new" word from the outside (instance can me created inside the
     * WebDriverUtils only) and reject the extension of the
     * WebDriverUtils object.
     */
    private WebDriverUtils() {
    }

    /**
     * creating and configuring the WebDriver object.
     * accessor, singleton.
     *
     * @return FirefoxDriver instance
     */
    public static WebDriver getDriver() {
        if (driver == null) {
            driver = new FirefoxDriver();
            driver.manage().timeouts().implicitlyWait(
                    getImplicitlyWaitTimeout(), TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
        return driver;
    }

    /**
     * getter fo the implicitlyWaitTimeout.
     * accessor.
     *
     * @return long implicitlyWaitTimeout
     */
    static long getImplicitlyWaitTimeout() {
        return IMPLICITLY_WAIT_TIMEOUT;
    }

    /**
     * loads the specified page(url) to the WebDriver.
     *
     * @param path url to navigate driver to, better to use fully qualified.
     */
    public static void load(final String path) {
        getDriver().navigate().to(path);
    }

    /**
     * Close the current window,
     * quitting the browser if it's the last window currently open.
     */
    public static void stop() {
        if (driver != null) {
            try {
                driver.close();
            } catch (UnhandledAlertException e) {
                String errorMesseme = e.getMessage();
                while (!errorMesseme.equals("")) {
                    try {
                        driver.close();
                        errorMesseme = "";
                    } catch (UnhandledAlertException e1) {
                        errorMesseme = e1.getMessage();
                    }
                }
                driver = null;
                throw new UnhandledAlertException(e.getMessage());
            }
        }
        driver = null;
    }

    /**
     * Refresh the current page.
     */
    public static void refresh() {
        getDriver().navigate().refresh();
    }

    /**
     * Get a string representing the current URL that the browser is looking at.
     *
     * @return the URL of the page currently loaded in the browser
     */
    public static String getCurrentUrl() {
        return getDriver().getCurrentUrl();
    }

    /**
     * gets the title of the current page.
     *
     * @return the title of the current page,
     * with leading and trailing whitespace stripped,
     * or null if one is not already set
     */
    public static String getTitle() {
        return getDriver().getTitle();
    }

    public static void switchToNewWindow() {
        java.util.Set<String> handles = driver.getWindowHandles();
        for (String handle1 : driver.getWindowHandles()) {
            driver.switchTo().window(handle1);
            driver.manage().window().maximize();

        }
    }

    /**
     * Method takes screenshot and add it to Allure report.
     *
     * @param name  test method name as {@code String}. Used by annotation.
     * @return      screenshot as {@code byte[]}.
     */
    @Attachment(value = "{0}", type = "image/png")
    static byte[] takeAllureScreenshot(String name) {
        return ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
    }
}
