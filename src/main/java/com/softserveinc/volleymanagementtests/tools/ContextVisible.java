package com.softserveinc.volleymanagementtests.tools;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * a tool to get the visible WebElement from the document(page).
 *
 * @author Danil Zhyliaiev
 */
public final class ContextVisible {
    /**
     * suppressed constructor.
     * in order to avoid creating the ContextVisible object with using the
     * "new" word from the outside (instance can me created inside the
     * ContextVisible only) and reject the extension of the
     * ContextVisible object.
     */
    private ContextVisible() {
    }

    /**
     * creates new instance of the ContextVisible object.
     *
     * @return ContextVisible instance
     */
    public static ContextVisible get() {
        return new ContextVisible();
    }

    /**
     * if the current document(page) contains the element specified by income
     * <code>ControlLocation</code> it will be returned.
     * NoSuchElementException or TimeoutException will be thrown if the element
     * is not found on the page.
     *
     * @param controlLocation element to get from the page.
     * @return WebElement specified by income <code>ControlLocation</code>.
     */
    public WebElement get(final ControlLocation controlLocation) {
        WebDriverWait wait = new WebDriverWait(
                WebDriverUtils.getDriver(),
                WebDriverUtils.getImplicitlyWaitTimeout());

        WebElement webElement = wait.until(
                ExpectedConditions.
                        visibilityOfElementLocated(controlLocation.getBy()));

        if (webElement == null) {
            throw new NoSuchElementException(
                    String.format("Error %s", controlLocation.getValue()));
        }

        return webElement;
    }

    /**
     * Used to get a list of visible web elements that match the
     * given ControlLocation.
     *
     * @param controlLocation some locator.
     * @return List of visible WebElements found by given locator.
     */
    public List<WebElement> getElements(final ControlLocation controlLocation) {
        turnOffTimeouts();
        List<WebElement> webElements = WebDriverUtils.getDriver()
                .findElements(controlLocation.getBy());
        turnOnTimeouts();

        if (webElements.size() != 0) {
            WebDriverWait wait = new WebDriverWait(
                    WebDriverUtils.getDriver(),
                    0);

            webElements = wait.until(
                    ExpectedConditions
                            .visibilityOfAllElementsLocatedBy(
                                    controlLocation.getBy()));

            if (webElements == null) {
                throw new NoSuchElementException(
                        String.format("Error %s", controlLocation.getValue()));
            }
        }

        return webElements;
    }

    /**
     * sets the driver implicitlyWait to 0(zero).
     */
    private void turnOffTimeouts() {
        WebDriverUtils.getDriver().manage().timeouts()
                .implicitlyWait(0, TimeUnit.SECONDS);
    }

    /**
     * sets the driver implicitlyWait to the specified in the WebDriverUtils
     * one.
     */
    private void turnOnTimeouts() {
        WebDriverUtils.getDriver().manage().timeouts()
                .implicitlyWait(WebDriverUtils.getImplicitlyWaitTimeout(),
                        TimeUnit.SECONDS);
    }

    /**
     * Switches to the currently active alert popup
     * for this particular driver instance.
     * <p>
     * the NoAlertPresentException - If the alert popup cannot be found
     *
     * @return alert modal dialogue.
     */
    public Alert getAlert() {
        return WebDriverUtils.getDriver().switchTo().alert();
    }
}
