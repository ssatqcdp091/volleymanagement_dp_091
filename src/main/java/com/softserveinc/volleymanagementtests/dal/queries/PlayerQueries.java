package com.softserveinc.volleymanagementtests.dal.queries;

/**
 * @author J.Bodnar
 */
public class PlayerQueries {


    //SELECT queries
    public static final String SELECT_ALL_QUERY = "SELECT Id, FirstName, "                          // all rows
            + "LastName, BirthYear, Height, Weight "
            + "FROM dbo.Players ";

    public static final String SELECT_BY_ID_QUERY = SELECT_ALL_QUERY + "WHERE Id = %d";             // by ID

    public static final String SELECT_TEAM_MEMBERS_QUERY = SELECT_ALL_QUERY + "WHERE TeamId = %d";  // by TeamId


    public static final String SELECT_BY_ALL_FIELDS_QUERY = SELECT_ALL_QUERY + "WHERE " +           // by ALL FIELDS
            "FirstName = N'%s' AND " + "LastName = N'%s' AND birthYear = %d AND height " +
            "= %d AND weight = %d";

    public static final String SELECT_BY_LAST_NAME_QUERY
            = "SELECT * FROM Players WHERE LastName LIKE N'%s'";  // by LastName

    public static final String SELECT_BY_FIRST_NAME_QUERY
            = "SELECT * FROM Players WHERE FirstName LIKE N'%s'"; // by FirstName

    //INSERT queries
    public static final String INSERT_QUERY = "INSERT INTO dbo.Players " +          // one row
            "(FirstName, LastName, BirthYear, Height, Weight)  VALUES  " +
            "(N'%s',N'%s',%d,%d,%d) ";


    //DELETE queries
    public static final String DELETE_QUERY = "DELETE dbo.Players";                                   // all rows

    public static final String DELETE_BY_ALL_FIELDS_QUERY = DELETE_QUERY + " WHERE FirstName " +      // by all fields
            "= N'%s' AND LastName = N'%s' AND birthYear = '%d' AND height = '%d' AND weight = '%d'";


    public static final String DELETE_BY_ID_QUERY = DELETE_QUERY + " WHERE id = '%d'";                // by id

    // UPDATE
    public static final String UPDATE_QUERY = "UPDATE dbo.Players SET TeamId = NULL WHERE TeamID = %d";
    public static final String UPDATE_TO_NULL_QUERY = "UPDATE dbo.Players SET TeamId = NULL WHERE TeamID is not NULL";
}
