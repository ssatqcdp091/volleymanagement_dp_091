package com.softserveinc.volleymanagementtests.dal.queries;

/**
 * @author Danil Zhyliaiev
 */
public class ContributorTeamsQueries {
    //SELECT queries
    public static final String SELECT_ALL_QUERY = "SELECT Id, Name, CourseDirection"
            + " FROM dbo.ContributorTeams;";
    public static final String SELECT_BY_ID = "SELECT Id, Name, CourseDirection"
            + " FROM dbo.ContributorTeams WHERE Id = %s;";

    private ContributorTeamsQueries() {
    }
}
