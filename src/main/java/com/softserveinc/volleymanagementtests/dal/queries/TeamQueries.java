package com.softserveinc.volleymanagementtests.dal.queries;

/* @author S.Tsyganovskiy */

public class TeamQueries {

    public static final String INSERT_QUERY = "INSERT INTO dbo.Teams " +
            "(Name, Coach, Achievements, CaptainId)  VALUES  " +
            "(N'%s',N'%s',N'%s',%d) ";

    public static final String SELECT_ALL_QUERY = "SELECT * FROM dbo.Teams";

    public static final String SELECT_BY_ID_QUERY = "SELECT * FROM dbo.Teams WHERE Id = %d";

    public static final String DELETE_BY_ID_QUERY = "DELETE dbo.Teams  WHERE Id = %d";
    public static final String DELETE_ALL_QUERY = "DELETE dbo.Teams";

    public static final String SELECT_BY_NAME_QUERY
            = "SELECT * FROM dbo.Teams WHERE Name = N'%s'";



}
