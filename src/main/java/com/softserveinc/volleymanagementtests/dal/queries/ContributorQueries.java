package com.softserveinc.volleymanagementtests.dal.queries;

/**
 * @author Danil Zhyliaiev
 */
public class ContributorQueries {
    //SELECT queries
    public static final String SELECT_ALL_QUERY = "SELECT Id, Name, ContributorTeamId"
            + " FROM dbo.Contributors;";
    public static final String SELECT_BY_TEAM_ID = "SELECT Id, Name, ContributorTeamId"
            + " FROM dbo.Contributors WHERE ContributorTeamId = %s;";

    private ContributorQueries() {
    }
}
