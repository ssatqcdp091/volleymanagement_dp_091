package com.softserveinc.volleymanagementtests.dal.repositories;

/* @author S.Tsyganovskiy */

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.models.TeamDal;
import com.softserveinc.volleymanagementtests.dal.queries.PlayerQueries;
import com.softserveinc.volleymanagementtests.dal.queries.TeamQueries;
import com.softserveinc.volleymanagementtests.specification.TestConfig;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TeamRepository {

    public TeamDal create(TeamDal team) throws SQLException, ClassNotFoundException {
        try (SqlQuery SqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet resulSetKeys = SqlQuery.getKey(String.format(TeamQueries.INSERT_QUERY,
                    team.getName().replace("'", "''"), team.getCoach(), team.getAchievements(), team.getCaptainId()));
            resulSetKeys.next();
            return getById(resulSetKeys.getInt(1));
        }
    }

    public TeamDal getById(int id) throws SQLException, ClassNotFoundException {
        try (SqlQuery SqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.select(
                    String.format(TeamQueries.SELECT_BY_ID_QUERY, id));
            resultSet.next();
            return getTeamModel(resultSet);
        }
    }

    public List<TeamDal> getAll() throws SQLException, ClassNotFoundException {
        try (SqlQuery SqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.select(TeamQueries.SELECT_ALL_QUERY);
            List<TeamDal> teamModelsList = new ArrayList<>();
            if (resultSet != null) {
                while (resultSet.next()) {
                    teamModelsList.add(getTeamModel(resultSet));
                }
            }
            return teamModelsList;
        }
    }

    public int deleteById(Integer id) throws SQLException, ClassNotFoundException {
        int affectedRows;
        try (SqlQuery sqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            sqlQuery.modify(String.format(PlayerQueries.UPDATE_QUERY, id));
            affectedRows = sqlQuery.modify(String.format(TeamQueries.DELETE_BY_ID_QUERY, id));
            return affectedRows;
        }
    }

    public int deleteAll() throws SQLException, ClassNotFoundException {
        int affectedRows;
        try (SqlQuery sqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            sqlQuery.modify(String.format(PlayerQueries.UPDATE_TO_NULL_QUERY));
            affectedRows = sqlQuery.modify(String.format(TeamQueries.DELETE_ALL_QUERY));
            return affectedRows;
        }
    }

    public boolean isTeamExistsInDataBaseByName(String name) throws SQLException,
            ClassNotFoundException {
        try (SqlQuery SqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet selectedTeams
                    = SqlQuery.select(String.format(TeamQueries.SELECT_BY_NAME_QUERY, name.replace("'", "''")));
            return selectedTeams.next();
        }
    }

    public boolean isTeamExistsInDataBaseById(int id) throws SQLException, ClassNotFoundException {
        try (SqlQuery SqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet selectedTeams
                    = SqlQuery.select(String.format(TeamQueries.SELECT_BY_ID_QUERY, id));
            return selectedTeams.next();
        }
    }

    private TeamDal getTeamModel(ResultSet resultSet) throws SQLException {

        String achievements = resultSet.getString("Achievements");
        if (resultSet.wasNull()) {
            achievements = null;
        }

        String coach = resultSet.getString("Coach");
        if (resultSet.wasNull()) {
            coach = null;
        }
        return TeamDal.newBuilder()
                .setId(resultSet.getInt("Id"))
                .setName(resultSet.getString("Name"))
                .setCoach(coach)
                .setAchievements(achievements)
                .setCaptainId(resultSet.getInt("CaptainId"))
                .build();
    }

}
