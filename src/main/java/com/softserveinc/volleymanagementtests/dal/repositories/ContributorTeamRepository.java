package com.softserveinc.volleymanagementtests.dal.repositories;

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.models.ContributorTeamDal;
import com.softserveinc.volleymanagementtests.dal.queries.ContributorTeamsQueries;
import com.softserveinc.volleymanagementtests.specification.TestConfig;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Danil Zhyliaiev
 */
public class ContributorTeamRepository {
    public List<ContributorTeamDal> getAll() throws SQLException, ClassNotFoundException {
        try (SqlQuery sqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            List<ContributorTeamDal> allTeams;
            try (ResultSet resultSet = sqlQuery.select(ContributorTeamsQueries.SELECT_ALL_QUERY)) {
                allTeams = new ArrayList<>();

                while (resultSet.next()) {
                    allTeams.add(getContributorTeamModel(resultSet));
                }
            }

            return allTeams;
        }
    }

    public ContributorTeamDal getById(int id) throws SQLException, ClassNotFoundException {
        try (SqlQuery sqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet resultSet = sqlQuery.select(
                    String.format(ContributorTeamsQueries.SELECT_BY_ID, id));
            resultSet.next();

            return getContributorTeamModel(resultSet);
        }
    }

    private ContributorTeamDal getContributorTeamModel(
            final ResultSet resultSet) throws SQLException {
        Integer id = resultSet.getInt("Id");
        if (resultSet.wasNull()) {
            id = null;
        }

        return ContributorTeamDal.newBuilder()
                .setId(id)
                .setName(resultSet.getString("Name"))
                .setCourseDirection(resultSet.getString("CourseDirection"))
                .build();
    }
}
