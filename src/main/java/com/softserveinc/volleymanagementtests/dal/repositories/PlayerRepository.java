package com.softserveinc.volleymanagementtests.dal.repositories;

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.queries.PlayerQueries;
import com.softserveinc.volleymanagementtests.dal.queries.TeamQueries;
import com.softserveinc.volleymanagementtests.specification.TestConfig;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PlayerRepository {


    public List<PlayerDal> getAll() throws SQLException, ClassNotFoundException {
        try (SqlQuery SqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.select(PlayerQueries.SELECT_ALL_QUERY);
            List<PlayerDal> playerModelsList = new ArrayList<>();
            if (resultSet != null) {
                while (resultSet.next()) {
                    playerModelsList.add(getPlayerModel(resultSet));
                }
            }
            return playerModelsList;
        }
    }

    public PlayerDal getById(int id) throws SQLException, ClassNotFoundException {
        try (SqlQuery SqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.select(
                    String.format(PlayerQueries.SELECT_BY_ID_QUERY, id));
            resultSet.next();
            return getPlayerModel(resultSet);
        }
    }

    public List<PlayerDal> getTeamMembers(int teamId) throws SQLException, ClassNotFoundException {
        try (SqlQuery SqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet resultSet = SqlQuery.select(String.format(
                    PlayerQueries.SELECT_TEAM_MEMBERS_QUERY, teamId));
            List<PlayerDal> playerModelsList = new ArrayList<>();
            if (resultSet != null) {
                while (resultSet.next()) {
                    playerModelsList.add(getPlayerModel(resultSet));
                }
            }
            return playerModelsList;
        }

    }

    public PlayerDal create(PlayerDal player) throws SQLException, ClassNotFoundException {
        try (SqlQuery SqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet resulSetKeys = SqlQuery.getKey(String.format(PlayerQueries.INSERT_QUERY,
                    player.getFirstName().replace("'", "''"),
                    player.getLastName().replace("'", "''"),
                    player.getBirthYear(),
                    player.getHeight(),
                    player.getWeight()));

            resulSetKeys.next();
            return getById(resulSetKeys.getInt(1));
        }
    }


    public int deletePlayer(PlayerDal player) throws SQLException, ClassNotFoundException {
        int affectedRows;
        try (SqlQuery sqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            affectedRows = sqlQuery.modify(String.format(PlayerQueries.DELETE_BY_ALL_FIELDS_QUERY, player.getFirstName(),
                    player.getLastName(), player.getBirthYear(), player.getHeight(), player.getWeight()));
            return affectedRows;
        }
    }


    public int deleteById(Integer id) throws SQLException, ClassNotFoundException {
        int affectedRows;
        try (SqlQuery sqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            affectedRows = sqlQuery.modify(String.format(PlayerQueries.DELETE_BY_ID_QUERY, id));
            return affectedRows;
        }
    }

    public int deleteAll() throws SQLException, ClassNotFoundException {
        int affectedRows;
        try (SqlQuery sqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            sqlQuery.modify(String.format(PlayerQueries.UPDATE_TO_NULL_QUERY));
            sqlQuery.modify(String.format(TeamQueries.DELETE_ALL_QUERY));
            affectedRows = sqlQuery.modify(String.format(PlayerQueries.DELETE_QUERY));
            return affectedRows;
        }
    }

    private PlayerDal getPlayerModel(ResultSet resultSet) throws SQLException {
        Integer birthYear = resultSet.getInt("birthYear");
        if (resultSet.wasNull()) {
            birthYear = null;
        }

        Integer height = resultSet.getInt("height");
        if (resultSet.wasNull()) {
            height = null;
        }

        Integer weight = resultSet.getInt("weight");
        if (resultSet.wasNull()) {
            weight = null;
        }

        return new PlayerDal.PlayerDalBuilder()
                .id(resultSet.getInt("Id"))
                .firstName(resultSet.getString("FirstName"))
                .lastName(resultSet.getString("LastName"))
                .birthYear(birthYear)
                .height(height)
                .weight(weight)
                .build();
    }

    public boolean isPlayerExistsInDataBaseByLastName(String lastName) throws SQLException, ClassNotFoundException {
        try (SqlQuery SqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet selectedPlayers
                    = SqlQuery.select(String.format(
                    PlayerQueries.SELECT_BY_LAST_NAME_QUERY, lastName.replace("'", "''")));
            return selectedPlayers.next();
        }
    }

    public boolean isPlayerExistsInDataBaseByFirstName(String firstName) throws SQLException, ClassNotFoundException {
        try (SqlQuery SqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet selectedPlayers
                    = SqlQuery.select(String.format(
                    PlayerQueries.SELECT_BY_FIRST_NAME_QUERY, firstName.replace("'", "''")));
            return selectedPlayers.next();
        }
    }

    public boolean isPlayerExistsInDataBase(PlayerDal player) throws SQLException, ClassNotFoundException {
        ResultSet resultSetReturnedPlayers;
        try (SqlQuery sqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            resultSetReturnedPlayers
                    = sqlQuery.select(String.format(
                    PlayerQueries.SELECT_BY_ALL_FIELDS_QUERY, player.getFirstName().replace("'", "''"),
                    player.getLastName().replace("'", "''"), player.getBirthYear(), player.getHeight(), player.getWeight()));
            return resultSetReturnedPlayers.next();
        }
    }
}
