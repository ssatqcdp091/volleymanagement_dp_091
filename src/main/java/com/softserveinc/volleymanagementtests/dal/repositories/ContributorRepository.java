package com.softserveinc.volleymanagementtests.dal.repositories;

import com.softserveinc.volleymanagementtests.dal.SqlQuery;
import com.softserveinc.volleymanagementtests.dal.queries.ContributorQueries;
import com.softserveinc.volleymanagementtests.specification.TestConfig;
import com.softserveinc.volleymanagementtests.dal.models.ContributorDal;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Danil Zhyliaiev
 */
public class ContributorRepository {
    public List<ContributorDal> getAll() throws SQLException, ClassNotFoundException {
        try (SqlQuery sqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet resultSet = sqlQuery.select(ContributorQueries.SELECT_ALL_QUERY);
            List<ContributorDal> allContributors = new ArrayList<>();

            while (resultSet.next()) {
                allContributors.add(getContributorModel(resultSet));
            }

            return allContributors;
        }
    }

    public List<ContributorDal> getTeamMembersById(int teamId)
            throws SQLException, ClassNotFoundException {
        try (SqlQuery sqlQuery = new SqlQuery(TestConfig.getDBConnectionString())) {
            ResultSet resultSet = sqlQuery.select(
                    String.format(ContributorQueries.SELECT_BY_TEAM_ID, teamId));
            List<ContributorDal> allContributors = new ArrayList<>();

            while (resultSet.next()) {
                allContributors.add(getContributorModel(resultSet));
            }

            return allContributors;
        }
    }

    private ContributorDal getContributorModel(final ResultSet resultSet) throws SQLException {
        Integer id = resultSet.getInt("Id");
        if (resultSet.wasNull()) {
            id = null;
        }

        Integer teamId = resultSet.getInt("ContributorTeamId");
        if (resultSet.wasNull()) {
            teamId = null;
        }

        return ContributorDal.newBuilder()
                .setId(id)
                .setName(resultSet.getString("Name"))
                .setTeamId(teamId)
                .build();
    }
}
