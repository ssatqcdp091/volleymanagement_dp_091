package com.softserveinc.volleymanagementtests.dal.models;

public class TeamDal {
    private Integer id;
    private String name;
    private String coach;
    private String achievements;
    private Integer captainId;

    private TeamDal() {
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCoach() {
        return coach;
    }

    public String getAchievements() {
        return achievements;
    }

    public Integer getCaptainId() {
        return captainId;
    }

    public static TeamDalBuilder newBuilder() {
        return new TeamDal().new TeamDalBuilder();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TeamDal teamDal = (TeamDal) o;

        if (!getId().equals(teamDal.getId())) {
            return false;
        }

        if (!getName().equals(teamDal.getName())) {
            return false;
        }

        if (getCoach() != null) {
            if (!getCoach().equals(teamDal.getCoach())) {
                return false;
            }
        } else {
            if (teamDal.getCoach() != null) {
                return false;
            }
        }

        if (getAchievements() != null) {
            if (!getAchievements().equals(teamDal.getAchievements())) {
                return false;
            }
        } else {
            if (teamDal.getAchievements() != null) {
                return false;
            }
        }
        return getCaptainId().equals(teamDal.getCaptainId());

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + (getCoach() != null ? getCoach().hashCode() : 0);
        result = 31 * result + (getAchievements() != null ? getAchievements().hashCode() : 0);
        result = 31 * result + getCaptainId().hashCode();
        return result;
    }

    public class TeamDalBuilder {
        public TeamDalBuilder setId(final Integer idArg) {
            id = idArg;
            return this;
        }

        public TeamDalBuilder setName(final String nameArg) {
            name = nameArg;
            return this;
        }

        public TeamDalBuilder setCoach(final String coachArg) {
            coach = coachArg;
            return this;
        }

        public TeamDalBuilder setAchievements(final String achievementsArg) {
            achievements = achievementsArg;
            return this;
        }

        public TeamDalBuilder setCaptainId(final Integer captainIdArg) {
            captainId = captainIdArg;
            return this;
        }

        public TeamDal build() {
            return TeamDal.this;
        }
    }
}
