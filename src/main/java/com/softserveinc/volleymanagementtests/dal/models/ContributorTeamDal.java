package com.softserveinc.volleymanagementtests.dal.models;

/**
 * @author Danil Zhyliaiev
 */
public class ContributorTeamDal {
    private Integer id;
    private String name;
    private String courseDirection;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCourseDirection() {
        return courseDirection;
    }

    public static ContributorTeamDalBuilder newBuilder() {
        return new ContributorTeamDal().new ContributorTeamDalBuilder();
    }

    public class ContributorTeamDalBuilder {
        public ContributorTeamDalBuilder setId(final Integer idArg) {
            id = idArg;
            return this;
        }

        public ContributorTeamDalBuilder setName(final String nameArg) {
            name = nameArg;
            return this;
        }

        public ContributorTeamDalBuilder setCourseDirection(final String courseDirectionArg) {
            courseDirection = courseDirectionArg;
            return this;
        }

        public ContributorTeamDal build() {
            return ContributorTeamDal.this;
        }
    }
}
