package com.softserveinc.volleymanagementtests.dal.models;

/**
 * Created by Sergey Tsyganovskiy on 05.03.2016.
 * This class define a Player returned from Database
 * ****************How to USE**********************
 * PlayerDal playerDal = new PlayerDalBuilder().id(1).firstName("Tom").lastName("Adams")
 * .birthYear(1994).height(1780).weight(80).build();
 */

public class PlayerDal {
    private final Integer id;
    private final String firstName;
    private final String lastName;
    private final Integer birthYear;
    private final Integer height;
    private final Integer weight;

    private PlayerDal(PlayerDalBuilder playerDalBuilder) {
        id = playerDalBuilder.id;
        firstName = playerDalBuilder.firstName;
        lastName = playerDalBuilder.lastName;
        birthYear = playerDalBuilder.birthYear;
        height = playerDalBuilder.height;
        weight = playerDalBuilder.weight;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    public Integer getHeight() {
        return height;
    }

    public Integer getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlayerDal)) {
            return false;
        }

        PlayerDal playerDal = (PlayerDal) o;

        if (!getId().equals(playerDal.getId())) {
            return false;
        }
        if (!getFirstName().equals(playerDal.getFirstName())) {
            return false;
        }
        if (!getLastName().equals(playerDal.getLastName())) {
            return false;
        }
        if ((getBirthYear() != null)
                ? !getBirthYear().equals(playerDal.getBirthYear())
                : (playerDal.getBirthYear() != null)) {
            return false;
        }
        if ((getHeight() != null)
                ? !getHeight().equals(playerDal.getHeight())
                : (playerDal.getHeight() != null)) {
            return false;
        }
        return getWeight() != null
                ? getWeight().equals(playerDal.getWeight())
                : playerDal.getWeight() == null;

    }

    @Override
    public int hashCode() {
        int magicOddPrime = 31;
        int result = getId().hashCode();
        result = magicOddPrime * result + getFirstName().hashCode();
        result = magicOddPrime * result + getLastName().hashCode();
        result = magicOddPrime * result + (getBirthYear() != null ? getBirthYear().hashCode() : 0);
        result = magicOddPrime * result + (getHeight() != null ? getHeight().hashCode() : 0);
        result = magicOddPrime * result + (getWeight() != null ? getWeight().hashCode() : 0);
        return result;
    }

    public static final class PlayerDalBuilder {
        private Integer id;
        private String firstName;
        private String lastName;
        private Integer birthYear;
        private Integer height;
        private Integer weight;

        public PlayerDalBuilder() {
        }

        public PlayerDalBuilder id(Integer val) {
            id = val;
            return this;
        }

        public PlayerDalBuilder firstName(String val) {
            firstName = val;
            return this;
        }

        public PlayerDalBuilder lastName(String val) {
            lastName = val;
            return this;
        }

        public PlayerDalBuilder birthYear(Integer val) {
            birthYear = val;
            return this;
        }

        public PlayerDalBuilder height(Integer val) {
            height = val;
            return this;
        }

        public PlayerDalBuilder weight(Integer val) {
            weight = val;
            return this;
        }

        public PlayerDal build() {
            return new PlayerDal(this);
        }
    }
}

