package com.softserveinc.volleymanagementtests.tests.team.create;


import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Team")
@Stories("Create")
public class TeamCreationWithoutCaptainTest extends TestBase{
    @DataProvider
    public Object[][] invalidDataProvider() {

        return new Object[][]{
                {TeamTestData.getInvalidTeamWithoutCaptain(), ErrorMessages.TEAM_CAPTAIN_IS_NOT_CHOSEN_ERROR}
        };
    }

    @Test(dataProvider = "invalidDataProvider")
    public void testTeamCreationWithoutCaptain(Team team, String errorMessage) throws Exception {
        TeamCreatePage teamCreatePage = new TeamCreatePage();

        teamCreatePage.setAllTeamFields(team);
        teamCreatePage.submitCreateTeamButton();
        Specification.get().forThe(teamCreatePage).isErrorMessageForCaptainAbsenseAppeared(errorMessage).next().check();

        Assert.assertFalse(new TeamRepository().isTeamExistsInDataBaseByName(team.getName()));
    }
}
