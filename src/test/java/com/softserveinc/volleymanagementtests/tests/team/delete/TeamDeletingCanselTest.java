package com.softserveinc.volleymanagementtests.tests.team.delete;

import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

/**
 * @author J.Bodnar
 */
@Features("Team")
@Stories("Delete")
public class TeamDeletingCanselTest extends TeamDeleteBaseTest {

    @Test
    public void TestTeamDeletingCansel() throws SQLException, ClassNotFoundException {

        TeamListPage teamListPage = new TeamListPage();
        teamListPage.clickDeleteTeamButton(team).dismiss();
        Assert.assertTrue(new TeamRepository().isTeamExistsInDataBaseByName(team.getName()));
    }
    @Override
    @AfterMethod
    public final void tearDown() throws Exception {

        WebDriverUtils.stop();
    }
}
