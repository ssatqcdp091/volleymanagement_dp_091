package com.softserveinc.volleymanagementtests.tests.team.read;

import com.softserveinc.volleymanagementtests.specification.Specification;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

/**
 * @author Karabaza Anton on 22.03.2016.
 */
public class CorrectTeamReadFromDBAndDisplayOnTeamListPageTest extends ReadBaseTest {

    @Features("Team")
    @Stories("Read")
    @Test
    public void testCorrectTeamReadFromDBAndDisplayOnTeamListPage() throws SQLException,
            ClassNotFoundException {

        Specification.get()
                .forThe(teamListPage)
                .isTeamDisplayedOnList(team)
                .next()
                .check();
    }

}
