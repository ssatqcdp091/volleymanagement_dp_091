package com.softserveinc.volleymanagementtests.tests.team.create;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.sql.SQLException;

/**
 * @author S.Tsyganovskiy
 */
public class TestBase {

    @BeforeMethod
    public final void setUp() throws SQLException, ClassNotFoundException {
        WebDriverUtils.load(PageUrls.TEAM_CREATE_URL);
        TeamCreatePage teamCreatePage = new TeamCreatePage();
        teamCreatePage.logInAsCaptain();
        clearDataBase();
    }

    @AfterMethod
    public final void tearDown() {
        WebDriverUtils.stop();
    }

    private void clearDataBase() throws SQLException, ClassNotFoundException{
        new PlayerRepository().deleteAll();
        new TeamRepository().deleteAll();
    }

}
