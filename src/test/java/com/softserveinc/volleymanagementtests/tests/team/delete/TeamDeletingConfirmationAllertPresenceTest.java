package com.softserveinc.volleymanagementtests.tests.team.delete;

import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;

import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * @author  J.Bodnar
 */
@Features("Team")
@Stories("Delete")
public class TeamDeletingConfirmationAllertPresenceTest extends TeamDeleteBaseTest {

    @Test
    public void TestConfirmationAllertPresence(){
        TeamListPage teamListPage = new TeamListPage();
        Specification specification = Specification.get();
        specification.forThe(teamListPage.clickDeleteTeamButton(team))
                .textMatch(String.format("Вы действительно хотите удалить команду \"%s\" ?"
                        ,team.getName()));
        specification.check();


    }
}


