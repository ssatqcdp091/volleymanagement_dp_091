package com.softserveinc.volleymanagementtests.tests.team.back_to_list;


import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.pages.team.TeamDetailsPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;
@Features("Team")
@Stories("Back link")
public class CheckBackToTheTeamListLinkFromTeamDetailsPageTest  {
    @BeforeMethod
    public void setUp()throws SQLException, ClassNotFoundException  {
        WebDriverUtils.load(PageUrls.TEAM_CREATE_URL);
        new PlayerRepository().deleteAll();
        new TeamRepository().deleteAll();
    }

    @AfterMethod
    public void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void testCheckBackToThePlayersListLink() throws Exception{
        TeamCreatePage teamCreatePage = new TeamCreatePage();
        Team team = TeamTestData.getValidTeam();
        new PlayerRepository().create(PlayerToDal.getInstance().map(team.getCaptain()));

        if (team.getRoster() != null) {
            for (Player player : team.getRoster()) {
                new PlayerRepository().create(PlayerToDal.getInstance().map(player));
            }
        }

        teamCreatePage.setAllTeamFields(team);
        TeamListPage teamsListPage =  teamCreatePage.submitCreateTeamButton();
        TeamDetailsPage teamDetailsPage =  teamsListPage.showTeamDetails(team);
        teamDetailsPage.clickBackLink();
        Specification.get().forThe(teamsListPage.getListOfTeamsLabel()).textMatch("List of teams").next().check();
    }
}
