package com.softserveinc.volleymanagementtests.tests.team.create;


import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Team")
@Stories("Create")
public class TeamCreationWithInvalidNameTest extends TestBase{
    @DataProvider
    public Object[][] invalidDataProvider() {

        return new Object[][]{
                {TeamTestData.getInvalidTeamByName(Violations.EMPTY_FIELD),
                        ErrorMessages.TEAM_NAME_CAN_NOT_BE_EMPTY_ERROR}
                ,{TeamTestData.getInvalidTeamByName(Violations.MAX_LENGTH_VIOLATED),
                ErrorMessages.TEAM_NAME_LENGTH_ERROR}
        };
    }

    @Test(dataProvider = "invalidDataProvider")
    public void teamCreationWithInvalidName(Team team, String errorMessage) throws Exception {
        TeamCreatePage teamCreatePage = new TeamCreatePage();
        new PlayerRepository().create(PlayerToDal.getInstance().map(team.getCaptain()));

        if (team.getRoster() != null) {
            for (Player player : team.getRoster()) {
                new PlayerRepository().create(PlayerToDal.getInstance().map(player));
            }
        }

        teamCreatePage.setAllTeamFields(team);
        Specification specification = Specification.get();
        specification.forThe(teamCreatePage).isAllFieldsFilledInCorrectly(team);
        teamCreatePage.submitCreateTeamButton();

        specification.forThe(teamCreatePage.getErrorMessageNameInput()).textMatch(errorMessage).next().check();
        Assert.assertFalse(new TeamRepository().isTeamExistsInDataBaseByName(team.getName()));
    }
}
