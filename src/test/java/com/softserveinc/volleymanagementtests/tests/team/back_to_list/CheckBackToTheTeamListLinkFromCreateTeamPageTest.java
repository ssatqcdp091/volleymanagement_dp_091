package com.softserveinc.volleymanagementtests.tests.team.back_to_list;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Team")
@Stories("Back link")
public class CheckBackToTheTeamListLinkFromCreateTeamPageTest {
    @BeforeMethod
    public void setUp() {
        WebDriverUtils.load(PageUrls.TEAM_CREATE_URL);
    }

    @AfterMethod
    public void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void testCheckBackToThePlayersListLink(){

        TeamListPage teamListPage = new TeamCreatePage().clickBackToList();
        Specification.get().forThe(teamListPage.getListOfTeamsLabel()).textMatch("List of teams").next().check();
    }
}
