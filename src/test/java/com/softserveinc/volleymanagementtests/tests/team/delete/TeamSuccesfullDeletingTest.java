package com.softserveinc.volleymanagementtests.tests.team.delete;

import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.AlertImpl;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

/**
 * @author J.Bodnar
 */
@Features("Team")
@Stories("Delete")
public class TeamSuccesfullDeletingTest extends TeamDeleteBaseTest {
    @Test
    public void TestSuccesfullTeamDeleting() throws SQLException, ClassNotFoundException {
        TeamListPage teamListPage = new TeamListPage();
        Specification specification = Specification.get();
        teamListPage.clickDeleteTeamButton(team).accept();
        AlertImpl.get().accept();
        specification.forThe(new TeamRepository().isTeamExistsInDataBaseByName(team.getName())).isFalse();
        specification.check();
    }
    @Override
    @AfterMethod
    public final void tearDown() throws Exception {

        WebDriverUtils.stop();
    }
}
