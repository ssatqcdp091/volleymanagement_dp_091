package com.softserveinc.volleymanagementtests.tests.team.update;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.team.TeamDetailsPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamEditPage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.DalToPlayer;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import com.softserveinc.volleymanagementtests.testdata.team.mappers.TeamToDal;
import com.softserveinc.volleymanagementtests.tools.AllureOnFailListener;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

@Features("Team")
@Stories("Create")
@Listeners({AllureOnFailListener.class})
public class TeamUpdateDefaultValuesTest {
    private TeamDetailsPage teamDetailsPage;
    @BeforeMethod
    public final void setUp() throws SQLException, ClassNotFoundException {
        WebDriverUtils.load(PageUrls.TEAMS_LIST_URL);

        TeamListPage teamListPage = new TeamListPage();

        if (teamListPage.getTeamList().size() == 0) {
            Team team = TeamTestData.getValidTeam();
            PlayerDal captainDal = new PlayerRepository()
                    .create(PlayerToDal
                            .getInstance()
                            .map(team.getCaptain()));

            team = Team.newBuilder(team).setCaptain(DalToPlayer.getInstance().map(captainDal))
                    .build();
            new TeamRepository().create(TeamToDal.getInstance().map(team));
            WebDriverUtils.refresh();

            teamListPage = new TeamListPage();
        }

        teamDetailsPage = teamListPage.showRandomTeamDetails();
    }

    @AfterTest
    public final void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public final void testTeamUpdateDefaultFieldValues() {
        Team team = teamDetailsPage.readTeam();
        TeamEditPage teamEditPage = teamDetailsPage.clickEditTeamLink();

        Specification.get()
                .forThe(teamEditPage.readTeam())
                    .equalTo(team)
                    .next()
                .check();
    }
}
