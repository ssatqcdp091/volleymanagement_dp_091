package com.softserveinc.volleymanagementtests.tests.team.delete;


import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.AlertImpl;
import org.testng.annotations.*;


/**
 * @author J.Bodnar
 */
abstract public class TeamDeleteBaseTest {

    Team team;

    @BeforeMethod()
    public final void setUp() throws Exception {

        team = TeamTestData.getValidTeam();
        new PlayerRepository().create(PlayerToDal.getInstance().map(team.getCaptain()));
        for(Player player: team.getRoster()){
            new PlayerRepository().create(PlayerToDal.getInstance().map(player));
        }
        WebDriverUtils.load(PageUrls.TEAM_CREATE_URL);
        TeamCreatePage teamCreatePage = new TeamCreatePage();
        teamCreatePage.setAllTeamFields(team);
        teamCreatePage.submitCreateTeamButton();
        WebDriverUtils.load(PageUrls.TEAMS_LIST_URL);
    }


    @AfterMethod
    public void tearDown() throws Exception {
        AlertImpl.get().accept();
        WebDriverUtils.stop();
    }

}
