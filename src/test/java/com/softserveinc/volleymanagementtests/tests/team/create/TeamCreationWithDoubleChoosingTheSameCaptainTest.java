package com.softserveinc.volleymanagementtests.tests.team.create;


import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Team")
@Stories("Create")
public class TeamCreationWithDoubleChoosingTheSameCaptainTest extends TestBase{
    @DataProvider
    public Object[][] validDataProvider() {

        return new Object[][]{
                {TeamTestData.getValidTeam(), ErrorMessages.TEAM_CAPTAIN_IN_OTHER_TEAM_ERROR}
        };
    }

    @Test(dataProvider = "validDataProvider")
    public void testTeamCreationWithDoubleChoosingTheSameCaptain(Team team, String errorMessage) throws Exception {
        TeamCreatePage teamCreatePage = new TeamCreatePage();

        new PlayerRepository().create(PlayerToDal.getInstance().map(team.getCaptain()));

        if (team.getRoster() != null) {
            for (Player player : team.getRoster()) {
                new PlayerRepository().create(PlayerToDal.getInstance().map(player));
            }
        }

        teamCreatePage.setAllTeamFields(team);
        TeamListPage teamListPage =  teamCreatePage.submitCreateTeamButton();
        TeamCreatePage teamCreatePageNew = teamListPage.clickCreateTeamLink();
        teamCreatePageNew.setAllTeamFields(team);
        teamCreatePageNew.submitCreateTeamButton();
        Specification.get().forThe(teamCreatePage).isErrorMessageForCaptainAbsenseAppeared(errorMessage).next().check();
        teamCreatePage.submitCreateTeamButton();

        Assert.assertFalse(new TeamRepository().isTeamExistsInDataBaseByName(team.getName()));
    }
}
