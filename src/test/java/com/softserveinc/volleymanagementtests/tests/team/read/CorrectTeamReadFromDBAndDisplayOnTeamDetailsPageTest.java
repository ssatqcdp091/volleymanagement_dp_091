package com.softserveinc.volleymanagementtests.tests.team.read;

import com.softserveinc.volleymanagementtests.specification.Specification;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

/**
 * @author Karabaza Anton on 23.03.2016.
 */
public class CorrectTeamReadFromDBAndDisplayOnTeamDetailsPageTest extends ReadBaseTest {

    @Features("Team")
    @Stories("Read")
    @Test
    public void testCorrectTeamReadFromDBAndDisplayOnTeamDetailsPage() throws SQLException,
            ClassNotFoundException {

        Specification.get()
                .forThe(teamListPage)
                .isTeamDisplayedOnListWithCorrectDetails(team)
                .next()
                .check();
    }
}
