package com.softserveinc.volleymanagementtests.tests.team.create;


import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.team.TeamCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.testdata.team.Team;
import com.softserveinc.volleymanagementtests.testdata.team.TeamTestData;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Team")
@Stories("Create")

public class TeamCreationWithValidDataTest extends TestBase {

    @DataProvider
    public Object[][] validDataProvider() {
        int validTeamsCount = TeamTestData.getValidTeamsList().size();
        Team[] validTeams = new Team[validTeamsCount];
        TeamTestData.getValidTeamsList().toArray(validTeams);

        Object[][] data = new Object[validTeamsCount][1];

        for (int i = 0; i < validTeamsCount; i++) {
            data[i][0] = validTeams[i];
        }
        return data;
    }

    @Test(dataProvider = "validDataProvider")
    public void testTeamCreationWithValidData(Team team) throws Exception {
        TeamCreatePage teamCreatePage = new TeamCreatePage();
        new PlayerRepository().create(PlayerToDal.getInstance().map(team.getCaptain()));

        if (team.getRoster() != null) {
            for (Player player : team.getRoster()) {
                new PlayerRepository().create(PlayerToDal.getInstance().map(player));
            }
        }

        teamCreatePage.setAllTeamFields(team);
        Specification.get().forThe(teamCreatePage).isAllFieldsFilledInCorrectly(team).next().check();
        teamCreatePage.submitCreateTeamButton();

       Assert.assertTrue(new TeamRepository().isTeamExistsInDataBaseByName(team.getName()));
    }
}
