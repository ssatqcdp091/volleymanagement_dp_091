package com.softserveinc.volleymanagementtests.tests.team.read;

import com.softserveinc.volleymanagementtests.dal.models.TeamDal;
import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.team.TeamListPage;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;

/**
 * @author Karabaza Anton on 23.03.2016.
 */
public class ReadBaseTest {
    TeamListPage teamListPage;
    TeamDal team;

    @BeforeMethod
    public void setUp() throws Exception {

        team = TeamDal.newBuilder()
                .setName(RandomStringUtils.random(10, true, false))
                .setCoach(RandomStringUtils.random(7, true, false))
                .setAchievements(RandomStringUtils.random(13, true, false))
                .setCaptainId(new PlayerRepository()
                        .create(PlayerToDal.getInstance().map(PlayerTestData.getValidPlayer()))
                        .getId())
                .build();

        team = new TeamRepository().create(team);

        WebDriverUtils.load(PageUrls.TEAMS_LIST_URL);
        teamListPage = new TeamListPage();
    }

    @AfterTest
    public final void tearDown() {
        WebDriverUtils.stop();
    }
}
