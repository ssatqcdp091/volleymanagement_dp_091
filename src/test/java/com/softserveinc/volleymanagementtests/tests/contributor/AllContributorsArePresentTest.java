package com.softserveinc.volleymanagementtests.tests.contributor;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.contributors.ContributorListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.contributor.ContributorTeam;
import com.softserveinc.volleymanagementtests.testdata.contributor.ContributorTestData;
import com.softserveinc.volleymanagementtests.tools.AllureOnFailListener;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;
import java.util.List;

/**
 * Test based on TC "TC1_ContributorsRead_ContentEquals".
 *
 * @author Danil Zhyliaiev
 */
@Features("Contributors")
@Stories("Read")
@Listeners({AllureOnFailListener.class})
public class AllContributorsArePresentTest {
    @BeforeMethod
    public void setUp() {
        WebDriverUtils.load(PageUrls.CONTRIBUTORS_LIST_URL);
    }

    @AfterMethod
    public void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void testAllContributorsArePresent() throws SQLException, ClassNotFoundException {
        ContributorListPage contributorListPage = new ContributorListPage();

        List<ContributorTeam> expectedTeams = ContributorTestData.getAllExistingTeams();
        List<ContributorTeam> actualTeams = contributorListPage.getAllTeams();

        Specification specification = Specification.get()
                .forThe(actualTeams)
                .listSizeEquals(expectedTeams.size())
                .next();

        verifyIfExpectedTeamsContainEveryActualTeam(specification, expectedTeams, actualTeams);

        specification.check();
    }

    private void verifyIfExpectedTeamsContainEveryActualTeam(
            Specification specification,
            List<ContributorTeam> expected,
            List<ContributorTeam> actual) {
        for (ContributorTeam contributorTeam : actual) {
            specification.forThe(contributorTeam)
                    .containedIn(expected);
        }
    }
}
