package com.softserveinc.volleymanagementtests.tests.player.update;

import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.tests.player.update.base.BaseUpdateFieldValidationTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC5_PlayerUpdate_HeightInvalidValuesValidation".
 *
 * @author Danil Zhyliaiev
 */
@Features("Player")
@Stories("Update")
public class PlayerUpdateHeightInvalidValuesValidationTest extends BaseUpdateFieldValidationTest {
    /**
     * DataProvider for test method containing players with preset invalid
     * height field and expected error messages.
     *
     * @return Player-String pairs as {@code Object[][]}.
     */
    @DataProvider
    private Object[][] invalidPlayerProvider() {
        return new Object[][]{
                {PlayerTestData
                        .getInvalidPlayerByHeight(Violations.MAX_VALUE_VIOLATED),
                        ErrorMessages.PLAYER_HEIGHT_RANGE_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByHeight(Violations.MIN_VALUE_VIOLATED),
                        ErrorMessages.PLAYER_HEIGHT_RANGE_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByHeight(Violations.CONTAINS_LETTERS),
                        ErrorMessages.PLAYER_HEIGHT_INVALID_INPUT_ERROR},
                {PlayerTestData.getInvalidPlayerByHeight(Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        ErrorMessages.PLAYER_HEIGHT_INVALID_INPUT_ERROR},
                {PlayerTestData.getInvalidPlayerByHeight(Violations.NEGATIVE_NUMBER),
                        ErrorMessages.PLAYER_HEIGHT_RANGE_ERROR},
                {PlayerTestData.getInvalidPlayerByHeight(Violations.NOT_INTEGER_NUMBER_WITH_COMMA),
                        ErrorMessages.PLAYER_HEIGHT_INVALID_INPUT_ERROR}
        };
    }

    @Test(dataProvider = "invalidPlayerProvider")
    public final void testPlayerUpdateHeightInvalidValuesValidation(
            final Player invalidPlayer, final String error) {
        playerEditPage.setHeight(invalidPlayer.getHeight());
        playerEditPage.saveWithInvalidFields();

        Specification.get()
                .forThe(playerEditPage.getHeightInput())
                    .textMatch(invalidPlayer.getHeight())
                    .next()
                .forThe(playerEditPage.getHeightErrorLabel())
                    .textMatch(error)
                    .next()
                .check();
    }

    @Test
    public final void testPlayerUpdateWhereHeightIsNotIntegerWithDotSeparator() {
        Player invalidPlayer = PlayerTestData
                .getInvalidPlayerByHeight(Violations.NOT_INTEGER_NUMBER_WITH_DOT);
        String error = ErrorMessages.PLAYER_HEIGHT_INVALID_INPUT_ERROR;

        playerEditPage.setHeight(invalidPlayer.getHeight());
        playerEditPage.saveWithInvalidFields();

        Specification.get()
                .forThe(playerEditPage.getHeightInput())
                    .textMatch(invalidPlayer.getHeight())
                    .next()
                .forThe(playerEditPage.getHeightValidationErrorLabel())
                    .textMatch(error)
                    .next()
                .check();

    }
}
