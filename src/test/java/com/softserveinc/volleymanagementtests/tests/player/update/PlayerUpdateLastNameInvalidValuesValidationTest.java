package com.softserveinc.volleymanagementtests.tests.player.update;

import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.tests.player.update.base.BaseUpdateFieldValidationTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC3_PlayerUpdate_LastNameInvalidValuesValidation".
 *
 * @author Danil Zhyliaiev
 */
@Features("Player")
@Stories("Update")
public class PlayerUpdateLastNameInvalidValuesValidationTest extends BaseUpdateFieldValidationTest {
    /**
     * DataProvider for test method containing players with random invalid
     * last name field and expected error messages.
     *
     * @return Player-String pairs as {@code Object[][]}.
     */
    @DataProvider
    private Object[][] invalidPlayerProvider() {
        return new Object[][]{
                {PlayerTestData.getInvalidPlayerByLastName(Violations.EMPTY_FIELD),
                        ErrorMessages.PLAYER_LAST_NAME_CAN_NOT_BE_EMPTY_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByLastName(Violations.MAX_LENGTH_VIOLATED),
                        ErrorMessages.PLAYER_LAST_NAME_LENGTH_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByLastName(Violations.CONTAINS_NUMBERS),
                        ErrorMessages.PLAYER_LAST_NAME_MUST_CONTAIN_ONLY_LETTERS_ERROR},
                {PlayerTestData.getInvalidPlayerByLastName(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        ErrorMessages.PLAYER_LAST_NAME_MUST_CONTAIN_ONLY_LETTERS_ERROR}
        };
    }

    @Test(dataProvider = "invalidPlayerProvider")
    public final void testPlayerUpdateLastNameInvalidValuesValidation(
            final Player invalidPlayer, final String error) {
        playerEditPage.setLastName(invalidPlayer.getLastName());
        playerEditPage.saveWithInvalidFields();

        Specification.get()
                .forThe(playerEditPage.getLastNameInput())
                    .textMatch(invalidPlayer.getLastName())
                    .next()
                .forThe(playerEditPage.getLastNameErrorLabel())
                    .textMatch(error)
                    .next()
                .check();
    }
}
