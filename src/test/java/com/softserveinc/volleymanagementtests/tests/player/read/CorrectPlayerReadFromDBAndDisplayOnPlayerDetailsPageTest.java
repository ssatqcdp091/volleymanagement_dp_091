package com.softserveinc.volleymanagementtests.tests.player.read;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Karabaza Anton on 06.03.2016.
 */
@Features("Player")
@Stories("Read")
public class CorrectPlayerReadFromDBAndDisplayOnPlayerDetailsPageTest {

    private PlayersListPage playersListPage;
    private PlayerDal firstPlayer;
    private PlayerDal secondPlayer;

    @BeforeMethod
    public void setUp() throws Exception {
        PlayerRepository playerRepository = new PlayerRepository();
        firstPlayer = new PlayerDal.PlayerDalBuilder()
                .id(null)
                .firstName("FirstPlayerName")
                .lastName("FirstPlayerLastName")
                .birthYear(1980)
                .height(180)
                .weight(85)
                .build();
        secondPlayer = new PlayerDal.PlayerDalBuilder()
                .id(null)
                .firstName("SecondPlayerName")
                .lastName("SecondPlayerLastName")
                .birthYear(1978)
                .height(183)
                .weight(90)
                .build();

        playerRepository.create(firstPlayer);
        playerRepository.create(secondPlayer);

        WebDriverUtils.load(PageUrls.PLAYERS_LIST_URL);
        playersListPage = new PlayersListPage();
    }

    @AfterTest
    public final void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void testCorrectPlayerRead() {

        Specification.get()
                .forThe(playersListPage)
                .isPlayerDisplayedOnListWithCorrectDetails(firstPlayer)
                .next()
                .forThe(new PlayersListPage())
                .isPlayerDisplayedOnListWithCorrectDetails(secondPlayer)
                .next()
                .check();
    }
}
