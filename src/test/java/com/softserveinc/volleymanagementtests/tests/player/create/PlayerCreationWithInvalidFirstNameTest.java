package com.softserveinc.volleymanagementtests.tests.player.create;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Player")
@Stories("Create")
public class PlayerCreationWithInvalidFirstNameTest extends TestBase {

    @DataProvider
    public final Object[][] validDataProvider() {

        return new Object[][]{
                {PlayerTestData
                        .getInvalidPlayerByFirstName(
                        Violations.EMPTY_FIELD),
                        ErrorMessages.PLAYER_FIRST_NAME_CAN_NOT_BE_EMPTY_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByFirstName(
                        Violations.MAX_LENGTH_VIOLATED),
                        ErrorMessages.PLAYER_FIRST_NAME_LENGTH_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByFirstName(
                        Violations.CONTAINS_NUMBERS),
                        ErrorMessages.
                                PLAYER_FIRST_NAME_MUST_CONTAIN_ONLY_LETTERS_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByFirstName(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        ErrorMessages.
                                PLAYER_FIRST_NAME_MUST_CONTAIN_ONLY_LETTERS_ERROR}};
    }

    @Test(dataProvider = "validDataProvider")
    public  void testPlayerCreationWithInvaliFirstName( Player player,
                                                        String errorMessage)throws Exception  {

        PlayerRepository playerRepository = new PlayerRepository();
        PlayerCreatePage playerCreatePage = new PlayerCreatePage();
        playerCreatePage.setAllPlayerFields(player);
        playerCreatePage.submitButtonCreate();

        Specification.get().forThe(playerCreatePage).isAllFieldsFilledInCorrectly(player).next()
                .forThe(playerCreatePage.getErrorInputFirstNameMessage())
                .textMatch(errorMessage).next().check();


        Assert.assertFalse(playerRepository.isPlayerExistsInDataBaseByLastName(player.getLastName()));
    }
}
