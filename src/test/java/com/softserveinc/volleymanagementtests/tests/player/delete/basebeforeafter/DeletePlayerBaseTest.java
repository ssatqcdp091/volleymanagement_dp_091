package com.softserveinc.volleymanagementtests.tests.player.delete.basebeforeafter;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

/**
 * preconditions and after test actions on deleting the player.
 */
public class DeletePlayerBaseTest {

    protected PlayersListPage playersListPage;
    protected PlayerDal playerDal;

    @BeforeClass()
    public final void setUp() throws Exception {

        // set the player to db
        playerDal = new PlayerRepository()
                .create(PlayerToDal.getInstance().map(PlayerTestData.getValidPlayer()));

        //load
        WebDriverUtils.load(PageUrls.PLAYERS_LIST_URL);
        playersListPage = new PlayersListPage();
    }

    @AfterMethod
    public final void afterMethod() {
        // close the unexpected alert in order the webDriver closed with no exceptions
        while (true) {
            try {
                Alert alert = playersListPage.getAlert();
                alert.dismiss();
            } catch (NoAlertPresentException e) {
                break;
            }
        }
    }

    @AfterClass
    public final void tearDown() throws Exception {

        if (new PlayerRepository().getAll().contains(playerDal)) {
            new PlayerRepository().deleteById(playerDal.getId());
        }

        WebDriverUtils.stop();
    }
}
