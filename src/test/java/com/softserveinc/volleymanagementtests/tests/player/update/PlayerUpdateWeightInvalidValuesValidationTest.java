package com.softserveinc.volleymanagementtests.tests.player.update;

import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.tests.player.update.base.BaseUpdateFieldValidationTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC6_PlayerUpdate_WeightInvalidValuesValidation".
 *
 * @author Danil Zhyliaiev
 */
@Features("Player")
@Stories("Update")
public class PlayerUpdateWeightInvalidValuesValidationTest extends BaseUpdateFieldValidationTest {
    /**
     * DataProvider for test method containing players with preset invalid
     * weight field and expected error messages.
     *
     * @return Player-String pairs as {@code Object[][]}.
     */
    @DataProvider
    private Object[][] invalidPlayerProvider() {
        return new Object[][]{
                {PlayerTestData
                        .getInvalidPlayerByWeight(Violations.MAX_VALUE_VIOLATED),
                        ErrorMessages.PLAYER_WEIGHT_RANGE_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByWeight(Violations.MIN_VALUE_VIOLATED),
                        ErrorMessages.PLAYER_WEIGHT_RANGE_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByWeight(Violations.CONTAINS_LETTERS),
                        ErrorMessages.PLAYER_WEIGHT_INVALID_INPUT_ERROR},
                {PlayerTestData.getInvalidPlayerByWeight(Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        ErrorMessages.PLAYER_WEIGHT_INVALID_INPUT_ERROR},
                {PlayerTestData.getInvalidPlayerByWeight(Violations.NEGATIVE_NUMBER),
                        ErrorMessages.PLAYER_WEIGHT_RANGE_ERROR},
                {PlayerTestData.getInvalidPlayerByWeight(Violations.NOT_INTEGER_NUMBER_WITH_COMMA),
                        ErrorMessages.PLAYER_WEIGHT_INVALID_INPUT_ERROR}
        };
    }

    @Test(dataProvider = "invalidPlayerProvider")
    public final void testPlayerUpdateWeightInvalidValuesValidation(
            final Player invalidPlayer, final String error) {
        playerEditPage.setWeight(invalidPlayer.getWeight());
        playerEditPage.saveWithInvalidFields();

        Specification.get()
                .forThe(playerEditPage.getWeightInput())
                    .textMatch(invalidPlayer.getWeight())
                    .next()
                .forThe(playerEditPage.getWeightErrorLabel())
                    .textMatch(error)
                    .next()
                .check();
    }

    @Test
    public final void testPlayerUpdateWhereWeightIsNotIntegerWithDotSeparator() {
        Player invalidPlayer = PlayerTestData
                .getInvalidPlayerByWeight(Violations.NOT_INTEGER_NUMBER_WITH_DOT);
        String error = ErrorMessages.PLAYER_WEIGHT_INVALID_INPUT_ERROR;

        playerEditPage.setWeight(invalidPlayer.getWeight());
        playerEditPage.saveWithInvalidFields();

        Specification.get()
                .forThe(playerEditPage.getWeightInput())
                    .textMatch(invalidPlayer.getWeight())
                    .next()
                .forThe(playerEditPage.getWeightValidationErrorLabel())
                    .textMatch(error)
                    .next()
                .check();
    }
}
