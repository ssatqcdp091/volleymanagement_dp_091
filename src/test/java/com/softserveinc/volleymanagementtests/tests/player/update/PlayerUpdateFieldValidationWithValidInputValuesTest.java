package com.softserveinc.volleymanagementtests.tests.player.update;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayerEditPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.DalToPlayer;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.AllureOnFailListener;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC1_PayerUpdate_ValidValues".
 *
 * @author Danil Zhyliaiev
 */
@Features("Player")
@Stories("Update")
@Listeners({AllureOnFailListener.class})
public class PlayerUpdateFieldValidationWithValidInputValuesTest {
    private PlayersListPage playersListPage;
    private PlayerEditPage playerEditPage;

    @BeforeMethod
    public final void setUp() throws Exception {
        WebDriverUtils.load(PageUrls.PLAYERS_LIST_URL);

        playersListPage = new PlayersListPage();

        if (playersListPage.getCountOfPlayersOnCurrentPage() == 0) {
            new PlayerRepository().create(PlayerToDal.getInstance()
                    .map(PlayerTestData.getValidPlayer()));

            WebDriverUtils.refresh();

            playersListPage = new PlayersListPage();
        }

        playerEditPage = playersListPage.editRandomPlayer();
    }

    @AfterTest
    public final void tearDown() {
        WebDriverUtils.stop();
    }

    /**
     * DataProvider for test method with preset of valid players.
     *
     * @return preset of valid players as {@code Object[][]}.
     */
    @DataProvider
    private Object[][] validPlayerProvider() {
        int validPlayersCount = PlayerTestData.getValidPlayersList().size();
        Player[] validPlayers = new Player[validPlayersCount];
        PlayerTestData.getValidPlayersList().toArray(validPlayers);

        Object[][] data = new Object[validPlayersCount][1];

        for (int i = 0; i < validPlayersCount; i++) {
            data[i][0] = validPlayers[i];
        }

        return data;
    }

    @Test(dataProvider = "validPlayerProvider")
    public final void testPlayerUpdateFieldValidationWithValidInputValues(
            final Player player) throws Exception {
        int id = playerEditPage.getPlayerID();

        playerEditPage.setPlayer(player);

        Specification specification = Specification.get()
                .forThe(playerEditPage.readPlayer())
                    .equalsTo(player)
                    .next();

        try {
            playersListPage = playerEditPage.saveWithValidFields();
        } catch (RuntimeException e) {
            if (playerEditPage.getErrorLabels().size() != 0) {
                specification.forThe(playerEditPage.getErrorLabels())
                        .labelTextsMatch("");
            }
        }

        specification.forThe(player)
                .equalsTo(DalToPlayer.getInstance().map(new PlayerRepository().getById(id)))
                .next()
            .check();
    }
}
