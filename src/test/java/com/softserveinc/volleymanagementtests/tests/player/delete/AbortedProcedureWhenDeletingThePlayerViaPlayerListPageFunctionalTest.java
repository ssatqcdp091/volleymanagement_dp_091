package com.softserveinc.volleymanagementtests.tests.player.delete;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tests.player.delete.basebeforeafter.DeletePlayerBaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * testing the delete player functionality via the ui tools.
 * aborted on half-way.
 */
@Features("Player")
@Stories("Delete")
public class AbortedProcedureWhenDeletingThePlayerViaPlayerListPageFunctionalTest
        extends DeletePlayerBaseTest {
    @Test
    public final void testAbortedProcedureWhenDeletingThePlayerViaPlayerListPageFunctional()
            throws Exception {
        playersListPage
                .clickDeleteLink(playerDal.getId())
                .dismiss(); // are u sure u want to delete?

        Specification.get()
                // there should not be any Alert on the page
                .forThe(playersListPage)
                .ifUnexpectedAlertPresent()
                .next()

                // the player should be displayed on the page
                .forThe(new PlayerRepository().getById(playerDal.getId()).equals(playerDal))
                .isTrue()
                .next()
                .check();
    }
}
