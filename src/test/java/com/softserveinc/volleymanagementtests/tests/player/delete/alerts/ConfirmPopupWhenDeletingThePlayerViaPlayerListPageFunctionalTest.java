package com.softserveinc.volleymanagementtests.tests.player.delete.alerts;

import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.AlertMessages;
import com.softserveinc.volleymanagementtests.tests.player.delete.basebeforeafter.DeletePlayerBaseTest;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Player")
@Stories("Delete")
public class ConfirmPopupWhenDeletingThePlayerViaPlayerListPageFunctionalTest
        extends DeletePlayerBaseTest {

    @Test
    public final void testConfirmPopupWhenDeletingThePlayerViaPlayerListPageFunctional() {

        playersListPage
                .clickDeleteLink(playerDal.getId())
                .accept(); // are u sure u want to delete?

        // get the confirm modal dialog to validate it
        Alert confirm = playersListPage.getAlert();

        Specification.get()
                .forThe(confirm)
                .textMatch(AlertMessages.THE_PLAYER_WAS_DELETED)
                .next().check();

        // to get driver back to the parent frame
        confirm.accept();

        Specification.get()
                // there should not be any Alert on the page
                .forThe(playersListPage)
                .ifUnexpectedAlertPresent()
                .next().check();

        throw new AssertionError("floating bug is here. shown when run with debugger (stepover)");

    }
}
