package com.softserveinc.volleymanagementtests.tests.player.create;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC_ Valid Player Creation".
 * @author S.Tsyganovskiy
 */
@Features("Player")
@Stories("Create")
public class PlayerCreationWithValidDataTest extends TestBase{

    @DataProvider
    public  Object[][] validDataProvider() {
        int validPlayersCount = PlayerTestData.getValidPlayersList().size();
        Player[] validPlayers = new Player[validPlayersCount];
        PlayerTestData.getValidPlayersList().toArray(validPlayers);

        Object[][] data = new Object[validPlayersCount][1];

        for (int i = 0; i < validPlayersCount; i++) {
            data[i][0] = validPlayers[i];
        }
        return data;
    }

    @Test(dataProvider = "validDataProvider")
    public  void testValidPlayerCreation(Player player) throws Exception{
        PlayerRepository playerRepository = new PlayerRepository();
        if (playerRepository.isPlayerExistsInDataBase(PlayerToDal.getInstance().map(player))){
            playerRepository.deletePlayer(PlayerToDal.getInstance().map(player));
        }

        PlayerCreatePage playerCreatePage = new PlayerCreatePage();
        playerCreatePage.setAllPlayerFields(player);
        Specification.get().forThe(playerCreatePage).isAllFieldsFilledInCorrectly(player).next().check();

        playerCreatePage.submitButtonCreate();
        Assert.assertTrue(playerRepository.isPlayerExistsInDataBase(PlayerToDal.getInstance().map(player)));
    }
}
