package com.softserveinc.volleymanagementtests.tests.player.update;

import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.tests.player.update.base.BaseUpdateFieldValidationTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC2_PlayerUpdate_FirstNameInvalidValuesValidation".
 *
 * @author Danil Zhyliaiev
 */
@Features("Player")
@Stories("Update")
public class PlayerUpdateFirstNameInvalidValuesValidationTest
        extends BaseUpdateFieldValidationTest {
    /**
     * DataProvider for test method containing players with random invalid
     * first name field and expected error messages.
     *
     * @return Player-String pairs as {@code Object[][]}.
     */
    @DataProvider
    private Object[][] invalidPlayerProvider() {
        return new Object[][]{
                {PlayerTestData
                        .getInvalidPlayerByFirstName(Violations.EMPTY_FIELD),
                        ErrorMessages.PLAYER_FIRST_NAME_CAN_NOT_BE_EMPTY_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByFirstName(Violations.MAX_LENGTH_VIOLATED),
                        ErrorMessages.PLAYER_FIRST_NAME_LENGTH_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByFirstName(Violations.CONTAINS_NUMBERS),
                        ErrorMessages.PLAYER_FIRST_NAME_MUST_CONTAIN_ONLY_LETTERS_ERROR},
                {PlayerTestData.getInvalidPlayerByFirstName(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        ErrorMessages.PLAYER_FIRST_NAME_MUST_CONTAIN_ONLY_LETTERS_ERROR}
        };
    }

    @Test(dataProvider = "invalidPlayerProvider")
    public final void testPlayerUpdateFirstNameInvalidValuesValidation(
            final Player invalidPlayer, final String error) {
        playerEditPage.setFirstName(invalidPlayer.getFirstName());
        playerEditPage.saveWithInvalidFields();

        Specification.get()
                .forThe(playerEditPage.getFirstNameInput())
                    .textMatch(invalidPlayer.getFirstName())
                    .next()
                .forThe(playerEditPage.getNameErrorLabel())
                    .textMatch(error)
                    .next()
                .check();
    }
}
