package com.softserveinc.volleymanagementtests.tests.player.pagination;

import com.softserveinc.volleymanagementtests.dal.models.PlayerDal;
import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Karabaza Anton on 15.03.2016.
 */
@Features("Player")
@Stories("Pagination")
public class PaginationTest {
    private PlayersListPage playersListPage;

    @BeforeMethod
    public void setUp() throws Exception {
        WebDriverUtils.load(PageUrls.PLAYERS_LIST_URL);
        playersListPage = new PlayersListPage();

        if (playersListPage.getPaginator().size() < 2) {
            PlayerRepository playerRepository = new PlayerRepository();
            PlayerDal player;

            player = new PlayerDal.PlayerDalBuilder()
                    .id(null)
                    .firstName("FirstPlayerName")
                    .lastName("FirstPlayerLastName")
                    .birthYear(1980)
                    .height(180)
                    .weight(85)
                    .build();

            for (int i = 0; i < 11; i++) {
                playerRepository.create(player);
            }

            WebDriverUtils.refresh();
            playersListPage = new PlayersListPage();
        }
    }

    @AfterTest
    public final void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void testPagination() {
        Specification specification = Specification.get();

        playersListPage.goToPage(2);
        specification.forThe(playersListPage)
                .matchPageIndex("2");

        playersListPage.goToPage(3);
        specification.forThe(playersListPage)
                .matchPageIndex("3");

        playersListPage.goToPage(1);
        specification.forThe(playersListPage)
                .matchPageIndex("1")
                .next()
                .check();
    }
}
