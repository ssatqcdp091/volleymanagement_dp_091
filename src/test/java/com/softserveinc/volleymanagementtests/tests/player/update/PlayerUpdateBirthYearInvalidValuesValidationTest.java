package com.softserveinc.volleymanagementtests.tests.player.update;

import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import com.softserveinc.volleymanagementtests.tests.player.update.base.BaseUpdateFieldValidationTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC4_PlayerUpdate_BirthYearInvalidValuesValidation".
 *
 * @author Danil Zhyliaiev
 */
@Features("Player")
@Stories("Update")
public class PlayerUpdateBirthYearInvalidValuesValidationTest
        extends BaseUpdateFieldValidationTest {
    /**
     * DataProvider for test method containing players with preset invalid
     * birth year field and expected error messages.
     *
     * @return Player-String pairs as {@code Object[][]}.
     */
    @DataProvider
    private Object[][] invalidPlayerProvider() {
        return new Object[][]{
                {PlayerTestData
                        .getInvalidPlayerByBirthYear(Violations.MAX_VALUE_VIOLATED),
                        ErrorMessages.PLAYER_BIRTH_YEAR_RANGE_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByBirthYear(Violations.MIN_VALUE_VIOLATED),
                        ErrorMessages.PLAYER_BIRTH_YEAR_RANGE_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByBirthYear(Violations.CONTAINS_LETTERS),
                        ErrorMessages.PLAYER_BIRTH_YEAR_INVALID_INPUT_ERROR},
                {PlayerTestData.getInvalidPlayerByBirthYear(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        ErrorMessages.PLAYER_BIRTH_YEAR_INVALID_INPUT_ERROR},
                {PlayerTestData.getInvalidPlayerByBirthYear(Violations.NEGATIVE_NUMBER),
                        ErrorMessages.PLAYER_BIRTH_YEAR_RANGE_ERROR},
                {PlayerTestData.getInvalidPlayerByBirthYear(
                        Violations.NOT_INTEGER_NUMBER_WITH_COMMA),
                        ErrorMessages.PLAYER_BIRTH_YEAR_INVALID_INPUT_ERROR}
        };
    }

    @Test(dataProvider = "invalidPlayerProvider")
    public final void testPlayerUpdateBirthYearInvalidValuesValidation(
            final Player invalidPlayer, final String error) {
        playerEditPage.setBirthYear(invalidPlayer.getBirthYear());
        playerEditPage.saveWithInvalidFields();

        Specification.get()
                .forThe(playerEditPage.getBirthYearInput())
                    .textMatch(invalidPlayer.getBirthYear())
                    .next()
                .forThe(playerEditPage.getBirthYearErrorLabel())
                    .textMatch(error)
                    .next()
                .check();
    }


    @Test
    public final void testPlayerUpdateWhereBirthYearIsNotIntegerWithDotSeparator() {
        Player invalidPlayer = PlayerTestData
                .getInvalidPlayerByBirthYear(Violations.NOT_INTEGER_NUMBER_WITH_DOT);
        String error = ErrorMessages.PLAYER_BIRTH_YEAR_INVALID_INPUT_ERROR;

        playerEditPage.setBirthYear(invalidPlayer.getBirthYear());
        playerEditPage.saveWithInvalidFields();

        Specification.get()
                .forThe(playerEditPage.getBirthYearInput())
                    .textMatch(invalidPlayer.getBirthYear())
                    .next()
                .forThe(playerEditPage.getBirthYearValidationErrorLabel())
                    .textMatch(error)
                    .next()
                .check();
    }
}
