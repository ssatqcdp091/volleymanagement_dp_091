package com.softserveinc.volleymanagementtests.tests.player.delete.alerts;

import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.AlertMessages;
import com.softserveinc.volleymanagementtests.tests.player.delete.basebeforeafter.DeletePlayerBaseTest;
import com.softserveinc.volleymanagementtests.tools.controls.contracts.Alert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * validating the Alert popup which appears during the deleting the player
 * via ui from the PlayersListPage.
 */
@Features("Player")
@Stories("Delete")
public class AlertPopupWhenDeletingThePlayerViaPlayerListPageFunctionalTest
        extends DeletePlayerBaseTest {
    @Test
    public final void testAlertPopupWhenDeletingThePlayerViaPlayerListPageFunctional() {

        // get the alert modal dialog to validate it
        Alert alert = playersListPage
                .clickDeleteLink(playerDal.getId()); // are u sure u want to delete?

        Specification.get()
                .forThe(alert)
                .textMatch(String.format(
                        AlertMessages.DO_YOU_REALLY_WANT_TO_DELETE_THE_PLAYER,
                        playerDal.getLastName().concat(" ").concat(playerDal.getFirstName())))
                .next().check();

        // to get driver back to the parent frame
        alert.dismiss();

        Specification.get()
                // there should not be any Alert on the page
                .forThe(playersListPage)
                .ifUnexpectedAlertPresent()
                .next().check();
    }
}
