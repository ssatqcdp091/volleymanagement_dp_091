package com.softserveinc.volleymanagementtests.tests.player.search;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

/**
 * @author J.Bodnar
 */
@Features("Player")
@Stories("Search")
public class PlayerSearchTest {

    @BeforeClass
    public void setUp() throws SQLException, ClassNotFoundException {

        WebDriverUtils.load(PageUrls.PLAYERS_LIST_URL);
        for (Player player : PlayerTestData.getPlayersListForSearching()) {
            new PlayerRepository().create(PlayerToDal.getInstance().map(player));
        }

    }

    @AfterClass
    public void tearDown() throws SQLException, ClassNotFoundException {
        for (Player player : PlayerTestData.getPlayersListForSearching()) {
            new PlayerRepository().deletePlayer(PlayerToDal.getInstance().map(player));
            WebDriverUtils.stop();
        }
    }
    @DataProvider
    private Object[][] textForSearchingProvider() {
        return new Object[][]{
                {"search", 3}, {"SEARCH", 3}, {"Sear", 3},
                {"earc", 4}, {"TEXT", 3}, {"text", 3},
                {"Tex", 3}, {"exT", 4}, {"Search Text", 3},
                {"searChText", 0}, {"sea ext", 0}, {"arch te", 0},
                {"sea rch", 0}, {"sefrtgarch", 0}};
    }

    @Test(dataProvider = "textForSearchingProvider")
    public final void testPlayerSearch(final String textForSearching, final int foundRowsNumber) {
        PlayersListPage playersListPage = new PlayersListPage();
        playersListPage.searchForPlayer(textForSearching);
        Assert.assertEquals(playersListPage.getCountOfPlayersOnCurrentPage() ,foundRowsNumber);

    }

}
