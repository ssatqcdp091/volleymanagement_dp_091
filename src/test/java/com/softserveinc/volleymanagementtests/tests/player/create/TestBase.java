package com.softserveinc.volleymanagementtests.tests.player.create;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.dal.repositories.TeamRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.sql.SQLException;


public class TestBase {
    @BeforeMethod
    public final void setUp() throws SQLException, ClassNotFoundException{
        WebDriverUtils.load(PageUrls.PLAYER_CREATE_URL);
        PlayerCreatePage playerCreatePage = new PlayerCreatePage();
        playerCreatePage.logInAsCaptain();
        clearDataBase();
    }
    @AfterMethod
    public final void tearDown() {
        WebDriverUtils.stop();
    }

    private void clearDataBase() throws SQLException, ClassNotFoundException{
        new PlayerRepository().deleteAll();
        new TeamRepository().deleteAll();
    }

}

