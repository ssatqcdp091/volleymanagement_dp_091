package com.softserveinc.volleymanagementtests.tests.player.create;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC_InValid By Heigth Player Creation".
 *
 * @author S.Tsyganovskiy
 */
@Features("Player")
@Stories("Create")
public class PlayerCreationWithInvalidHeigthTest extends TestBase{
    @DataProvider
    public final Object[][] validDataProvider() {

        return new Object[][]{
                {PlayerTestData
                        .getInvalidPlayerByHeight(
                        Violations.MAX_VALUE_VIOLATED),
                        ErrorMessages.PLAYER_HEIGHT_RANGE_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByHeight(
                        Violations.MIN_VALUE_VIOLATED),
                        ErrorMessages.PLAYER_HEIGHT_RANGE_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByHeight(
                        Violations.CONTAINS_LETTERS),
                        ErrorMessages
                                .PLAYER_HEIGHT_INVALID_INPUT_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByHeight(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        ErrorMessages
                                .PLAYER_HEIGHT_INVALID_INPUT_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByHeight(
                        Violations.NEGATIVE_NUMBER),
                        ErrorMessages
                                .PLAYER_HEIGHT_INVALID_INPUT_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByHeight(
                        Violations.NOT_INTEGER_NUMBER),
                        ErrorMessages
                                .PLAYER_HEIGHT_INVALID_INPUT_ERROR}};

    }

    @Test(dataProvider = "validDataProvider")
    public  void testPlayerCreationWithInvalidHeigt( Player player,
                                                     String errorMessage) throws Exception{

        PlayerRepository playerRepository = new PlayerRepository();
        PlayerCreatePage playerCreatePage = new PlayerCreatePage();
        playerCreatePage.setAllPlayerFields(player);
        playerCreatePage.submitButtonCreate();

        Specification.get().forThe(playerCreatePage).isAllFieldsFilledInCorrectly(player).next()
                .forThe(playerCreatePage.getErrorInputHeigthMessage())
                .textMatch(errorMessage).next().check();


        Assert.assertFalse(playerRepository.isPlayerExistsInDataBaseByLastName(player.getLastName()));
    }
}
