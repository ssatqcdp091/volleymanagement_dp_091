package com.softserveinc.volleymanagementtests.tests.player.delete;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tests.player.delete.basebeforeafter.DeletePlayerBaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * testing the delete player functionality via the ui tools.
 * end to end.
 */
@Features("Player")
@Stories("Delete")
public class CompletedProcedureWhenDeletingThePlayerViaPlayerListPageFunctionalTest
        extends DeletePlayerBaseTest {

    @Test
    public final void testCompletedProcedureWhenDeletingThePlayerViaPlayerListPageFunctional()
            throws Exception {

        // later, to check if the presetting are not changed after manipulations on the page
        String playerPageIndexAt = String.valueOf(playersListPage.getPlayerPageIndexAt(playerDal.getId()));

        playersListPage
                .clickDeleteLink(playerDal.getId())
                .accept(); // are u sure u want to delete?

        playersListPage.getAlert()
                .accept(); // was deleted!

        // ...the page should have been refreshed...

        Specification.get()
                // there should not be any Alert on the page
                .forThe(playersListPage)
                .ifUnexpectedAlertPresent() // floating bug here // shown when run with debugger (stepover)
                // the page index should be the same as the deleted player have been at.
                .matchPageIndex(playerPageIndexAt)
                .next()

                // there should not exist the player in the db
                // if we use getById-the exception will be thrown
                .forThe(new PlayerRepository().getAll().contains(playerDal))
                .isFalse()
                .next()

                .check();

        throw new AssertionError("floating bug is here. shown when run with debugger (stepover)");

    }
}
