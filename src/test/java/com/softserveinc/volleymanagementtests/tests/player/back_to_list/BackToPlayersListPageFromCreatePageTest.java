package com.softserveinc.volleymanagementtests.tests.player.back_to_list;

import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;


@Features("Player")
@Stories("Back link")
public class BackToPlayersListPageFromCreatePageTest {
    @BeforeMethod
    public void setUp() {
        WebDriverUtils.load(PageUrls.PLAYER_CREATE_URL);
    }

    @AfterMethod
    public void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void testCheckBackToThePlayersListLink() {

       PlayersListPage playersListPage =   new PlayerCreatePage().clickBackToList();
        Specification.get().forThe(playersListPage.getPlayersLabel()).textMatch("Игроки").next().check();

    }
}
