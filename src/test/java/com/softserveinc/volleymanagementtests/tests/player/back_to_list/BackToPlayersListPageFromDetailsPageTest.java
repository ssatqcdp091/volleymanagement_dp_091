package com.softserveinc.volleymanagementtests.tests.player.back_to_list;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayerDetailsPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.sql.SQLException;

/**
 * @author Karabaza Anton on 20.03.2016.
 */
@Features("Player")
@Stories("Back link")
public class BackToPlayersListPageFromDetailsPageTest {

    PlayerDetailsPage playerDetailsPage;
    PlayersListPage playersListPage;

    @BeforeMethod
    public void setUp() throws SQLException, ClassNotFoundException {

        WebDriverUtils.load(PageUrls.PLAYERS_LIST_URL);
        playersListPage = new PlayersListPage();

        if(playersListPage.getCountOfPlayersOnCurrentPage() == 0) {
            new PlayerRepository().create(PlayerToDal.getInstance()
                    .map(PlayerTestData.getValidPlayer()));

            WebDriverUtils.refresh();
            playersListPage = new PlayersListPage();
        }

        playerDetailsPage = playersListPage.showRandomPlayerDetails();
    }

    @AfterMethod
    public void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void testBackLinkOnDetailsPage() throws SQLException, ClassNotFoundException {

        playersListPage = playerDetailsPage.clickBackLink();

        Specification.get()
                .forThe(playersListPage)
                .matchURL(PageUrls.PLAYERS_LIST_URL)
                .next()
                .check();
    }
}
