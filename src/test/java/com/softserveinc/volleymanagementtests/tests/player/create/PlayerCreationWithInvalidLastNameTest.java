package com.softserveinc.volleymanagementtests.tests.player.create;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC_InValid By Last Name Player Creation".
 *
 * @author S.Tsyganovskiy
 */
@Features("Player")
@Stories("Create")
public class PlayerCreationWithInvalidLastNameTest extends TestBase{
    @DataProvider
    public final Object[][] validDataProvider() {

        return new Object[][]{
                {PlayerTestData
                        .getInvalidPlayerByLastName(
                        Violations.EMPTY_FIELD),
                        ErrorMessages.PLAYER_LAST_NAME_CAN_NOT_BE_EMPTY_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByLastName(
                        Violations.MAX_LENGTH_VIOLATED),
                        ErrorMessages.PLAYER_LAST_NAME_LENGTH_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByLastName(
                        Violations.CONTAINS_NUMBERS),
                        ErrorMessages
                                .PLAYER_LAST_NAME_MUST_CONTAIN_ONLY_LETTERS_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByLastName(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        ErrorMessages
                                .PLAYER_LAST_NAME_MUST_CONTAIN_ONLY_LETTERS_ERROR}};
    }

    @Test(dataProvider = "validDataProvider")
    public  void testPlayerCreationWithInvalidLastName( Player player,
                                                       String errorMessage) throws Exception{

        PlayerRepository playerRepository = new PlayerRepository();
        PlayerCreatePage playerCreatePage = new PlayerCreatePage();
        playerCreatePage.setAllPlayerFields(player);
        playerCreatePage.submitButtonCreate();

        Specification.get().forThe(playerCreatePage).isAllFieldsFilledInCorrectly(player).next()
                .forThe(playerCreatePage.getErrorInputLastNameMessage())
                .textMatch(errorMessage).next().check();


        Assert.assertFalse(playerRepository.isPlayerExistsInDataBaseByFirstName(player.getFirstName()));
    }
}
