package com.softserveinc.volleymanagementtests.tests.player.create;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.player.PlayerCreatePage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.ErrorMessages;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.Violations;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC_InValid By BirthYear Player Creation".
 *
 * @author S.Tsyganovskiy
 */
@Features("Player")
@Stories("Create")
public class PlayerCreationWithInvalidBirthYearTest extends TestBase {
    @DataProvider
    public final Object[][] validDataProvider() {

        return new Object[][]{
                {PlayerTestData
                        .getInvalidPlayerByBirthYear(
                        Violations.MAX_VALUE_VIOLATED),
                        ErrorMessages.PLAYER_BIRTH_YEAR_RANGE_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByBirthYear(
                        Violations.MIN_VALUE_VIOLATED),
                        ErrorMessages.PLAYER_BIRTH_YEAR_RANGE_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByBirthYear(
                        Violations.CONTAINS_LETTERS),
                        ErrorMessages
                                .PLAYER_BIRTH_YEAR_INVALID_INPUT_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByBirthYear(
                        Violations.CONTAIN_NOT_ALLOWED_CHARACTERS),
                        ErrorMessages
                                .PLAYER_BIRTH_YEAR_INVALID_INPUT_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByBirthYear(
                        Violations.NEGATIVE_NUMBER),
                        ErrorMessages
                                .PLAYER_BIRTH_YEAR_INVALID_INPUT_ERROR},
                {PlayerTestData
                        .getInvalidPlayerByBirthYear(
                        Violations.NOT_INTEGER_NUMBER),
                        ErrorMessages
                                .PLAYER_BIRTH_YEAR_INVALID_INPUT_ERROR}};

    }

    @Test(dataProvider = "validDataProvider")
    public  void testPlayerCreationWithInvalidBirthYear(Player player,
                                                        String errorMessage)throws Exception {

        PlayerRepository playerRepository = new PlayerRepository();
        PlayerCreatePage playerCreatePage = new PlayerCreatePage();
        playerCreatePage.setAllPlayerFields(player);
        playerCreatePage.submitButtonCreate();

        Specification.get().forThe(playerCreatePage).isAllFieldsFilledInCorrectly(player).next()
                .forThe(playerCreatePage.getErrorInputBirthYearMessage())
                .textMatch(errorMessage).next().check();

        Assert.assertFalse(playerRepository.isPlayerExistsInDataBaseByLastName(player.getLastName()));
    }
}
