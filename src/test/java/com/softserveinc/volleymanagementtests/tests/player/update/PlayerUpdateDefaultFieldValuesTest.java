package com.softserveinc.volleymanagementtests.tests.player.update;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayerDetailsPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayerEditPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.specification.Specification;
import com.softserveinc.volleymanagementtests.testdata.player.Player;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.AllureOnFailListener;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Test based on TC "TC7_PlayerUpdate_DefaultFieldValuesTest".
 *
 * @author Danil Zhyliaiev
 */
@Features("Player")
@Stories("Update")
@Listeners({AllureOnFailListener.class})
public class PlayerUpdateDefaultFieldValuesTest {
    private PlayerDetailsPage detailsPage;

    @BeforeMethod
    public final void setUp() throws Exception {
        WebDriverUtils.load(PageUrls.PLAYERS_LIST_URL);

        PlayersListPage playersListPage = new PlayersListPage();

        if (playersListPage.getCountOfPlayersOnCurrentPage() == 0) {
            new PlayerRepository().create(PlayerToDal.getInstance()
                    .map(PlayerTestData.getValidPlayer()));

            WebDriverUtils.refresh();

            playersListPage = new PlayersListPage();
        }

        detailsPage = playersListPage.showRandomPlayerDetails();
    }

    @AfterTest
    public final void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public final void testPlayerUpdateDefaultFieldValues() {
        Player expectedPlayer = detailsPage.readPlayer();
        PlayerEditPage playerEditPage = detailsPage.clickEditLink();
        Player actualPlayer = playerEditPage.readPlayer();

        Specification.get()
                .forThe(actualPlayer)
                    .equalsTo(expectedPlayer)
                    .next()
                .check();
    }
}
