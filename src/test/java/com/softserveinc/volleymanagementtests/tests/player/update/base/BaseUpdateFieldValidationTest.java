package com.softserveinc.volleymanagementtests.tests.player.update.base;

import com.softserveinc.volleymanagementtests.dal.repositories.PlayerRepository;
import com.softserveinc.volleymanagementtests.pages.PageUrls;
import com.softserveinc.volleymanagementtests.pages.player.PlayerEditPage;
import com.softserveinc.volleymanagementtests.pages.player.PlayersListPage;
import com.softserveinc.volleymanagementtests.testdata.player.PlayerTestData;
import com.softserveinc.volleymanagementtests.testdata.player.mappers.PlayerToDal;
import com.softserveinc.volleymanagementtests.tools.AllureOnFailListener;
import com.softserveinc.volleymanagementtests.tools.WebDriverUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

@Listeners({AllureOnFailListener.class})
public class BaseUpdateFieldValidationTest {
    protected PlayerEditPage playerEditPage;

    @BeforeMethod
    public final void setUp() throws Exception {
        WebDriverUtils.load(PageUrls.PLAYERS_LIST_URL);

        PlayersListPage playersListPage = new PlayersListPage();

        if (playersListPage.getCountOfPlayersOnCurrentPage() == 0) {
            new PlayerRepository().create(PlayerToDal.getInstance()
                    .map(PlayerTestData.getValidPlayer()));

            WebDriverUtils.refresh();

            playersListPage = new PlayersListPage();
        }

        playerEditPage = playersListPage.editRandomPlayer();
    }

    @AfterTest
    public final void tearDown() {
        WebDriverUtils.stop();
    }
}
